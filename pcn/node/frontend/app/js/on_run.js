'use strict';

/**
 * @ngInject
 */
var OnRun = ["$rootScope", "AppSettings", "InfoAppService", "AuthService", "ConfigService",
    function ($rootScope, AppSettings, InfoAppService, AuthService, ConfigService){

        InfoAppService.obterInfo().then(function (info) {
            AppSettings.identificador = info.identificador;
            AppSettings.usuarioLogado = info.usuarioLogado;
            AppSettings.perfil = info.roles;
            AppSettings.urlLogout = info.urlLogout;
            $rootScope.AppSettings = AppSettings;
        });

        ConfigService.getVersion().then(function (config) {
            $rootScope.config = config;
        });

        $rootScope.$on('authCredentialSet', function () {
            var user = {};
            var app = {};

            user.id = AuthService.credentials.usuario.id;
            user.name = AuthService.credentials.usuario.corporativo.nome;
            user.role = AuthService.credentials.usuario.roles;

            app.name = AppSettings.titleApp;
            app.browser = AppSettings.browsers;
	        app.version = '1.65.28';

            $rootScope.user = user;
            $rootScope.app = app;
            $rootScope.usuarioLogado = AuthService.credentials;
            user.menu = true
            user.menu = {
                'default':true,
                'edital': false,
                'lista_edital':false,
                'geral': false,
                'perfil':''
            }

            var isnum = /^\d+$/.test(user.id);
            if (isnum) {
                switch (user.id.length) {
                    case 16:
                        $rootScope.tipoIdentificador = '(ORCAID: ' + user.id.substring(0, 4) + '-' + user.id.substring(4, 8) + '-' + user.id.substring(8, 12) + '-' + user.id.substring(12, 16) + ')';
                        break;
                    case 11:
                        $rootScope.tipoIdentificador = '(CPF: ' + user.id.substring(0, 3) + '.' + user.id.substring(3, 6) + '.' + user.id.substring(6, 9) + '-' + user.id.substring(9, 11) + ')';
                        break;
                }
            } else {
                $rootScope.tipoIdentificador = '(Passaporte: ' + user.id + ')';
            }

            angular.forEach(user.role, function(perfil, key) {
                switch(perfil.toUpperCase()) {
                    case AppSettings.perfis.P01.Name:
                        user.menu.lista_edital = true;
                        user.menu.perfil  = AppSettings.perfis.P01.Name;
                        return user.menu;
                    case AppSettings.perfis.P02.Name:
                        user.menu.perfil  = AppSettings.perfis.P02.Name;
                        return user.menu;
                    case AppSettings.perfis.P03.Name:
                        user.menu.geral  = true;
                        user.menu.edital = true;
                        user.menu.perfil  = AppSettings.perfis.P03.Name;
                        return user.menu;
                    case AppSettings.perfis.P04.Name:
                        user.menu.edital = true;
                        user.menu.lista_edital = true;
                        user.menu.perfil  = AppSettings.perfis.P04.Name;
                        return user.menu;
                    case AppSettings.perfis.P05.Name:
                        user.menu.edital = true;
                        user.menu.lista_edital = true;
                        user.menu.perfil  = AppSettings.perfis.P05.Name;
                        return user.menu;
                }
            });
        });
    }];

module.exports = OnRun;

