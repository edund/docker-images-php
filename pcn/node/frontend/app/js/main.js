'use strict';

// instala vendor libs
window.jQuery = require('jquery');
window.$ = require('jquery');
//FIXME verifcar porque não funciona importando desta forma
require('bootstrap');


// modulos do angular
var angular = require('angular');

require('angular-ui-router');
require('angular-bootstrap');
require('angular-translate');
require('angular-animate');
require('angular-translate-loader-static-files');
require('angularjs-toaster/toaster');
require('angular-messages');
require('capes-tmpl-externo-angular');
require('capes-ui');
require('angular-input-masks');
require('angular-touch');
require('angucomplete-alt');
require('angular-ui-sortable');

// configura app no angular
require('./templates');
require('./controllers/_index');
require('./services/_index');
require('./directives/_index');
require('./filters/_index');

// cria a aplicação 
angular.element(document).ready(function() {

  // modulos angular requeridos
  var requires = [
    'ui.router',
    'ui.bootstrap',
    'ngAnimate',
    'ngMessages',
    'pascalprecht.translate',  // traducao
    'toaster', // mensagens 
    'templates',
    'app.controllers',
    'app.services',
    'app.filters',
    'app.directives',
    'capes-ui',
    'ui.utils.masks',
    'blockUI',
    'ngTouch',
    'angucomplete-alt',
    'ui.sortable'
  ];

  // configura a app na window para testes
  window.app = angular.module('app', requires);

  // configura angular
  angular.module('app').constant('AppSettings', require('./constants'));
  angular.module('app').config(require('./on_config'));
  angular.module('app').run(require('./on_run'));

  // inicia app
  angular.bootstrap(document, ['app']);
 
});