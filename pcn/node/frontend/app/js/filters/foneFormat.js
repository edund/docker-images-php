'use strict';


var filtersModule = require('./_index.js');

var moment = require('moment');

function FoneFormat() {
    return function (number) {
        if (!number) {
            return '';
        }
        number = String(number.replace(/[^0-9]/g, ''));

        var formattedNumber = number;
        var area = number.substring(2, 0);

        // # (##) ####-####
        if (number.length == 10) {
            var front = number.substring(2, 6);
            var end = number.substring(6, 12);
        } else {
            // # (##) #####-####
            var front = number.substring(2, 7);
            var end = number.substring(7, 11);
        }

        if (front) {
            formattedNumber = ("(" + area + ") " + front);
        }
        if (end) {
            formattedNumber += ("-" + end);
        }
        return formattedNumber;
    };
}

filtersModule.filter('foneFormat', FoneFormat);
