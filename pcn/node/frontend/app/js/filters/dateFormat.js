'use strict';


var filtersModule = require('./_index.js');

var moment = require('moment');

function DateFormat() {
    return function (number) {
        if (!number) {
            return '';
        }
        var date= number.split('-');

        return date[2].substring(0,2) + '/' +date[1] + '/' + date[0] + ' ' + date[2].substring(3,5) + 'h' + date[2].substring(6,8);
    };
}

filtersModule.filter('dateFormat', DateFormat);
