'use strict';


var filtersModule = require('./_index.js');

var moment = require('moment');

function SituacaoArtigo() {
    return function (situacao) {
        if (!situacao) {
            return '';
        }
        switch (situacao) {
            case 'I':
                return  'Inscrição';
                break;
            case 'A':
                return  'Aceito';
                break;
            case 'N':
                return  'Não verificado';
                break;
            case 'R':
                return  'Não aceito';
                break;
            case 'P':
                return  'Pendente';
                break;
        }
    };
}

filtersModule.filter('situacaoArtigo', SituacaoArtigo);
