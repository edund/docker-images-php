'use strict';


var filtersModule = require('./_index.js');

var moment = require('moment');

function ProgressoBar() {
    return function (dataInicio, dataFim) {
        if (!dataInicio) {
            return '';
        }

        var datas = [];
        dataInicio = dataInicio.split('-');
        dataFim = dataFim.split('-');

        datas.inicio = new Date(dataInicio[0],dataInicio[1]-1,dataInicio[2].substring(0,2),dataInicio[2].substring(3,5), dataInicio[2].substring(6,8));
        datas.fim = new Date(dataFim[0],dataFim[1]-1,dataFim[2].substring(0,2),dataFim[2].substring(3,5), dataFim[2].substring(6,8));
        datas.now = new Date();

        if (datas.now > datas.inicio && datas.now < datas.fim) {

            // VAMOS APLICAR A REGRINHA DE TRÊS
            datas.inicio = moment(dataInicio,'YYYY-MM-DD');
            datas.fim = moment(dataFim,'YYYY-MM-DD');
            datas.now = moment(datas.now.getFullYear()+'-'+(datas.now.getMonth()+1)+'-'+datas.now.getDate(), 'YYYY-MM-DD');
            datas.intervalo =  datas.fim.diff(datas.inicio, 'days');
            datas.restantes = datas.fim.diff(datas.now, 'days');

            return Math.abs(((datas.restantes*100)/datas.intervalo)-100).toFixed(2);

        } else {
            return 100;
        }
    };
}

filtersModule.filter('progressoBar', ProgressoBar);
