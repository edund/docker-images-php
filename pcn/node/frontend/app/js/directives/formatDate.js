'use strict';

// #see
// http://alexperry.io/angularjs/2014/12/10/parsers-and-formatters-angular.html

var directivesModule = require('./_index.js');

var moment = require('moment');

//var $ = require('jquery');

/**
 * @ngInject
 */
function FormatDate() {
	return {
		restrict : 'A',
		require : 'ngModel',
		link : function(scope, element, attr, ngModel) {
                    
			ngModel.$formatters.push(function(value) {
				if (value) {
					return moment(value, attr.from).format(attr.to);
				}
			});

			ngModel.$parsers.push(function(value) {

				if (value) {
					var data = moment(value, attr.to).format(attr.from);

					if (data == 'Invalid date')
						data = "";
					return data;
				}
			});

		}
	};

}

directivesModule.directive('formatDate', FormatDate);