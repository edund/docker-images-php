'use strict';

var controllersModule = require('../_index');

/**
 * @ngInject
 */
var ComissaoController = ["$scope", "$rootScope", "$capesModal", "ManterComissaoService", "PessoasService", "AppSettings", "CommonService", "DistribuicaoService", "ValidacaoService", "$location", "toaster",
    function ($scope, $rootScope, $capesModal, ManterComissaoService, PessoasService, AppSettings, CommonService, DistribuicaoService, ValidacaoService, $location, toaster) {

        ValidacaoService.validaPeriodo(3)
                .then(function (validacao) {
                    if (validacao.validade == true) {
                        $capesModal.alert({
                            "text":AppSettings.messages.MSG043,
                            "title":'Aviso'
                        });
                        $location.url('/');
                    }
                }).catch(function (err) {
            $location.url('/');
        });

        $rootScope.content_premio = [true, true, true, true, true, true, true, true, true, true, true, true, true, true];
        $rootScope.content_especialista = [true, true, true, true, true, true, true];
        $rootScope.aviso = [];
        //$rootScope.aviso['comissao_fechada'] = {
        //    show: true,
        //    type: '',
        //    msg: AppSettings.messages.MSG015
        //};
        /**
         * Categorias
         */
        $scope.aviso = [];
        $scope.mostrarAviso = 1;
        $scope.categorias = [];
        $scope.categoria = {
            'selecionado': null
        };

        /**
         * Etapas
         */
        $scope.etapas = [];
        $scope.etapa = {
            'selecionado': null
        };

        $scope.varModalFechada = false;

        DistribuicaoService.recuperarCategoriasTematicas(1)
                .then(function (dados) {
                    $scope.categorias.itens = dados.categorias;
                }).catch(function (err) {
            toaster.pop('error', 'Sistema', 'Não foi possivel recuperar a pessoa informada');
        });


        ManterComissaoService.recuperarEtapas()
                .then(function (dados) {
                    $scope.etapas.itens = dados.etapas;
                }).catch(function (err) {
            if (err.status === 401) {
                CommonService.redirectUserAccess(true);
            }
        });
        $scope.incluirParticipante = function (idComissao, title, areas, situacao)
        {
            ManterComissaoService.parametrizacaoInclusaoParticipante(idComissao, $scope.etapa.selecionado.nome, title)
                    .then(function (dados) {
                        var message = '';
                        switch (dados.cod_mensagem) {
                            case 36:
                                message = AppSettings.messages.MSG036.slice(0, 74) + dados.detail + AppSettings.messages.MSG036.slice(81 + Math.abs(0));
                                break;
                            case 37:
                                message = AppSettings.messages.MSG037.slice(0, 78) + dados.detail + AppSettings.messages.MSG037.slice(84 + Math.abs(0));
                                break;
                            case 38:
                                message = AppSettings.messages.MSG038.slice(0, 51) + dados.detail + AppSettings.messages.MSG038.slice(57 + Math.abs(0));
                                break;
                            case 42:
                                message = AppSettings.messages.MSG042;
                                break;
                            case 44:
                                message = AppSettings.messages.MSG044.slice(0, 13) + dados.detail + AppSettings.messages.MSG044.slice(19 + Math.abs(0));
                                break;
                            case 45:
                                message = AppSettings.messages.MSG045.slice(0, 8) + dados.detail + AppSettings.messages.MSG045.slice(14 + Math.abs(0));
                                break;
                        }
                        if (message != "") {
                            $scope.aviso[idComissao] = {
                                show: true,
                                type: dados.status === 'erro'?'danger':dados.status,
                                msg: message,
                            };
                        } else {
                            $capesModal.open({
                                modalOptions: {
                                    size: 'lg',
                                    keyboard: false,
                                    backdrop: 'static',
                                    controller: ["$scope", "$modalInstance",
                                        function ($scope, $modalInstance) {
                                            "ngInject";
                                            $scope.area = {
                                                'selecionada': {
                                                    'id': null
                                                }
                                            };
                                            $scope.areaEspecifica = {
                                                'selecionada': {
                                                    'id': null
                                                }
                                            };
                                            $scope.show_result = false;
                                            $rootScope.aviso = {
                                                'show': false,
                                                'message': null,
                                            };

                                            $scope.fechar = function (modal) {
                                                modal(atualizarComissoes(ManterComissaoService, $rootScope, $scope));
                                            };

                                            $scope.idComissao = (idComissao) ? idComissao : $rootScope.idComissao;
                                            $scope.title = title;
                                            $scope.areas = areas;
                                            $scope.carregarAreaEspecifica = function () {
                                                ManterComissaoService.recuperarAreasEspecificas($scope.area.selecionada.id)
                                                        .then(function (dados) {
                                                            $scope.areasEspecificas = dados.areasEspecificas;
                                                            setTimeout(function () {
                                                                jQuery('#areaEspecifica').chosen().trigger("chosen:updated");

                                                            }, 100);

                                                        }).catch(function (err) {
                                                });
                                            };

                                            $rootScope.search = function () {
                                                ManterComissaoService.pesquisar($scope.pesquisar)
                                                        .then(function (dados) {

                                                            $scope.result_search = dados.result_search;

                                                            if ($scope.result_search != undefined) {
                                                                $scope.pagination = {
                                                                    total: dados.result_search.length,
                                                                    current: 1,
                                                                    perPage: 10,
                                                                    limit: 0,
                                                                    maxSize: 5
                                                                };

                                                                $scope.pageChanged = function () {
                                                                    if ($scope.pagination.current === 1) {
                                                                        $scope.pagination.limit = 0;
                                                                    } else {
                                                                        $scope.pagination.limit = (($scope.pagination.current * $scope.pagination.perPage) - $scope.pagination.perPage);
                                                                    }
                                                                };
                                                                $scope.show_result = true;
                                                                $rootScope.aviso['modal'] = {
                                                                    show: false
                                                                };
                                                            } else {
                                                                $scope.show_result = false;
                                                                $rootScope.aviso['modal'] = {
                                                                    show: true,
                                                                    type: '',
                                                                    msg: AppSettings.messages.MSG012
                                                                };
                                                            }

                                                            $scope.getClass = function (id) {
                                                                if (id == '')
                                                                    return 'disabled';
                                                            };

                                                            $scope.openLattes = function (idLattes) {
                                                                modalLattes($capesModal, idLattes);
                                                            };

                                                            $scope.openDetalhar = function (idPessoa) {
                                                                modalDetalhar($capesModal, PessoasService, idPessoa);
                                                            };

                                                            $rootScope.fecharAviso = function (index)
                                                            {
                                                                $rootScope.aviso[index] = {
                                                                    show: false
                                                                };
//                                                        $rootScope.aviso.splice(index, 0);
                                                            }

                                                            $scope.incluirParticipante = function (idPessoa) {
                                                                $scope.participante = {
                                                                    'idPessoa': idPessoa,
                                                                    'idComissao': idComissao,
                                                                    'situacao': situacao
                                                                }

                                                                ManterComissaoService.incluirParticipante($scope.participante)
                                                                        .then(function (dados) {
                                                                            if (CommonService.isParametrizacao(dados, $scope)) {
                                                                                if (dados.status == 200 && dados.detail == 'parecer') {
                                                                                    $rootScope.aviso[idComissao] = {
                                                                                        show: true,
                                                                                        type: '',
                                                                                        msg: AppSettings.messages.MSG014
                                                                                    };


                                                                                } else if (dados.status == 'fechada') {
                                                                                    $rootScope.aviso[idComissao] = {
                                                                                        show: true,
                                                                                        type: '',
                                                                                        msg: AppSettings.messages.MSG015
                                                                                    };
                                                                                } else {
                                                                                    $rootScope.aviso[idComissao] = {
                                                                                        show: true,
                                                                                        type: 'success',
                                                                                        msg: AppSettings.messages.MSG002
                                                                                    };
                                                                                }

                                                                                $rootScope.varModalFechada = true;

                                                                                //ManterComissaoService.pesquisar($scope.pesquisar)
                                                                                //    .then(function (dados) {
                                                                                //        $scope.result_search = dados.result_search;
                                                                                //    }).catch(function (err) {
                                                                                //});
                                                                                $modalInstance.close(atualizarComissoes(ManterComissaoService, $rootScope, $scope));
                                                                                //atualizarComissoes(ManterComissaoService, $rootScope));
                                                                            }
                                                                        }).catch(function (err) {
                                                                });
                                                            };
                                                            $scope.removerParticipante = function (idParticipante, idComissao, mod) {
                                                                $scope.show_result = false;
                                                                if (mod == '') {
                                                                    removerParticipante(idParticipante, $capesModal, AppSettings, ManterComissaoService, $scope, idComissao, '', $rootScope);
                                                                } else {
                                                                    removerParticipante(idParticipante, $capesModal, AppSettings, ManterComissaoService, $scope, idComissao, $modalInstance, $rootScope);
                                                                }
                                                            };
                                                        }).catch(function (err) {

                                                });
                                            }
                                        }],
                                    templateUrl: 'comissao/incluirParticipante.html'
                                }
                            }).then(function (success) {
                                $scope.varModalFechada = true;
                            }, function (error) {
                                $scope.varModalFechada = true;
                            });
                        }
                    }).catch(function (err) {
                toaster.pop('error', 'Sistema', 'Não foi possivel recuperar a pessoa informada');
            });
        };

        $scope.adicionarComissao = function(categoria, etapa)
        {
            var validaEtapa = parseInt(etapa.nome[0]);
            var comissao = {'idCategoria': categoria.idTema, 'etapa':validaEtapa, 'add_comissao':true};

            ManterComissaoService.adicionarComissao(comissao)
                .then(function (dados){

                    if ($scope.mostrarAviso == 1) {
                        $scope.aviso = {
                            show: true,
                            type: dados.status,
                            msg: dados.mensagem
                        };
                        $rootScope.mostrarAviso = 0;
                    };
                    if(dados.reloadComissao){
                        $rootScope.recuperaGrupos();
                    }
                    //toaster.pop(dados.status, 'Sistema',dados.mensagem);

            }).catch(function (err){
                toaster.pop('error', 'Sistema', 'Não foi possível adicionar comissão.');
            });


        };

        $scope.fecharAviso = function ()
        {
            $scope.aviso = {
                show: false
            };
        };

        //console.log( "$scope.varModalFechada", $scope.varModalFechada );

        $scope.$watch('varModalFechada',
                function (newVal, oldVal) {
                    //console.log( "Function watched", newVal );
                    if (newVal === true) {
                        //$scope.recuperaGrupos();
                        $scope.varModalFechada = false;
                        setTimeout(function () {
                            jQuery('#etapa').trigger('change');
                        }, 2000);
                    }

                },
                function (newValue) {
                    console.log('newValue', newValue);
                }
        );

        $rootScope.recuperaGrupos = function () {
            if ($scope.categoria.selecionado == null || $scope.etapa.selecionado == null) {
                return false;
            }

            ManterComissaoService.recuperarComissoes($scope.categoria.selecionado.idTema, $scope.etapa.selecionado.id)
                    .then(function (dados) {

                        console.log("aqui ",dados);

                        $rootScope.comissoes = {
                            'premio': null,
                            'especializada': null
                        };

                        if (dados.comissoes['P']) {
                            $rootScope.comissoes.premio = dados.comissoes['P'].grupos;
                        }

                        if (dados.comissoes['G']) {
                            $rootScope.comissoes.especializada = dados.comissoes['G'].grupos;
                        }

                        $rootScope.areas = dados.areas;

                        $rootScope.getClass = function (id) {
                            if (id == '')
                                return 'disabled';
                        }

                        $rootScope.openDetalhar = function (idPessoa) {
                            modalDetalhar($capesModal, PessoasService, idPessoa);
                        }

                        $rootScope.openLattes = function (idLattes) {
                            modalLattes($capesModal, idLattes);
                        }

                        $scope.getClass = function (situacao) {
                            if (situacao == 'F' || situacao == '')
                                return 'disabled';
                        }

                        $rootScope.fecharComissao = function (id) {
                            $scope.comissao = {
                                'id': id,
                                'situacao': 'F',
                                'etapa': $scope.etapa.selecionado.nome
                            }

                            ManterComissaoService.alterarComissao($scope.comissao)
                                    .then(function (dados) {
                                        if (CommonService.isParametrizacao(dados, $scope)) {
                                            atualizarComissoes(ManterComissaoService, $rootScope, $scope);
                                            if (dados.id) {
                                                $rootScope.aviso[id] = {
                                                    show: true,
                                                    type: 'success',
                                                    msg: AppSettings.messages.MSG002
                                                };
                                            } else if (dados.restante_participante) {
                                                $rootScope.aviso[id] = {
                                                    show: true,
                                                    type: '',
                                                    msg: AppSettings.messages.MSG016.slice(0, 7) + dados.restante_participante + AppSettings.messages.MSG016.slice(16 + Math.abs(0))
                                                };
                                            } else if (dados.restante_artigo) {
                                                $rootScope.aviso[id] = {
                                                    show: true,
                                                    type: '',
                                                    msg: AppSettings.messages.MSG045.slice(0, 8) + dados.restante_artigo + AppSettings.messages.MSG045.slice(14 + Math.abs(0))
                                                };
                                            } else if (dados.status == 'fechada') {
                                                $rootScope.aviso['comissao_fechada'] = {
                                                    show: true,
                                                    type: '',
                                                    msg: AppSettings.messages.MSG015
                                                };

                                            } else if (dados.status == 'error') {
                                                $rootScope.aviso[id] = {
                                                    show: true,
                                                    type: 'danger',
                                                    msg: AppSettings.messages.MSG050
                                                };

                                            }
                                        }
                                        $scope.recuperaGrupos();
                                    }).catch(function (err) {
                            });
                        }

                        $rootScope.abrirComissao = function (id) {
                            $scope.comissao = {
                                'id': id,
                                'situacao': 'A'
                            }

                            ManterComissaoService.alterarComissao($scope.comissao)
                                    .then(function (dados) {
                                        atualizarComissoes(ManterComissaoService, $rootScope, $scope);
                                        $rootScope.aviso[id] = {
                                            show: true,
                                            type: 'success',
                                            msg: AppSettings.messages.MSG002
                                        };
                                        $scope.recuperaGrupos();
                                    }).catch(function (err) {
                            });
                        }

                        $rootScope.removerParticipante = function (idParticipante, idComissao, mod) {
                            if (mod == "") {
                                removerParticipante(idParticipante, $capesModal, AppSettings, ManterComissaoService, $scope, idComissao, '', $rootScope);
                            } else {
                                removerParticipante(idParticipante, $capesModal, AppSettings, ManterComissaoService, $scope, idComissao, $modalInstance, $rootScope);
                            }
                        }

                        CommonService.isAccessProfile('comissao');

                    }).catch(function (err) {

                if (err.status === 401 || err.status === 500) {
                    CommonService.redirectUserAccess(true);
                }

            });

        };

        $rootScope.fecharAviso = function (index)
        {
            $rootScope.aviso[index] = {
                show: false
            };
            //$rootScope.aviso.splice(index, 1);
        }
    }];

controllersModule.controller('ComissaoCtrl', ComissaoController);

function removerParticipante(idParticipante, Modal, AppSettings, Service, Scope, index, modalInstance, $rootScope)
{
    Modal.confirm({
        "text": AppSettings.messages.MSG011,
        "title": 'Confirmação',
        "dismissText": 'Voltar'
    }).then(function () {

        Service.removerParticipante(idParticipante)
                .then(function (dados) {
                    if (dados.status == 200 && dados.detail != 'ok') {
                        Scope.aviso[index] = {
                            show: true,
                            type: '',
                            msg: AppSettings.messages.MSG014
                        };
                    } else if (dados.detail == 'ok') {
                        Scope.aviso[index] = {
                            show: true,
                            type: 'success',
                            msg: AppSettings.messages.MSG002
                        };
                    } else if (dados.detail == 'fechada') {
                        Scope.aviso[index] = {
                            show: true,
                            type: '',
                            msg: AppSettings.messages.MSG015
                        };
                    }
                    if (modalInstance != '') {
                        modalInstance.close();
                    }
                    if (Scope.pesquisar == undefined) {
                        atualizarComissoes(Service, $rootScope, Scope);
                    }

                    Scope.recuperaGrupos();
                    Scope.varModalFechada = true;

                    setTimeout(function () {
                        jQuery('#etapa').trigger('change');
                    }, 2000);
                }).catch(function (err) {

        });

    });
}


function atualizarComissoes(service, $rootScope, $scope)
{
    if ($scope.categoria == undefined || $scope.etapa == undefined) {
        service.recuperarComissoes()
                .then(function (dados) {
                    $rootScope.comissoes = {
                        'premio': dados.comissoes[1].grupos,
                        'especializada': dados.comissoes[2].grupos
                    };
                    $rootScope.areas = dados.areas;

                }).catch(function (err) {
        });
    } else {
        service.recuperarComissoes($scope.categoria.selecionado.idTema, $scope.etapa.selecionado.id)
                .then(function (dados) {
                    $rootScope.comissoes = {
                        'premio': null,
                        'especializada': null
                    };
                    if (dados.comissoes[1]) {
                        $rootScope.comissoes.premio = dados.comissoes[1].grupos;
                    }
                    if (dados.comissoes[2]) {
                        $rootScope.comissoes.especializada = dados.comissoes[2].grupos;
                    }
                    $rootScope.areas = dados.areas;
                }).catch(function (err) {
        });
    }
}

function modalDetalhar(modal, Service, idPessoa)
{
    modal.open({
        modalOptions: {
            size: 'lg',
            controller: ["$scope",
                function ($scope) {
                    Service.recuperar(idPessoa,true)
                            .then(function (dados) {
                                $scope.pessoa = dados;
                            }).catch(function (err) {
                    });
                }],
            templateUrl: 'comissao/detalhar.html',
        }
    });
}

function modalLattes(modal, idLattes)
{
    window.open('http://lattes.cnpq.br/' + idLattes, '_blank');
    //modal.open({
    //    content: {
    //        header: {
    //            title: 'Curriculum Lattes',
    //            closeButton: true
    //        },
    //        body: '<iframe style="border: none;" src="http://lattes.cnpq.br/' + idLattes + '" width="100%" height="600px"></iframe>',
    //    },
    //    modalOptions: {
    //        size: 'lg'
    //    }
    //});

}