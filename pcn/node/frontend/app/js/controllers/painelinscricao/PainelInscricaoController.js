'use strict';

var controllersModule = require('../_index');

/**
 * @ngInject
 */
var PainelInscricaoController = ["toaster", "$scope", "$rootScope", "$capesModal",
    "AppSettings", "CommonService", "PainelInscricaoService", "$location", "$stateParams",
    function (toaster, $scope, $rootScope, $capesModal, AppSettings,
            CommonService, PainelInscricaoService, $location, $stateParams) {

        $scope.aviso = [];
        $scope.regex = '\^[a-zA-Z0-9].*$';

        $scope.orderByFieldS = 'dscSituacao';
        $scope.reverseSortS = false;

        $scope.orderByFieldP = 'dscPremiacao';
        $scope.reverseSortP = false;

        $scope.orderByFieldI = 'dscIes';
        $scope.reverseSortI = false;

        $scope.orderByFieldQ = 'dscQualis';
        $scope.reverseSortQ = false;

        $scope.orderByFieldC = 'dscCategoria';
        $scope.reverseSortC = false;

        $scope.orderByFieldN = 'nivel';
        $scope.reverseSortN = false;

        if ($stateParams.id == undefined) {
            recuperarDados(PainelInscricaoService, $scope, toaster, AppSettings, CommonService);
        }

        $scope.fecharAviso = function ()
        {
            $scope.aviso = {
                show: false
            };
        }

        $scope.redirect = function (url)
        {
            $location.path(url);
        }

        $scope.carregaRelatorio = function (tpRelatorio)
        {
            $stateParams.tpRelatorio = tpRelatorio;
            recuperarDadosRelatorio(PainelInscricaoService, $scope, toaster, AppSettings, $stateParams, CommonService);
        }

    }];

controllersModule.controller('PainelInscricaoCtrl', PainelInscricaoController);

function recuperarDados(PainelInscricaoService, $scope, toaster, AppSettings, CommonService)
{
    PainelInscricaoService.retornoDados()
            .then(function (retorno) {
                $scope.retorno = retorno;
                $scope.AppSettings = AppSettings;
            }).catch(function (err) {
        if (err.status === 401) {
            CommonService.redirectUserAccess(true);
        } else {
            toaster.pop('error', 'Sistema', 'Não foi possivel recuperar a informação');

        }
    });
}

function recuperarDadosRelatorio(PainelInscricaoService, $scope, toaster, AppSettings, $stateParams, CommonService)
{
    PainelInscricaoService.recuperarDadosRelatorio($stateParams.tpRelatorio, $stateParams.id)
            .then(function (retorno) {
                $scope.codElemento = $stateParams.id;
                $scope.Itens = retorno;
                $scope.AppSettings = AppSettings;
            }).catch(function (err) {
        if (err.status === 401) {
            CommonService.redirectUserAccess(true);
        } else {
            toaster.pop('error', 'Sistema', 'Não foi possivel recuperar a informação');

        }
    });
}