'use strict';

var controllersModule = require('../_index');


/**
 * @ngInject
 */
var GerenciarInstriucaoAvaliacaoController = ["$state", "$scope", "$rootScope", "DistribuicaoService", "toaster", "AppSettings", "$capesModal", "CommonService", "GerenciarInstrucaoAvaliacaoService",
    function ($state, $scope, $rootScope, DistribuicaoService, toaster, AppSettings, $capesModal, CommonService, GerenciarInstrucaoAvaliacaoService) {

        $scope.arquivos_enviado = {};

        /**
         * Categorias
         */
        $scope.categorias = [];
        $scope.categoria = {
            'selecionado': null
        };

        /**
         * Grupos
         */
        $scope.grupos = [];
        $scope.grupo = {
            'selecionado': null
        };

        $scope.documento = {
            titulo: null,
            comissaoAvaliacao: null
        };

        DistribuicaoService.recuperarCategoriasTematicas(1)
                .then(function (dados) {

                    $scope.categorias.itens = dados.categorias;

                }).catch(function (err) {
            if (err.status === 401) {
                CommonService.redirectUserAccess(true);
            }
        });

        GerenciarInstrucaoAvaliacaoService.recuperarDocumentos()
                .then(function (dados) {
                    if (typeof(dados._embedded) !== 'undefined') {
                        $scope.documentos = dados._embedded.instrucao_avaliacao;
                    }
                }).catch(function (err) {
            toaster.pop('error', 'Sistema', 'Não foi possivel recuperar a pessoa informada');
        });
        /**
         * Recupera os grupos
         *
         * @param idGrupoSelecionado
         * @returns {boolean}3
         */
        $scope.carregarGrupos = function (idGrupoSelecionado) {

            if ($scope.categoria.selecionado == null)
                return false;

            DistribuicaoService.recuperarGrupos($scope.categoria.selecionado.idTema)
                    .then(function (dados) {

                        CommonService.isAccessProfile('comissao')
                        if (dados) {
                            $scope.grupos.itens = dados.grupos;
                            if (idGrupoSelecionado) {
                                $scope.grupo.selecionado = {id: idGrupoSelecionado};
                                $scope.documento.comissaoAvaliacao = idGrupoSelecionado;
                            }
                        }

                    }).catch(function (err) {
                toaster.pop('error', 'Sistema', 'Não foi possivel recuperar a pessoa informada');
            });
        };

        $scope.setarComissao = function () {
            $scope.pode = false;

            if ($scope.documento.titulo == null || $scope.documento.titulo.trim() == "")
                return false;
            if ($scope.grupo.selecionado == undefined)
                return false;
            $scope.documento.comissaoAvaliacao = $scope.grupo.selecionado.id;
            $scope.pode = true;
        };

        $scope.setarTitulo = function () {
            $scope.setarComissao();
        };

        $scope.editar = function (documento) {
            GerenciarInstrucaoAvaliacaoService.recuperarDocumento(documento.id)
                    .then(function (dados) {
                        $scope.documento = dados;
                        $scope.categoria.selecionado = {idTema: dados.grupo.id};
                        $scope.carregarGrupos(dados.comissao.id);

                    }).catch(function (err) {
                toaster.pop('error', 'Sistema', 'Não foi possivel recuperar o documento');
            });

        };

        $scope.excluir = function (documento) {

            $capesModal.confirm({
                "text": AppSettings.messages.MSG006,
                "title": 'Exclusão',
                "dismissText": 'Voltar'
            }).then(function () {

                GerenciarInstrucaoAvaliacaoService.excluirDocumento(documento.id)
                        .then(function (dados) {

                            GerenciarInstrucaoAvaliacaoService.recuperarDocumentos()
                                    .then(function (dados) {
                                        if (typeof(dados._embedded) !== 'undefined') {
                                            $scope.documentos = dados._embedded.instrucao_avaliacao;
                                        }
                                    }).catch(function (err) {
                                toaster.pop('error', 'Sistema', 'Não foi possivel recuperar a pessoa informada');
                            });

                        }).catch(function (err) {
                    toaster.pop('error', 'Sistema', 'Não foi possivel excluir o documento');
                });

            });

        };

        $scope.fileuploadConf = {
            'options': {
                'url': '/rest/instrucao-avaliacao',
                'method': 'post',
                'maxFilesize': '1024',
                'dictDefaultMessage': 'Clique aqui para enviar.',
                'headers': {
                    "tipo-arquivo": 1,
                    "id-pessoa": 14564
                },
                'maxFiles': 1,
                'acceptedFiles': ".doc, .docx",
                'addRemoveLinks': true,
                'dictInvalidFileType': "Arquivo inválido. Somente aceito arquivo do tipo DOC ou DOCX"
            },
            'eventHandlers': {
                'sending': function (file, xhr, formData) {

                },
                'success': function (file, response) {
                    $state.reload();
                },
            }
        };

        $scope.fileuploadConf.options.myDropzone = {
            init: function () {
                this.on("addedfile", function (file) {
                    var removeButton = Dropzone.createElement("<button>Remove file</button>");
                    var _this = this;

                    removeButton.addEventListener("click", function (e) {
                        e.preventDefault();
                        e.stopPropagation();

                        _this.removeFile(file);
                    });

                    file.previewElement.appendChild(removeButton);
                });
            }
        };
        $scope.pode = false;
        $scope.podeExibirUpload = function () {
            return $scope.pode;
        }


    }];

controllersModule.controller('GerenciarInstrucaoAvaliacaoCtrl', GerenciarInstriucaoAvaliacaoController);