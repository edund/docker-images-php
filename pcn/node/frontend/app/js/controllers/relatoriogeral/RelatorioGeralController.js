'use strict';

var controllersModule = require('../_index');

/**
 * @ngInject
 */
var RelatorioGeralController = ["toaster", "$scope", "$rootScope", "$capesModal",
    "AppSettings", "CommonService", "$location", "$stateParams", "RelatorioGeralService",
    function (toaster, $scope, $rootScope, $capesModal, AppSettings,
            CommonService, $location, $stateParams, RelatorioGeralService) {

        $scope.fecharAviso = function ()
        {
            $scope.aviso = {
                show: false
            };
        }

        $scope.show_result = false;
        $scope.aviso = {
            'show': false,
            'message': null,
        };

        recuperarCombo(RelatorioGeralService, $scope, toaster, AppSettings, $stateParams, CommonService);
        $scope.etapa = {
            'itens': [
                {
                    'id': 'T',
                    'nome': 'Todos'
                },
                {
                    'id': '1',
                    'nome': '1ª'
                },
                {
                    'id': '2',
                    'nome': '2ª'
                }
            ]
        };

        $scope.search = function () {
            resultSearch($scope, AppSettings, RelatorioGeralService);
        };

        $scope.openParecer = function (idParecer) {
            modalParecer($capesModal, idParecer, RelatorioGeralService);
        };
    }];

controllersModule.controller('RelatorioGeralCtrl', RelatorioGeralController);

function resultSearch($scope, AppSettings, RelatorioGeralService) {
    RelatorioGeralService.pesquisar($scope)
            .then(function (dados) {
                $scope.result_search = dados.result_search;
                $scope.rtAno = dados.ano;
                $scope.rtEtapa = dados.etapa;
                if ($scope.result_search.length > 0) {
                    $scope.show_result = true;
                    $scope.aviso.show = false;
                } else {
                    $scope.show_result = false;
                    $scope.msg = AppSettings.messages.MSG012;
                    $scope.aviso.type = 'danger';
                    $scope.aviso.show = true;
                    $scope.aviso.msg = AppSettings.messages.MSG012;
                }
            }).catch(function (err) {
    });

}

function modalParecer(modal, idParecer, RelatorioGeralService) {
    modal.open({
        modalOptions: {
            size: 'lg',
            controller: ["$scope",
                function ($scope) {
                    RelatorioGeralService.parecer(idParecer)
                            .then(function (dados) {
                                $scope.parecer = dados.parecer;
                            }).catch(function (err) {
                    });
                }],
            templateUrl: 'relatoriopremio/parecer.html',
        }
    });
}

function recuperarCombo(RelatorioGeralService, $scope, toaster, AppSettings, $stateParams, CommonService)
{
    RelatorioGeralService.retornoCombo()
            .then(function (retorno) {

                $scope.model = {
                    selected: [],
                    data: retorno.itens
                };
                $scope.itens = retorno.itens;
                $scope.AppSettings = AppSettings;
            }).catch(function (err) {
        if (err.status === 401) {
            CommonService.redirectUserAccess(true);
        } else {
            toaster.pop('error', 'Sistema', 'Não foi possivel recuperar a informação');

        }
    });
}
