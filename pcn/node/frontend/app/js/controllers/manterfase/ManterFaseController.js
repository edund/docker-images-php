'use strict';

var controllersModule = require('../_index');

/**duardo
 * @ngInject
 */
var ManterFaseController = ["toaster", "$scope", "$rootScope", "$capesModal",
    "AppSettings", "CommonService", "ManterFaseService",
    function (toaster, $scope, $rootScope, $capesModal, AppSettings,
              CommonService, ManterFaseService) {

        $scope.fases = {};
        $scope.aviso = [];
        $scope.regex = '^[a-z\u00C0-\u00ff A-Z0-9]+$';

        $scope.validarInput = function (dados) {
            var valor = dados.nome.$viewValue;
            $scope.fase.nome = valor.replace(/[^a-z\u00C0-\u00ff A-Z0-9]/gi, '');
        }

        recuperarFases(ManterFaseService, $scope, toaster, AppSettings, CommonService);
        $scope.salvar = function () {
            var valor = $scope.enviar.nome.$modelValue;
            if (valor === undefined || valor === '') {
                jQuery('#fase_nome').addClass('has-error');
                toaster.pop('error', '', AppSettings.messages.MSG025);
                return;
            } else {
                jQuery('#fase_nome').removeClass('has-error');
                ManterFaseService.salvar($scope.enviar)
                    .then(function (dados) {
                        if (dados.exist == true) {
                            $scope.aviso = {
                                show: true,
                                type: 'warning',
                                msg: AppSettings.messages.MSG024
                            };
                        } else if (dados.removida == true) {
                            $scope.aviso = {
                                show: true,
                                type: 'warning',
                                msg: AppSettings.messages.MSG026
                            };
                        } else {
                            $scope.aviso = {
                                show: true,
                                type: 'success',
                                msg: AppSettings.messages.MSG002
                            };
                        }
                        recuperarFases(ManterFaseService, $scope, toaster, AppSettings)
                    }).catch(function (err) {
                });
            }
        }

        $scope.alterar = function (id) {
            jQuery('#fase_nome').removeClass('has-error');
            ManterFaseService.recuperarFase(id)
                .then(function (dados) {
                    $scope.aviso = {
                        show: false
                    }
                    $scope.fase = dados;

                }).catch(function (err) {
                if (err.status == 406) {
                    $scope.aviso = {
                        show: true,
                        type: 'danger',
                        msg: AppSettings.messages.MSG026
                    };
                }
            });

        }

        $scope.excluir = function (id) {
            jQuery('#fase_nome').removeClass('has-error');
            $capesModal.confirm({
                "text": AppSettings.messages.MSG011,
                "title": 'Confirmação',
                "dismissText": 'Voltar'
            }).then(function () {
                ManterFaseService.delete(id)
                    .then(function (dados) {
                        $scope.aviso = {
                            show: true,
                            type: 'success',
                            msg: AppSettings.messages.MSG002
                        };
                        window.scrollTo(0, 0);
                        recuperarFases(ManterFaseService, $scope, toaster, AppSettings);

                    }).catch(function (err) {
                    if (err.status == 405) {
                        $scope.aviso = {
                            show: true,
                            type: 'danger',
                            msg: AppSettings.messages.MSG031
                        };
                        window.scrollTo(0, 0);
                        recuperarFases(ManterFaseService, $scope, toaster, AppSettings);
                    }
                    if (err.status == 406) {
                        $scope.aviso = {
                            show: true,
                            type: 'danger',
                            msg: AppSettings.messages.MSG026
                        };
                        window.scrollTo(0, 0);
                        recuperarFases(ManterFaseService, $scope, toaster, AppSettings);
                    }
                });
            });
        }

        $scope.fecharAviso = function () {
            $scope.aviso = {
                show: false
            };
        }

    }];

controllersModule.controller('ManterFaseCtrl', ManterFaseController);

function recuperarFases(ManterFaseService, $scope, toaster, AppSettings, CommonService) {
    ManterFaseService.recuperarFase()
        .then(function (fases) {
            $scope.fases = fases;
            $scope.AppSettings = AppSettings;
            if (fases.permissao == false) {
                CommonService.redirectUserAccess(true);
            }
        }).catch(function (err) {
        toaster.pop('error', 'Sistema', 'Não foi possivel recuperar a pessoa informada');
    });

}