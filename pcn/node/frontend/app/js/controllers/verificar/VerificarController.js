'use strict';

var controllersModule = require('../_index');

/**
 * @ngInject
 */
var VerificarController = ["$stateParams", "$scope", "$window", "CommonService", "AppSettings",
    "VerificarArtigoService", "ArtigoService", "$rootScope", "PremioService", "$capesModal",
    "ManterComissaoService", "DistribuicaoService", "ValidacaoService", "$location",
    function ($stateParams, $scope, $window, CommonService, AppSettings, VerificarArtigoService,
              ArtigoService, $rootScope, PremioService, $capesModal, ManterComissaoService,
              DistribuicaoService, ValidacaoService, $location) {

        $scope.aviso = {
            'show': false,
            'message': null,
        };
        $rootScope.aviso = '';


        $scope.programas = {};
        $scope.categorias = {};
        $scope.ies = {};
        $scope.comissoes = {};
        $scope.situacao_inscricao = {
            'selecionado': {
                'id': null
            }
        };
        $scope.comissao = {
            'selecionado': {
                'id': null
            }
        };

        /**
         * Comissoes Adhoc
         */
        $scope.comissoesadhoc = [];
        $scope.comissaoadhoc = {
            'selecionado': null
        };


        $scope.show_result = false;


        $scope.situacoes = {
            'itens': [
                {
                    'id': 'A',
                    'nome': 'Aceito'
                },
                {
                    'id': 'R',
                    'nome': 'Não Aceito'
                },
                {
                    'id': 'N',
                    'nome': 'Não verificado'
                },
                {
                    'id': 'P',
                    'nome': 'Pendente'
                }
            ]
        };

        // suporte a troca de arquivos
        $scope.arquivos_enviado = {};
        $scope.total_enviados = {};

        ValidacaoService.validaPeriodo(2)
            .then(function (validacao) {
                if (validacao.validade === true ) {
                    if(validacao.situacao.estaDepoisDoPeriodo) {
                        $rootScope.aviso = AppSettings.messages.MSG043;
                    }
                    $capesModal.alert({
                        "text":AppSettings.messages.MSG043,
                        "title":'Aviso'
                    });
                    $location.url('/');
                }
            }).catch(function (err) {
            $location.url('/');
        });

        // recupera arquivos enviados
        // TODO implementar a recuperação conforme o evento correto
        ArtigoService.recuperarArquivos(0) //$rootScope.id_evento
            .then(function (retorno) {
                $scope.arquivos = retorno;
            }).catch(function (err) {
        });

        $scope.fileuploadConf = {
            'options': {// passed into the Dropzone constructor
                'url': '/rest/premio/tiposarquivos/1',
                'method': 'post',
                'uploadMultiple': false,
                'maxFilesize': 700,
                'parallelUploads': 1,
                'dictDefaultMessage': 'Clique aqui para enviar.',
                'headers': {"tipo-arquivo": 1, "id-pessoa": $scope.id_pessoa},
                'maxFiles': 1,
                'acceptedFiles': ".pdf",
                'dictFileTooBig': AppSettings.messages.MSG048,
                'dictResponseError': AppSettings.messages.MSG017,
                'dictMaxFilesExceeded': '',
                'dictInvalidFileType': AppSettings.messages.MSG008,
                init: function() {
                    this.on("addedfile", function(file) {
                        if (this.files[1] != null) {
                            this.removeFile(this.files[1]);
                        }
                        var removeButton = Dropzone.createElement("<a>Remover</a>");
                        var _this = this;
                        removeButton.addEventListener("click", function (e) {
                            e.preventDefault();
                            e.stopPropagation();
                            _this.removeFile(file);
                        });
                        file.previewElement.appendChild(removeButton);
                    });

                    this.on("error", function(file, msg, response) {
                        var dropzone = this;
                        file.previewElement.addEventListener("click", function (e) {
                            e.preventDefault();
                            e.stopPropagation();
                            dropzone.removeFile(file);
                        });

                        if (response !== undefined) {
                            $(file.previewElement).find('.dz-error-message > span').text(AppSettings.messages.MSG017);
                            return false;
                        }
                    });

                    this.on("success", function(file, response) {
                        PremioService.recuperarArquivosEnviados($scope.artigo.id_inscricao)
                            .then(function (retorno) {
                                if (retorno.enviados === undefined) {
                                    $scope.total_enviados = 1;
                                } else {
                                    $scope.total_enviados = retorno.enviados.length;
                                }
                                $scope.arquivos_enviado = retorno;
                            }).catch(function (err) {
                        });
                    });
                }
            },
        };

        // recupera os arquivos enviados por inscrição
        $scope.atualizar = function () {
            PremioService.recuperarArquivosEnviados($scope.artigo.id_inscricao)
                .then(function (retorno) {
                    if (retorno.enviados === undefined) {
                        $scope.total_enviados = 1;

                    } else {
                        $scope.total_enviados = retorno.enviados.length;
                    }
                    $scope.arquivos_enviado = retorno;

                }).catch(function (err) {
            });
        };

        $scope.init = function () {
            $scope.pesquisar.titulo.$modelValue = null;
            $scope.pesquisar.autor.$modelValue = null;
            ValidacaoService.validaPeriodo(2)
                .then(function (validacao) {
                    if (validacao.validade === true) {
                        $scope.show_result = false;
                        $scope.mensagem = AppSettings.messages.MSG043;
                        $scope.aviso.show = true;
                        $scope.aviso.message = AppSettings.messages.MSG043;
                    } else {
                        resultSearch(VerificarArtigoService, $scope, AppSettings);
                    }
                }).catch(function (err) {
                $location.url('/');
            });
        };

        $scope.search = function () {
            ValidacaoService.validaPeriodo(2)
                .then(function (validacao) {
                    if (validacao.validade === true) {
                        $scope.show_result = false;
                        $scope.mensagem = AppSettings.messages.MSG043;
                        $scope.aviso.show = true;
                        $scope.aviso.message = AppSettings.messages.MSG043;
                    } else {
                        resultSearch(VerificarArtigoService, $scope, AppSettings);
                    }
                }).catch(function (err) {
                $location.url('/');
            });
        };

        $scope.carregarComissoesAdhoc = function () {

            if ($scope.categoria.selecionado == null) {
                return false;
            }

            //@todo segundo parametro eh a etapa
            DistribuicaoService.recuperarGrupos($scope.categoria.selecionado.idTema)
                .then(function (dados) {

                    CommonService.isAccessProfile('verificar');
                    if (dados) {
                        $scope.comissoesadhoc.itens = dados.grupos;

                        setTimeout(function () {
                            $('#comissaoadhoc').trigger("chosen:updated");
                        }, 2000);
                    }

                }).catch(function (err) {
                toaster.pop('error', 'Sistema', 'Não foi possivel recuperar a pessoa informada');
            });
        };

        $scope.update = function () {
            VerificarArtigoService.salvar($scope.checklist)
                .then(function (dados) {
                    if (dados.status === 200) {
                        $scope.aviso.message = AppSettings.messages.MSG002;
                        $('.alert').removeClass('alert-danger');
                        $('.alert').addClass('alert-success');
                    } else {
                        //$scope.aviso.message = dados.detail;
                        $('.alert').removeClass('alert-success');
                        $('.alert').addClass('alert-danger');
                    }
                    window.scrollTo(0, 0);
                    $scope.aviso.show = true;

                    init($scope, $stateParams, CommonService, PremioService, $window, VerificarArtigoService);

                }).catch(function (err) {
            });
        };

        if ($stateParams.idArtigo == undefined) {
            VerificarArtigoService.recuperarIesAndProgramas()
                .then(function (dados) {
                    $scope.ies.itens = dados.ies;
                    $scope.programas.itens = dados.programas;
                    CommonService.isAccessProfile('verificar');

                }).catch(function (err) {
                if (err.status === 401 || err.status === 500) {
                    CommonService.redirectUserAccess(true);
                }
            });
        } else {
            init($scope, $stateParams, CommonService, PremioService, $window, VerificarArtigoService);
        }
    }];

controllersModule.controller('VerificarCtrl', VerificarController);

function init($scope, $stateParams, CommonService, PremioService, $window, VerificarArtigoService, AppSettings) {


    $scope.id_artigo = $stateParams.idArtigo;

    VerificarArtigoService.obterArtigo($stateParams.idArtigo)
        .then(function (dados) {

            $scope.comissoes = dados.comissoes;

            CommonService.isAccessProfile('verificar');
            $scope.artigo = dados;

            PremioService.recuperarArquivosEnviados(dados.id_inscricao)
                .then(function (retorno) {
                    if (retorno.enviados === undefined) {
                        $scope.total_enviados = 1;
                    } else {
                        $scope.total_enviados = retorno.enviados.length;
                    }
                    $scope.arquivos_enviado = retorno;
                }).catch(function (err) {
                $window.location.href = '#/pcn';
            });

            $('#situacao').val(dados.situacao_inscricao);

            setTimeout(function () {
                $('#comissao').trigger("chosen:updated");
            }, 3000);

            $('#situacao').trigger("chosen:updated");
            $scope.checklist.comissao.$modelValue.id = dados.id_comissao;
            $scope.checklist.situacao.$modelValue.id = dados.situacao_inscricao;

        }).catch(function (err) {
        if (err.status === 401) {
            CommonService.redirectUserAccess(true);
        }
        if (err.status === 500) {
            $window.location.href = '#/verificar';
        }
    });

}

function resultSearch(service, $scope, AppSettings) {
    service.pesquisar($scope.pesquisar)
        .then(function (dados) {
            $scope.ies.itens = dados.ies;
            $scope.programas.itens = dados.programas;
            $scope.categorias.itens = dados.categorias;
            $scope.result_search = dados.result_search;

            if ($scope.result_search != undefined) {
                $scope.pagination = {
                    total: dados.result_search.length,
                    current: 1,
                    perPage: AppSettings.resultsPerPage,
                    limit: 0,
                    maxSize: 5
                };

                $scope.pageChanged = function () {
                    if ($scope.pagination.current === 1) {
                        $scope.pagination.limit = 0;
                    } else {
                        $scope.pagination.limit = (($scope.pagination.current * $scope.pagination.perPage) - $scope.pagination.perPage);
                    }
                };
                $scope.show_result = true;
                $scope.aviso.show = false;
            } else {
                $scope.show_result = false;
                $scope.mensagem = AppSettings.messages.MSG012;
                $scope.aviso.show = true;
                $scope.aviso.message = AppSettings.messages.MSG012;
            }
        }).catch(function (err) {
    });

}