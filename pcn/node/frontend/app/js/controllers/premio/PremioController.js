'use strict';

var controllersModule = require('../_index');

/**duardo
 * @ngInject
 */
var PremioController = ["toaster", "$scope", "$rootScope", "$capesModal",
    "AppSettings", "CommonService", "PremioService", "$stateParams", "$window", "CategoriaService",
    function (toaster, $scope, $rootScope, $capesModal, AppSettings,
            CommonService, PremioService, $stateParams, $window, CategoriaService) {

        $scope.rowsDocs = [];
        $scope.rowsChecklist = [];
        $scope.rowsTematicas = [];
        $scope.rowsParametros = [];
        $scope.aviso = [];
        $scope.par = [];
        $scope.AppSettings = AppSettings;
        $scope.parametrizacoes = [];
        $scope.dtHoje = 0;
        $scope.disabledSelect=false;
        $scope.situacoes = [
            {
                'id': 'A',
                'nome': 'Aberto'
            },
            {
                'id': 'F',
                'nome': 'Fechado'
            }
        ];
        $scope.dados = [
            {
                'tipo_documento': '',
                'item_checklist': ''
            },
        ];
        $scope.regex = '[0-9].*$';
        $scope.regexN = '^[0-9]$';
        $scope.regexT = '^((Sim|sim|SIM|Não|não|NÃO))$';
        $scope.parametro = {};
        $scope.parametro.unidades = [
            {
                'id': 1,
                'nome': 'Ano'
            },
            {
                'id': 2,
                'nome': 'Mês'
            },
            {
                'id': 3,
                'nome': 'Dia'
            }
        ];
        $scope.parametro.tipos = [
            {
                'id': 1,
                'nome': 'Texto'
            },
            {
                'id': 2,
                'nome': 'Numeral'
            }
        ];
        $scope.parametro.operacoes = [
            {
                'id': 1,
                'nome': '='
            },
            {
                'id': 2,
                'nome': '<'
            },
            {
                'id': 3,
                'nome': '>'
            },
            {
                'id': 4,
                'nome': '<>'
            },
            {
                'id': 5,
                'nome': '>='
            },
            {
                'id': 6,
                'nome': '<='
            },
        ];

        if (!$stateParams.id) {
            PremioService.recuperarPremio()
                    .then(function (dados) {
                        console.log("recuperarPremio 297")
                        console.log(dados);
                        $scope.dados = dados;
                        $scope.edicao = 0;
                    }).catch(function (err) {
            });
        } else {
            recuperarPremio(PremioService, $scope, $stateParams);
            setTimeout(function () {
                $('#fases').trigger("chosen:updated")
            }, 3000);
        }


        $scope.validarValParam = function (param) {
            var tipo = $scope.tipo === undefined ? jQuery('#tipos_chosen a span').text() : $scope.tipo.nome;
            if (param === false || tipo === 'selecione') {
                $scope.valor = '';
                return false
            } else {
                if (tipo === 'Numeral') {
                    var regexp = /^[0-9]*$/;
                    var result = regexp.test($scope.valor);
                    if(result === false){
                        $scope.valor = '';
                        return false
                    }
                }
                if (tipo === 'Texto') {
                    var regexp = /^([sim]{3}|[não]{3}|[nao]{3})$/i;

                    var result = regexp.test($scope.valor);
                    if(result === false){
                        $scope.valor = '';
                        return false
                    }
                }
            }
        }

        $scope.validaValor = (function () {
            return {
                test: function (value) {
                    if ($scope.validaValor === false) {
                        return true;
                    }
                    var tipo = $scope.tipo === undefined ? jQuery('#tipos_chosen a span').text() : $scope.tipo.nome;
                    var result = $scope.validaValor;
                    // if (tipo !== "" ) {
                    //     if (tipo === 'Numeral') {
                    //         var regexp = /^[0-9]*$/;
                    //         $scope.maxLength = '3'
                    //     }
                    //     if (tipo === 'Texto') {
                    //         var regexp = /^([sim]{0,3}|[não]{0,3}|[nao]{0,3})$/i;
                    //         $scope.maxLength = '3';
                    //     }
                    //     result = regexp.test(value);
                    // }else if ($scope.tipo.nome !== undefined){
                    //     if ($scope.tipo.nome === 'Numeral') {
                    //         var regexp = /^[0-9]*$/;
                    //         $scope.maxLength = '3'
                    //     }
                    //     if ($scope.tipo.nome === 'Texto') {
                    //         var regexp = /^([sim]{0,3}|[não]{0,3}|[nao]{0,3})$/i;
                    //         $scope.maxLength = '3';
                    //     }
                    //     result = regexp.test(value);
                    // }

                    if ((tipo === "" || tipo === undefined) && result === false) {
                        $scope.valor = "";
                        jQuery('#valor').val('')
                    }else{
                        if (tipo === 'Numeral') {
                            var regexp = /^[0-9]*$/;
                            $scope.maxLength = '3';
                            result = regexp.test(value);
                            if(result === false){
                                jQuery('#valor').val('');
                            }
                        }
                        if (tipo === 'Texto') {
                            var regexp = /^([sim]{0,3}|[não]{0,3}|[nao]{0,3})$/i;
                            $scope.maxLength = '3';
                            result = regexp.test(value);
                        }
                    }
                    return result;
                }
            };
        })();



        //Atender a regra : RNG046 - Parâmetro número comissão
        $scope.validarNumComissao = function (param, fase) {
            if ($scope.fases_premio.fases === undefined) {
                return false;
            } else {
                angular.forEach($scope.fases_premio.fases, function (value, key) {
                    $scope.dtAtual = value.dtHoje;
                    $scope.dtHoje = formtDataHoraComp(value.dtHoje);

                    //Inscrição
                    if (value.idFase === 1) {
                        $scope.dtIniInsc = value.inicio;
                    }
                    //Verificar
                    if (value.idFase === 3) {
                        $scope.dtIniVerif = value.inicio;
                    }
                    //Montar comissões
                    if (value.idFase === 4) {
                        $scope.dtIniCom = value.inicio;
                    }
                    //Analisar e emitir Parecer - 1ª Etapa
                    if (value.idFase === 5) {
                        $scope.dataIniEmiPrc1 = value.inicio;
                    }
                    //Emitir Parecer Final - 1ª Etapa
                    if (value.idFase === 6) {
                        $scope.dtIniPrcFin1 = value.inicio;
                    }
                    //Analisar e Emitir Parecer – 2ª Etapa
                    if (value.idFase === 7) {
                        $scope.dtIniEmiPrc2 = value.inicio;
                    }
                    //Analisar e Emitir Parecer Final – 2ª Etapa
                    if (value.idFase === 8) {
                        $scope.dtIniEmiPrcFin2 = value.inicio;
                    }

                });

                if (param.nome === "nr_tot_max_comissoes1" || param.nome === "nr_tot_min_comissoes1" || param.nome === "nr_tot_max_comissoes2" || param.nome === "nr_tot_min_comissoes2") {
                    if ($scope.dtIniVerif === undefined) {
                        var dataIni = 0;
                    } else {
                        var dataIni = formtDataHoraComp($scope.dtIniVerif);
                    }

                    if (dataIni < $scope.dtHoje) {
                        // toaster.pop('error', '', 'Na alteração do número (máximo e mínimo) de comissões só poderá ser feita enquanto não iniciar a análise de artigos.');
                        return false;
                    }
                    if (dataIni > $scope.dtHoje) {
                        return true;
                    }

                } else {
                    if (fase.id === 1) {
                        if ($scope.dtIniInsc === undefined) {
                            var dataIni = 0;
                        } else {
                            var dataIni = formtDataHoraComp($scope.dtIniInsc);
                        }
                        if (dataIni < $scope.dtHoje) {
                            // toaster.pop('error', '', 'Na alteração do número (máximo e mínimo) de comissões só poderá ser feita enquanto não iniciar a análise de artigos.');
                            return false;
                        }
                        if (dataIni > $scope.dtHoje) {
                            return true;
                        }

                    }
                    if (fase.id === 3) {
                        if ($scope.dtIniVerif === undefined) {
                            var dataIni = 0;
                        } else {
                            var dataIni = formtDataHoraComp($scope.dtIniVerif);
                        }
                        if (dataIni < $scope.dtHoje) {
                            // toaster.pop('error', '', 'Na alteração do número (máximo e mínimo) de comissões só poderá ser feita enquanto não iniciar a análise de artigos.');
                            return false;
                        }
                        if (dataIni > $scope.dtHoje) {
                            return true;
                        }

                    }
                    if (fase.id === 4) {
                        if ($scope.dtIniCom === undefined) {
                            var dataIni = 0;
                        } else {
                            var dataIni = formtDataHoraComp($scope.dtIniCom);
                        }
                        if (dataIni < $scope.dtHoje) {
                            // toaster.pop('error', '', 'Na alteração do número (máximo e mínimo) de comissões só poderá ser feita enquanto não iniciar a análise de artigos.');
                            return false;
                        }
                        if (dataIni > $scope.dtHoje) {
                            return true;
                        }

                    }

                    if (fase.id === 5) {
                        if ($scope.dataIniEmiPrc1 === undefined) {
                            var dataIni = 0;
                        } else {
                            var dataIni = formtDataHoraComp($scope.dataIniEmiPrc1);
                        }
                        if (dataIni < $scope.dtHoje) {
                            // toaster.pop('error', '', 'Na alteração do número (máximo e mínimo) de comissões só poderá ser feita enquanto não iniciar a análise de artigos.');
                            return false;
                        }
                        if (dataIni > $scope.dtHoje) {
                            return true;
                        }

                    }
                    if (fase.id === 6) {
                        if ($scope.dtIniPrcFin1 === undefined) {
                            var dataIni = 0;
                        } else {
                            var dataIni = formtDataHoraComp($scope.dtIniPrcFin1);
                        }
                        if (dataIni < $scope.dtHoje) {
                            // toaster.pop('error', '', 'Na alteração do número (máximo e mínimo) de comissões só poderá ser feita enquanto não iniciar a análise de artigos.');
                            return false;
                        }
                        if (dataIni > $scope.dtHoje) {
                            return true;
                        }

                    }
                    if (fase.id === 7) {
                        if ($scope.dtIniEmiPrc2 === undefined) {
                            var dataIni = 0;
                        } else {
                            var dataIni = formtDataHoraComp($scope.dtIniEmiPrc2);
                        }
                        if (dataIni < $scope.dtHoje) {
                            // toaster.pop('error', '', 'Na alteração do número (máximo e mínimo) de comissões só poderá ser feita enquanto não iniciar a análise de artigos.');
                            return false;
                        }
                        if (dataIni > $scope.dtHoje) {
                            return true;
                        }

                    }
                    if (fase.id === 8) {
                        if ($scope.dtIniEmiPrcFin2 === undefined) {
                            var dataIni = 0;
                        } else {
                            var dataIni = formtDataHoraComp($scope.dtIniEmiPrcFin2);
                        }
                        if (dataIni < $scope.dtHoje) {
                            // toaster.pop('error', '', 'Na alteração do número (máximo e mínimo) de comissões só poderá ser feita enquanto não iniciar a análise de artigos.');
                            return false;
                        }
                        if (dataIni > $scope.dtHoje) {
                            return true;
                        }

                    }
                }
            }

        }

        $scope.addParametroFormulario = function () {

            var exist_obj = false;
            var pos_old_obj = 0;
            var new_obj = {
                'fase_evento': $scope.fase_evento,
                'parametro': $scope.parametro,
                'unidade': $scope.unidade,
                'valor': $scope.valor,
                'tipo': $scope.tipo,
                'operacao': $scope.operacao,
                'situacao_parametro': $scope.situacao_parametro,
                'categoria_tematica': $scope.dados.categoria_tematica
            }
            console.log(new_obj);

            jQuery('#contem_fase_param').removeClass('has-error');
            jQuery('#contem_parametro_param').removeClass('has-error');
            jQuery('#contem_valor_param').removeClass('has-error');
            jQuery('#param_tipo').removeClass('has-error');
            jQuery('.chosen-container-single .chosen-single').css({border: "1px solid #ccc"});
            jQuery('#parametros_chosen').css({border: "1px solid #ccc"});
            jQuery('#tipos_chosen').css({border: "1px solid #ccc"});
            jQuery('#operacoes_chosen').css({border: "1px solid #ccc"});

            if ($scope.fase_evento === undefined || $scope.parametro.nome === undefined || $scope.valor === undefined || $scope.valor === "" || $scope.tipo === undefined || $scope.tipo.id === null || $scope.tipo === '' || $scope.operacao === undefined || $scope.operacao.id === null || $scope.operacao.id === undefined) {

                if ($scope.fase_evento === undefined ||$scope.fase_evento === "") {
                    jQuery('#fases_chosen').css({border: "1px solid #a94442"});
                }

                if ($scope.parametro.nome === undefined || $scope.parametro.nome === "") {
                    jQuery('#parametros_chosen').css({border: "1px solid #a94442"});
                }
                if ($scope.valor === undefined || $scope.valor === "") {
                    jQuery('#contem_valor_param').addClass('has-error');
                }

                if ($scope.tipo === undefined || $scope.tipo.id === null || $scope.tipo.id === undefined) {
                    jQuery('#tipos_chosen').css({border: "1px solid #a94442"});
                }
                if ($scope.operacao === undefined || $scope.operacao.id === null || $scope.operacao.id === undefined) {
                    jQuery('#operacoes_chosen').css({border: "1px solid #a94442"});
                }                
                toaster.pop('error', '', AppSettings.messages.MSG025);
                return;
            }


            $scope.fase_evento = '';
            $scope.parametro = '';
            $scope.valor = '';
            $scope.unidade = '';
            $scope.operacao = '';
            $scope.tipo = '';
            $scope.situacao_parametro = '';
            $scope.dados.categoria_tematica = '';

            $scope.disabledSelect = false;

            angular.forEach($scope.rowsParametros, function (parametro, indice) {
                if (parametro.fase_evento.id == new_obj.fase_evento.id &&
                        parametro.parametro.id == new_obj.parametro.id) {
                    exist_obj = true;
                    pos_old_obj = indice;
                }


            });
            if (exist_obj === false) {
                if ($scope.rowsParametros == null) {
                    $scope.rowsParametros = [];
                }
                $scope.rowsParametros.push(new_obj);
            } else {
                jQuery('.chosen-container-single .chosen-single').css({border: "1px solid #ccc"});
                var param_exist = false;
                $scope.rowsParametros.map(function (obj) {
                    if (obj.fase_evento.id == new_obj.fase_evento.id &&
                            obj.parametro.id == new_obj.parametro.id &&
                            obj.parametro.categoria == new_obj.parametro.categoria) {
                        param_exist = true;
                        obj.valor = new_obj.valor;
                        obj.unidade = new_obj.unidade;
                        obj.operacao = new_obj.operacao;
                        obj.tipo = new_obj.tipo;
                        obj.situacao_parametro = new_obj.situacao_parametro;
                        obj.categoria_tematica = new_obj.categoria_tematica;
                    }
                });
                console.log( $scope.rowsParametros);
                if (!param_exist) {
                    toaster.pop('error', '', AppSettings.messages.MSG030);
                }
            }
        }
        $scope.disabledInput = function () {
            $scope.disabledSelect=true;
        };

        $scope.editParametro = function (param) {
            $scope.fase_evento = param.fase_evento;
            $scope.parametro = param.parametro;
            $scope.valor = param.valor;
            $scope.unidade = param.unidade;
            $scope.operacao = param.operacao;
            $scope.tipo = param.tipo;
            if (param.situacao_parametro == "A") {
                param.situacao_parametro = true
            } else
            if (param.situacao_parametro == "I") {
                param.situacao_parametro = false
            }
            $scope.situacao_parametro = param.situacao_parametro;
        }

        $scope.addRow = function (list, selected_object) {
            var operacao = true;
            angular.forEach(list, function (row) {
                if (row['nome'] == selected_object.nome) {
                    operacao = false;
                }
            });

            if (operacao == true && selected_object != undefined) {
                list.push(selected_object);
            }
        }

        $scope.removeRow = function (list, id, tipo) {
            var index = -1;
            var comArr = eval(list);
            for (var i = 0; i < comArr.length; i++) {
                if (comArr[i].id === id) {
                    index = i;
                    break;
                }
            }
            switch (tipo) {
                case 1:
                    deleteTipoDocumentoEvento(
                            id, list, index, $scope, AppSettings,
                            $stateParams, PremioService);
                    break
                case 2:
                    deleteItemChecklistEvento(
                            id, list, index, $scope, AppSettings,
                            $stateParams, PremioService);
                    break
                case 3:
                    deleteCategoriaTematicaEvento(
                            id, list, index, CategoriaService, $scope,
                            AppSettings, $stateParams, PremioService);
                    break
            }
        };

        $scope.fecharAviso = function () {
            $scope.aviso = {
                show: false
            };
        }

        $scope.validaCampos = function () {
            validarCampos($scope, AppSettings, toaster);
        }

        $scope.salvar = function () {
            console.log($scope.enviar);
            if (validarCampos($scope, AppSettings, toaster)) {
                PremioService.salvar($scope.enviar)
                        .then(function (dados) {
                            if (dados.exist == true) {
                                toaster.pop('error', '', AppSettings.messages.MSG029);
                            } else {
                                if (dados.status == 'error') {
                                    toaster.pop('error', '', AppSettings.messages.MSG034);
                                } else {
                                    $rootScope.mostrarAviso = 1;
                                    $window.location.href = '#/calendario';
                                }
                            }
                        }).catch(function (err) {
                            switch (err.status) {
                                case 409:
                                    toaster.pop('error', 'Sistema', err.data.mensagem);
                                    break;
                                case 412:
                                    console.log(err);
                                    toaster.pop('error', 'Sistema', err.data.detail);
                                    break;
                                default:
                                    toaster.pop('error', 'Sistema', 'ocorreu um erro ao salvar');
                            }
                        });
            }
        }
        $scope.ativarDesativarSituacaoParametro = function (situacao) {
            if (situacao == 'A') {
                return true;
            } else {
                return false;
            }
        }
        $scope.getCategoria = function () {
            $scope.parametro.categoria = $scope.rowsTematicas.nome;
        }
    }];

controllersModule.controller('PremioCtrl', PremioController);


function formtDataCmp(dado) {

    var result = "";

    if (dado.length == 10) {
        result = dado.substr(6, 4) + dado.substr(3, 2) + dado.substr(0, 2);
    }
    return result;
}


function formtHoraCmp(dado) {

    var result = "";

    if (dado.length == 5) {
        result = dado.substr(0, 2) + dado.substr(3, 2);
    }
    if (dado.length == 8) {
        result = dado.substr(0, 2) + dado.substr(3, 2) + dado.substr(6, 2);
    }
    return result;
}

function formtDataHoraComp(dado) {

    var result = "";

    if (dado.length >= 10) {
        result = formtDataCmp(dado.substr(0, 10));
        if (dado.length > 10) {
            result += formtHoraCmp(dado.substr(11));
        }
    }
    return result;
}

function recuperarPremio(PremioService, $scope, $stateParams) {
    PremioService.recuperarPremio($stateParams.id)
            .then(function (dados) {
                console.log("recuperarPremio 601")
                console.log(dados);
                $scope.edicao = $stateParams.id;
                $scope.dados = dados;
                if (dados.itens != null) {
                    $scope.rowsChecklist = dados.itens;
                }

                if (dados.tipos_documentos != null) {
                    $scope.rowsDocs = dados.tipos_documentos;
                }

                if (dados.tipos_categorias != null) {
                    $scope.rowsTematicas = dados.tipos_categorias;
                }

                if (dados.rowsParametros == undefined) {
                    $scope.rowsParametros = dados.tipos_parametrizacoes;
                }

                $scope.parametrizacoes.fases = dados.parametrizacoes;
                $scope.fases_premio = dados.fases_premio;

                window.scrollTo(0, 0);
            }).catch(function (err) {
    });
}

function deleteCategoriaTematicaEvento(id, list, index, CategoriaService, $scope, AppSettings, $stateParams, PremioService) {
    list.splice(index, 1);
}

function deleteTipoDocumentoEvento(id, list, index, $scope, AppSettings, $stateParams, PremioService) {
    list.splice(index, 1);
}

function deleteItemChecklistEvento(id, list, index, $scope, AppSettings, $stateParams, PremioService) {
    list.splice(index, 1);
}

function validarCampos($scope, AppSettings, toaster) {
    var error = 0;

    jQuery('.chosen-container-single .chosen-single').css({border: "1px solid #ccc"});

    jQuery('#ano_even').removeClass('has-error');
    if (!$scope.dados.ano) {
        jQuery('#ano_even').addClass('has-error');
        error = 1;
    }
    jQuery('#ano_prem').removeClass('has-error');
    if (!$scope.dados.ano_premiacao) {
        jQuery('#ano_prem').addClass('has-error');
        error = 1;
    }
    jQuery('#nome_evento').removeClass('has-error');
    if (!$scope.dados.nome) {
        jQuery('#nome_evento').addClass('has-error');
        error = 1;
    }
    jQuery('#titulo_premio').removeClass('has-error');
    if (!$scope.dados.titulo) {
        jQuery('#titulo_premio').addClass('has-error');
        error = 1;
    }
//    jQuery('#descricao_evento').removeClass('has-error');
//    if (!$scope.dados.descricao) {
//        jQuery('#descricao_evento').addClass('has-error');
//        error = 1;
//    }
    jQuery('#situacao_chosen').css({border: "1px solid #ccc"});
    if (!$scope.dados.situacao) {
        jQuery('#situacao_chosen').css({border: "1px solid #a94442"});
        error = 1;
    }
    jQuery('#tipo_documento_chosen').css({border: "1px solid #ccc"});
    if ($scope.rowsDocs.length === 0) {
        jQuery('#tipo_documento_chosen').css({border: "1px solid #a94442"});
        error = 1;
    }
    jQuery('#item_cheklist_chosen').css({border: "1px solid #ccc"});
    if ($scope.rowsChecklist.length === 0) {
        jQuery('#item_cheklist_chosen').css({border: "1px solid #a94442"});
        error = 1;
    }
    jQuery('#categoria_tematica_chosen').css({border: "1px solid #ccc"});
    if ($scope.rowsTematicas.length === 0) {
        jQuery('#categoria_tematica_chosen').css({border: "1px solid #a94442"});
        error = 1;
    }
    if (error == 0) {
        if ($scope.dados.ano < 2015) {
            toaster.pop('error', '', AppSettings.messages.MSG033);
            jQuery('#ano_even').addClass('has-error');
            window.scrollTo(0, 0);
        } else if ($scope.dados.ano_premiacao < 2015) {
            toaster.pop('error', '', AppSettings.messages.MSG035);
            jQuery('#ano_prem').addClass('has-error');
            window.scrollTo(0, 0);
        } else {
            return true;
        }
    } else {
        toaster.pop('error', '', AppSettings.messages.MSG025);
    }
}
