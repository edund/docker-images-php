'use strict';

var controllersModule = require('../_index');

/**
 * @ngInject
 */
var EmitirParecerFinalController = ["$capesModal", "$rootScope", "$scope", "$window", "$location", "CommonService", "AppSettings", "EmitirParecerFinalService", "toaster", "DistribuicaoService", "ValidacaoService",
    function ($capesModal, $rootScope, $scope, $window, $location, CommonService, AppSettings, EmitirParecerFinalService, toaster, DistribuicaoService, ValidacaoService) {

        ValidacaoService.validaPeriodo(5)
                .then(function (validacao) {
                    if (validacao.validade == true) {
                        $location.url('/');
                    }
                }).catch(function (err) {
            $location.url('/');
        });

        $rootScope.rows = [];
        $rootScope.counter = 0;
        CommonService.isAccessProfile('emitirparecerfinal');

        $scope.carregarComissoes = function () {
            DistribuicaoService.recuperarGrupos($rootScope.dados.categoria.idTema, $rootScope.dados.etapa, 'presidente_comissao')
                    .then(function (dados) {

                        if (dados) {
                            $rootScope.comissoes = {
                                'itens': dados.grupos
                            };
                        }
                        $rootScope.dados.itens = {};
                        $scope.pagination = {
                            total: 0,
                            current: 1,
                            perPage: AppSettings.resultsPerPage,
                            limit: 0,
                            maxSize: 5
                        };

                    }).catch(function (err) {
                toaster.pop('error', 'Sistema', 'NÃ£o foi possivel recuperar a pessoa informada');
            });

        };

        $scope.fileuploadConf = {
            'options': {// passed into the Dropzone constructor
                'url': '/rest/emitir-parecer-final/file',
                'method': 'post',
                'maxFilesize': 8,
                'dictDefaultMessage': 'Clique aqui para enviar.',
                'headers': {"tipo-arquivo": 1, "id-pessoa": 14564},
                'maxFiles': 1,
                'acceptedFiles': "application/pdf,.pdf,.PDF",
                'addRemoveLinks': true,
                'dictResponseError': AppSettings.messages.MSG017,
                'dictInvalidFileType': AppSettings.messages.MSG008,
                init: function () {
                    this.on("addedfile", function (file) {
                        if (this.files[1] != null) {
                            this.removeFile(this.files[1]);
                        }
                        var removeButton = Dropzone.createElement("<a>Substituir Arquivo</a>");
                        var _this = this;
                        removeButton.addEventListener("click", function (e) {
                            e.preventDefault();
                            e.stopPropagation();
                            _this.removeFile(file);
                        });
                        file.previewElement.appendChild(removeButton);
                    });
                    this.on("error", function (file, msg, response) {
                        var dropzone = this;
                        file.previewElement.addEventListener("click", function (e) {
                            e.preventDefault();
                            e.stopPropagation();
                            dropzone.removeFile(file);
                        });

                        if (response !== undefined) {
                            $(file.previewElement).find('.dz-error-message > span').text(AppSettings.messages.MSG017);
                            return false;
                        }
                    });

                    this.on("success", function (file, response) {
                                EmitirParecerFinalService.recuperarDadosParaAnalise(false)
                                    .then(function (dados) {
                                        CommonService.isAccessModule(dados.acesso);
                                        $rootScope.dados = dados;

                                        $rootScope.comissoes = {
                                            'itens': dados.grupos
                                        };
                                        $rootScope.rows = dados.artigos_indicados;
                                        if (dados.permissao === false) {
                                            $rootScope.aviso = AppSettings.messages.MSG019;
                                            $location.path('/');
                                        }

                                        $scope.pagination = {
                                            total: dados.itens.length,
                                            current: 1,
                                            perPage: AppSettings.resultsPerPage,
                                            limit: 0,
                                            maxSize: 5
                                        };

                                        $scope.pageChanged = function () {
                                            if ($scope.pagination.current === 1) {
                                                $scope.pagination.limit = 0;
                                            } else {
                                                $scope.pagination.limit = (($scope.pagination.current * $scope.pagination.perPage) - $scope.pagination.perPage);
                                            }
                                        };

                                        $scope.$broadcast('angucomplete-alt:changeInput', 'ex4', 'Hello!');

                                    }).catch(function (err) {
                                        if (err.status === 500) {
                                            $window.location.href = '#/';
                                        }
                                    }
                                );
                                dropzone.removeAllFiles();
                    });
                }

            }

        };


        $scope.carregarAnaliseArtigos = function (comissao) {
            EmitirParecerFinalService.recuperarDadosParaAnalise(false, $rootScope.dados.categoria.idTema, $rootScope.dados.comissao.id)
                    .then(function (dados) {
                        CommonService.isAccessModule(dados.acesso);
                        $rootScope.dados = dados;
                        $rootScope.rows = dados.artigos_indicados;
                        if (dados.permissao === false) {
                            $rootScope.aviso = AppSettings.messages.MSG019;
                            $location.path('/');
                        }

                        $scope.pagination = {
                            total: dados.itens.length,
                            current: 1,
                            perPage: AppSettings.resultsPerPage,
                            limit: 0,
                            maxSize: 5
                        };

                        $scope.pageChanged = function () {
                            if ($scope.pagination.current === 1) {
                                $scope.pagination.limit = 0;
                            } else {
                                $scope.pagination.limit = (($scope.pagination.current * $scope.pagination.perPage) - $scope.pagination.perPage);
                            }
                        };

                        $scope.$broadcast('angucomplete-alt:changeInput', 'ex4', 'Hello!');

                    }).catch(function (err) {
                if (err.status === 500) {
                    $window.location.href = '#/';
                }
            }
            );
        };

        $scope.deleleFile = function (id) {
            EmitirParecerFinalService.deleteFile(id)
                    .then(function (dados) {
                        EmitirParecerFinalService.recuperarDadosParaAnalise(false)
                                .then(function (dados) {
                                    CommonService.isAccessModule(dados.acesso);
                                    $rootScope.dados = dados;
                                    $rootScope.comissoes = {
                                        'itens': dados.grupos
                                    };
                                    $rootScope.rows = dados.artigos_indicados;
                                    if (dados.permissao === false) {
                                        $rootScope.aviso = AppSettings.messages.MSG019;
                                        $location.path('/');
                                    }

                                    $scope.pagination = {
                                        total: dados.itens.length,
                                        current: 1,
                                        perPage: AppSettings.resultsPerPage,
                                        limit: 0,
                                        maxSize: 5
                                    };

                                    $scope.pageChanged = function () {
                                        if ($scope.pagination.current === 1) {
                                            $scope.pagination.limit = 0;
                                        } else {
                                            $scope.pagination.limit = (($scope.pagination.current * $scope.pagination.perPage) - $scope.pagination.perPage);
                                        }
                                    };

                                    $scope.$broadcast('angucomplete-alt:changeInput', 'ex4', 'Hello!');

                                }).catch(function (err) {
                            if (err.status === 500) {
                                $window.location.href = '#/';
                            }
                        }
                        );

                    }).catch(function (err) {
                if (err.status === 500) {
                    $window.location.href = '#/';
                }
                if (err.status === 402) {
                    toaster.pop('error', 'Serviço', AppSettings.messages.MSG017);
                }
            }
            );
        }



        EmitirParecerFinalService.recuperarDadosParaAnalise(false)
                .then(function (dados) {
                    CommonService.isAccessModule(dados.acesso);
                    $rootScope.dados = dados;
                    $rootScope.comissoes = {
                        'itens': dados.grupos
                    };
                    $rootScope.rows = dados.artigos_indicados;
                    if (dados.permissao === false) {
                        $rootScope.aviso = AppSettings.messages.MSG019;
                        $location.path('/');
                    }

                    $scope.pagination = {
                        total: dados.itens.length,
                        current: 1,
                        perPage: AppSettings.resultsPerPage,
                        limit: 0,
                        maxSize: 5
                    };

                    $scope.pageChanged = function () {
                        if ($scope.pagination.current === 1) {
                            $scope.pagination.limit = 0;
                        } else {
                            $scope.pagination.limit = (($scope.pagination.current * $scope.pagination.perPage) - $scope.pagination.perPage);
                        }
                    };

                    $scope.$broadcast('angucomplete-alt:changeInput', 'ex4', 'Hello!');

                }).catch(function (err) {
            if (err.status === 500) {
                $window.location.href = '#/';
            }
        }
        );

        var move = function (origin, destination) {
            var temp = $rootScope.rows[destination];
            $rootScope.rows[destination] = $rootScope.rows[origin];
            $rootScope.rows[origin] = temp;
        };

        $scope.moveUp = function (index) {
            move(index, index - 1);
        };

        $scope.moveDown = function (index) {
            move(index, index + 1);
        };

        $rootScope.teste = true;

        $scope.habilitarDesabilitarCampos = function (campo) {
            if (campo == 'titulo') {

                setTimeout(function () {
                    $('#t1_value').val('');
                }, 200);

                $rootScope.disableInputAutoComplete = {
                    'titulo': true,
                    'autor': false
                }
            } else {
                setTimeout(function () {
                    $('#t2_value').val('');
                }, 200);

                $rootScope.disableInputAutoComplete = {
                    'titulo': false,
                    'autor': true
                }
            }
        }

        $scope.addRow = function (artigo) {
            var operacao = true;
            angular.forEach($rootScope.rows, function (row) {
                if (row['titulo_artigo'] == artigo.description.titulo_artigo) {
                    operacao = false;
                }
            });

            setTimeout(function () {
                $('#t1_value').val('');
                $('#t2_value').val('');
            }, 200);
            $rootScope.disableInputAutoComplete = {
                'titulo': false,
                'autor': false
            }

            if ($rootScope.rows === undefined) {
                $rootScope.rows = [];
            }

            if (operacao === true) {
                $rootScope.rows.push(artigo.description);
                $rootScope.counter++;
            }
        }

        $scope.removeRow = function (name, id_inscricao, id_parecer) {
            var index = -1;
            var comArr = eval($rootScope.rows);
            for (var i = 0; i < comArr.length; i++) {
                if (comArr[i].name === name) {
                    index = i;
                    break;
                }
            }

            $capesModal.confirm({
                "text": AppSettings.messages.MSG011,
                "title": 'Exclusão',
                "dismissText": 'Voltar'
            }).then(function () {
                $rootScope.rows.splice(index, 1);
                EmitirParecerFinalService.removerIndicacao(id_inscricao, id_parecer)
                        .then(function (dados) {
                            $rootScope.dados = dados;

                            EmitirParecerFinalService.recuperarDadosParaAnalise(false)
                                    .then(function (dados) {
                                        CommonService.isAccessModule(dados.acesso);
                                        $rootScope.dados = dados;
                                        $rootScope.comissoes = {
                                            'itens': dados.grupos
                                        };
                                        $rootScope.rows = dados.artigos_indicados;
                                        if (dados.permissao === false) {
                                            $rootScope.aviso = AppSettings.messages.MSG019;
                                            $location.path('/');
                                        }

                                        $scope.pagination = {
                                            total: dados.itens.length,
                                            current: 1,
                                            perPage: AppSettings.resultsPerPage,
                                            limit: 0,
                                            maxSize: 5
                                        };

                                        $scope.pageChanged = function () {
                                            if ($scope.pagination.current === 1) {
                                                $scope.pagination.limit = 0;
                                            } else {
                                                $scope.pagination.limit = (($scope.pagination.current * $scope.pagination.perPage) - $scope.pagination.perPage);
                                            }
                                        };

                                        $scope.$broadcast('angucomplete-alt:changeInput', 'ex4', 'Hello!');

                                    }).catch(function (err) {
                                if (err.status === 500) {
                                    $window.location.href = '#/';
                                }
                            }
                            );
                        }).catch(function (err) {

                }
                );

            });
        };

        $scope.downloadAllFiles = function (idInscricao) {
            CommonService.isServiceDocument(idInscricao)
                    .then(function (reply) {
                        var url = '../rest/arquivo/download/insc-' + idInscricao;
                        $window.location.href = url;
                        return false;
                    }).catch(function (err) {
                if (err.status == 500 || err.status === 404) {
                    toaster.pop('error', 'Serviço', AppSettings.messages.MSG017);
                }
                if (err.status === 401) {
                    CommonService.redirectUserAccess(true);
                }
            }
            );
        };

        $scope.emitirParecerFinal = function () {
            $scope.dados.enviado = false;
            EmitirParecerFinalService.salvar($scope.emitir)
                    .then(function (dados) {
                        $scope.aviso = {
                            show: true,
                            type: 'success',
                            msg: AppSettings.messages.MSG002
                        };
                        $rootScope.dados = dados;
                        //window.scrollTo(0, 0);
                        //window.location.href = '/pcn/#/emitirparecerfinal';
                        EmitirParecerFinalService.recuperarDadosParaAnalise(false)
                                .then(function (dados) {
                                    CommonService.isAccessModule(dados.acesso);
                                    $rootScope.dados = dados;
                                    $rootScope.comissoes = {
                                        'itens': dados.grupos
                                    };
                                    $rootScope.rows = dados.artigos_indicados;
                                    if (dados.permissao === false) {
                                        $rootScope.aviso = AppSettings.messages.MSG019;
                                        $location.path('/');
                                    }

                                    $scope.pagination = {
                                        total: dados.itens.length,
                                        current: 1,
                                        perPage: AppSettings.resultsPerPage,
                                        limit: 0,
                                        maxSize: 5
                                    };

                                    $scope.pageChanged = function () {
                                        if ($scope.pagination.current === 1) {
                                            $scope.pagination.limit = 0;
                                        } else {
                                            $scope.pagination.limit = (($scope.pagination.current * $scope.pagination.perPage) - $scope.pagination.perPage);
                                        }
                                    };

                                    $scope.$broadcast('angucomplete-alt:changeInput', 'ex4', 'Hello!');


                                }).catch(function (err) {
                            if (err.status === 500) {
                                $window.location.href = '#/';
                            }
                        }
                        );

                    }).catch(function (err) {
            });
        };

        $scope.enviar = function () {
            $capesModal.confirm({
                "text": AppSettings.messages.MSG018,
                "title": 'Confirmação',
                "dismissText": 'Voltar'
            }).then(function () {
                $scope.emitir.enviado = true;
                EmitirParecerFinalService.salvar($scope.emitir)
                        .then(function (dados) {
                            $scope.aviso = {
                                show: true,
                                type: 'success',
                                msg: AppSettings.messages.MSG002
                            };

                            $scope.emitir.enviado = false;
                            $rootScope.dados = dados;
                            window.scrollTo(0, 0);
                            //location.href = '#/emitirparecerfinal';

                            EmitirParecerFinalService.recuperarDadosParaAnalise(false)
                                    .then(function (dados) {
                                        CommonService.isAccessModule(dados.acesso);
                                        $rootScope.dados = dados;
                                        $rootScope.comissoes = {
                                            'itens': dados.grupos
                                        };
                                        $rootScope.rows = dados.artigos_indicados;
                                        if (dados.permissao === false) {
                                            $rootScope.aviso = AppSettings.messages.MSG019;
                                            $location.path('/');
                                        }

                                        $scope.pagination = {
                                            total: dados.itens.length,
                                            current: 1,
                                            perPage: AppSettings.resultsPerPage,
                                            limit: 0,
                                            maxSize: 5
                                        };

                                        $scope.pageChanged = function () {
                                            if ($scope.pagination.current === 1) {
                                                $scope.pagination.limit = 0;
                                            } else {
                                                $scope.pagination.limit = (($scope.pagination.current * $scope.pagination.perPage) - $scope.pagination.perPage);
                                            }
                                        };

                                        $scope.$broadcast('angucomplete-alt:changeInput', 'ex4', 'Hello!');


                                    }).catch(function (err) {
                                if (err.status === 500) {
                                    $window.location.href = '#/';
                                }
                            }
                            );
                        }).catch(function (err) {
                });

            });
        }
    }];

controllersModule.controller('EmitirParecerFinalCtrl', EmitirParecerFinalController);