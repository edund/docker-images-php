'use strict';

var controllersModule = require('../../_index');

/**
 * @ngInject
 */
var EmitirParecerFinalSegundaFaseController = ["$capesModal", "$rootScope", "$scope", "$window", "$location", "CommonService", "AppSettings", "EmitirParecerFinalService", "toaster", "DistribuicaoService", "ValidacaoService",
    function ($capesModal, $rootScope, $scope, $window, $location, CommonService, AppSettings, EmitirParecerFinalService, toaster, DistribuicaoService, ValidacaoService) {
        $rootScope.rows = [];
        $rootScope.counter = 0;
        $rootScope.aviso = [];
        $rootScope.indicacoes = {
            'itens': [
                {
                    'id': 'M',
                    'nome': 'Menção Honrosa'
                },
                {
                    'id': 'P',
                    'nome': 'Prêmio'
                }
            ]
        };
        CommonService.isAccessProfile('emitirparecerfinalsegundafase');
        ValidacaoService.validaPeriodo(7)
            .then(function (validacao) {
                if (validacao.validade == true) {
                    $location.url('/');
                }
                if (validacao.validaParam !== true) {
                    $rootScope.parametroInativo = 'ok';
                    $rootScope.param = validacao.validaParam;
                    $location.url('/');
                } else {
                    $rootScope.parametroInativo = 'error';
                    $rootScope.param = {};
                }
            }).catch(function (err) {
            $location.url('/');
        });
        $scope.fileuploadConf = {
            'options': {// passed into the Dropzone constructor
                'url': '/rest/emitir-parecer-final/file',
                'method': 'post',
                'maxFilesize': 8,
                'dictDefaultMessage': 'Clique aqui para enviar.',
                'headers': {"tipo-arquivo": 1, "id-pessoa": 14564},
                'maxFiles': 1,
                'acceptedFiles': "application/pdf,.pdf,.PDF",
                'addRemoveLinks': true,
                'dictInvalidFileType': AppSettings.messages.MSG008,
                init: function () {
                    this.on("addedfile", function (file) {
                        if (this.files[1] != null) {
                            this.removeFile(this.files[1]);
                        }
                        var removeButton = Dropzone.createElement("<a>Substituir Arquivo</a>");
                        var _this = this;
                        removeButton.addEventListener("click", function (e) {
                            e.preventDefault();
                            e.stopPropagation();
                            _this.removeFile(file);
                        });
                        file.previewElement.appendChild(removeButton);
                    });
                    this.on("error", function (file, msg, response) {
                        var dropzone = this;
                        file.previewElement.addEventListener("click", function (e) {
                            e.preventDefault();
                            e.stopPropagation();
                            dropzone.removeFile(file);
                        });

                        if (response !== undefined) {
                            $(file.previewElement).find('.dz-error-message > span').text(AppSettings.messages.MSG017);
                            return false;
                        }
                    });

                    this.on("success", function (file, response) {
                        EmitirParecerFinalService.recuperarDadosParaAnalise(true, $rootScope.dados.categoria.idTema, $rootScope.dados.comissao.id)
                                    .then(function (dados) {
                                        CommonService.isAccessModule(dados.acesso);
                                        $rootScope.dados = dados;
                                        $rootScope.rows = dados.artigos_indicados;
                                        if (dados.permissao === false) {
                                            $rootScope.aviso = AppSettings.messages.MSG019;
                                            $location.path('/');
                                        }

                                        $scope.pagination = {
                                            total: dados.itens.length,
                                            current: 1,
                                            perPage: AppSettings.resultsPerPage,
                                            limit: 0,
                                            maxSize: 5
                                        };

                                        $scope.pageChanged = function () {
                                            if ($scope.pagination.current === 1) {
                                                $scope.pagination.limit = 0;
                                            } else {
                                                $scope.pagination.limit = (($scope.pagination.current * $scope.pagination.perPage) - $scope.pagination.perPage);
                                            }
                                        };

                                        $scope.$broadcast('angucomplete-alt:changeInput', 'ex4', 'Hello!');

                            }).catch(function (err) {
                                if (err.status === 500) {
                                    $window.location.href = '#/';
                                }
                            }
                        );
                        dropzone.removeAllFiles();
                    });
                }
            }

        };


        $scope.carregarComissoes = function () {
            DistribuicaoService.recuperarGrupos($rootScope.dados.categoria.idTema, $rootScope.dados.etapa, 'membro_comissao')
                .then(function (dados) {

                    if (dados) {
                        $rootScope.comissoes = {
                            'itens': dados.grupos
                        };
                    }
                    $rootScope.dados.itens = {};
                    $scope.pagination = {
                        total: 0,
                        current: 1,
                        perPage: AppSettings.resultsPerPage,
                        limit: 0,
                        maxSize: 5
                    };
                }).catch(function (err) {
                toaster.pop('error', 'Sistema', 'Não foi possivel recuperar a pessoa informada');
            });

        };

        $scope.deleleFile = function (id) {
            EmitirParecerFinalService.deleteFile(id)
                .then(function (dados) {
                    EmitirParecerFinalService.recuperarDadosParaAnalise(true)
                        .then(function (dados) {
                            CommonService.isAccessModule(dados.acesso);
                            $rootScope.dados = dados;
                            $rootScope.comissoes = {
                                'itens': dados.grupos
                            };
                            $rootScope.rows = dados.artigos_indicados;
                            if (dados.permissao === false) {
                                $rootScope.aviso = AppSettings.messages.MSG019;
                                $location.path('/');
                            }

                            $scope.pagination = {
                                total: dados.itens.length,
                                current: 1,
                                perPage: AppSettings.resultsPerPage,
                                limit: 0,
                                maxSize: 5
                            };

                            $scope.pageChanged = function () {
                                if ($scope.pagination.current === 1) {
                                    $scope.pagination.limit = 0;
                                } else {
                                    $scope.pagination.limit = (($scope.pagination.current * $scope.pagination.perPage) - $scope.pagination.perPage);
                                }
                            };

                            $scope.$broadcast('angucomplete-alt:changeInput', 'ex4', 'Hello!');

                        }).catch(function (err) {
                        if (err.status === 500) {
                            $window.location.href = '#/';
                        }
                    }
                    );

                }).catch(function (err) {
                if (err.status === 500) {
                    $window.location.href = '#/';
                }
            }
            );
        }

        $scope.carregarAnaliseArtigos = function (comissao) {
            EmitirParecerFinalService.recuperarDadosParaAnalise(true, $rootScope.dados.categoria.idTema, $rootScope.dados.comissao.id)
                .then(function (dados) {
                    CommonService.isAccessModule(dados.acesso);

                    $rootScope.dados = dados;
                    $rootScope.rows = dados.artigos_indicados;
                    $rootScope.comissoes = {
                        'itens': dados.grupos
                    };

                    if (dados.parametrizacao != undefined) {
                        CommonService.isParametrizacao(dados.parametrizacao, $scope);
                    }

                    if (dados.permissao === false) {
                        $rootScope.aviso = AppSettings.messages.MSG019;
                        $location.path('/');
                    }

                    $scope.pagination = {
                        total: dados.itens.length,
                        current: 1,
                        perPage: AppSettings.resultsPerPage,
                        limit: 0,
                        maxSize: 5
                    };

                    $scope.pageChanged = function () {
                        if ($scope.pagination.current === 1) {
                            $scope.pagination.limit = 0;
                        } else {
                            $scope.pagination.limit = (($scope.pagination.current * $scope.pagination.perPage) - $scope.pagination.perPage);
                        }
                    };

                    $scope.$broadcast('angucomplete-alt:changeInput', 'ex4', 'Hello!');

                }).catch(function (err) {
                if (err.status === 500) {
                    $window.location.href = '#/';
                }
            }
            );
        };

        EmitirParecerFinalService.recuperarDadosParaAnalise(true)
            .then(function (dados) {
                CommonService.isAccessModule(dados.acesso);

                $rootScope.dados = dados;
                $rootScope.rows = dados.artigos_indicados;
                $rootScope.comissoes = {
                    'itens': dados.grupos
                };

                if (dados.parametrizacao != undefined) {
                    CommonService.isParametrizacao(dados.parametrizacao, $scope);
                }

                if (dados.permissao === false) {
                    $rootScope.aviso = AppSettings.messages.MSG019;
                    $location.path('/');
                }

                $scope.pagination = {
                    total: dados.itens.length,
                    current: 1,
                    perPage: AppSettings.resultsPerPage,
                    limit: 0,
                    maxSize: 5
                };

                $scope.pageChanged = function () {
                    if ($scope.pagination.current === 1) {
                        $scope.pagination.limit = 0;
                    } else {
                        $scope.pagination.limit = (($scope.pagination.current * $scope.pagination.perPage) - $scope.pagination.perPage);
                    }
                };

                $scope.$broadcast('angucomplete-alt:changeInput', 'ex4', 'Hello!');

            }).catch(function (err) {
            if (err.status === 500) {
                $window.location.href = '#/';
            }
        }
        );

        $rootScope.init = function (indice, valor) {
            setTimeout(function () {
                $('#indicacao' + indice).val(valor);
                $('#indicacao' + indice).trigger("chosen:updated");
            }, 200);
        }

        var move = function (origin, destination) {
            var temp = $rootScope.rows[destination];
            $rootScope.rows[destination] = $rootScope.rows[origin];
            $rootScope.rows[origin] = temp;
        };

        $scope.moveUp = function (index) {
            move(index, index - 1);
        };

        $scope.moveDown = function (index) {
            move(index, index + 1);
        };

        $rootScope.teste = true;

        $scope.habilitarDesabilitarCampos = function (campo) {
            if (campo == 'titulo') {

                setTimeout(function () {
                    $('#t1_value').val('');
                }, 200);

                $rootScope.disableInputAutoComplete = {
                    'titulo': true,
                    'autor': false
                }
            } else {
                setTimeout(function () {
                    $('#t2_value').val('');
                }, 200);

                $rootScope.disableInputAutoComplete = {
                    'titulo': false,
                    'autor': true
                }
            }
        }

        $scope.addRow = function (artigo) {
            var operacao = true;
            angular.forEach($rootScope.rows, function (row) {
                if (row['titulo_artigo'] == artigo.description.titulo_artigo) {
                    operacao = false;
                }
            });

            setTimeout(function () {
                $('#t1_value').val('');
                $('#t2_value').val('');
            }, 200);
            $rootScope.disableInputAutoComplete = {
                'titulo': false,
                'autor': false
            }

            if ($rootScope.rows === undefined) {
                $rootScope.rows = [];
            }

            if (operacao === true) {
                $rootScope.rows.push(artigo.description);
                $rootScope.counter++;
            }
        }

        $scope.removeRow = function (name, id_inscricao, id_parecer) {

            $capesModal.confirm({
                "text": AppSettings.messages.MSG011,
                "title": 'Confirmação',
                "dismissText": 'Voltar'
            }).then(function () {
                var arrExAux = $rootScope.rows;
                $rootScope.rows = [];
                var l = 0;
                angular.forEach(arrExAux, function (row) {
                    if (row.id_inscricao != id_inscricao) {
                        $rootScope.rows[l] = row;
                        l++;
                    }
                });
                if (id_parecer != undefined) {
                    EmitirParecerFinalService.removerIndicacao(id_inscricao, id_parecer)
                        .then(function (dados) {
                            $rootScope.dados = dados;
                            $rootScope.dados.enviado = 'Não enviado';
                            $window.location.reload();
                        }).catch(function (err) {
                    }
                    );
                }
            });
        };

        $scope.downloadAllFiles = function (idInscricao) {
            CommonService.isServiceDocument(idInscricao)
                .then(function (reply) {
                    var url = '../rest/arquivo/download/insc-' + idInscricao;
                    $window.location.href = url;
                    return false;

                }).catch(function (err) {
                if (err.status == 500 || err.status === 404) {
                    toaster.pop('error', 'Serviço', AppSettings.messages.MSG017);
                }
                if (err.status === 401) {
                    CommonService.redirectUserAccess(true);
                }
            }
            );
        };

        $scope.emitirParecerFinal = function () {

            EmitirParecerFinalService.salvar($scope.emitir)
                .then(function (dados) {
                    $rootScope.aviso = {
                        show: true,
                        type: 'success',
                        msg: AppSettings.messages.MSG002
                    };

                    $rootScope.dados = dados;
                    $rootScope.dados.enviado = 'Não enviado';
                    //$location.path($location.path())
                    //window.scrollTo(0, 0);
                    EmitirParecerFinalService.recuperarDadosParaAnalise(true)
                        .then(function (dados) {
                            CommonService.isAccessModule(dados.acesso);

                            $rootScope.dados = dados;
                            $rootScope.rows = dados.artigos_indicados;
                            $rootScope.comissoes = {
                                'itens': dados.grupos
                            };
                            if (dados.permissao === false) {
                                $rootScope.aviso = AppSettings.messages.MSG019;
                                $location.path('/');
                            }

                            $scope.pagination = {
                                total: dados.itens.length,
                                current: 1,
                                perPage: AppSettings.resultsPerPage,
                                limit: 0,
                                maxSize: 5
                            };

                            $scope.pageChanged = function () {
                                if ($scope.pagination.current === 1) {
                                    $scope.pagination.limit = 0;
                                } else {
                                    $scope.pagination.limit = (($scope.pagination.current * $scope.pagination.perPage) - $scope.pagination.perPage);
                                }
                            };

                            $scope.$broadcast('angucomplete-alt:changeInput', 'ex4', 'Hello!');

                        }).catch(function (err) {
                        if (err.status === 500) {
                            $window.location.href = '#/';
                        }
                    }
                    );
                    //$window.location.reload();

                }).catch(function (err) {
                EmitirParecerFinalService.recuperarDadosParaAnalise(true)
                    .then(function (dados) {
                        CommonService.isAccessModule(dados.acesso);

                        $rootScope.dados = dados;
                        $rootScope.rows = dados.artigos_indicados;
                        $rootScope.comissoes = {
                            'itens': dados.grupos
                        };
                        if (dados.permissao === false) {
                            $rootScope.aviso = AppSettings.messages.MSG019;
                            $location.path('/');
                        }

                        $scope.pagination = {
                            total: dados.itens.length,
                            current: 1,
                            perPage: AppSettings.resultsPerPage,
                            limit: 0,
                            maxSize: 5
                        };

                        $scope.pageChanged = function () {
                            if ($scope.pagination.current === 1) {
                                $scope.pagination.limit = 0;
                            } else {
                                $scope.pagination.limit = (($scope.pagination.current * $scope.pagination.perPage) - $scope.pagination.perPage);
                            }
                        };

                        $scope.$broadcast('angucomplete-alt:changeInput', 'ex4', 'Hello!');

                    }).catch(function (err) {
                    if (err.status === 500) {
                        $window.location.href = '#/';
                    }
                }
                );
            });
        };


        $scope.validar_finalizar = function (indicacao) {
            var i = 0;

            if ($rootScope.dados.indicacao == true) {
                angular.forEach($scope.emitir, function (row, index) {
                    if (index.indexOf('indica') >= 0) {
                        if ($("#" + index + " option:selected").val() == '') {
                            i++;
                        }
                    }
                });
                if (i > 0) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }

        $scope.imprimir = function () {
            if ($rootScope.dados.artigos_indicados == undefined) {
                toaster.pop('info', 'Relatório', AppSettings.messages.MSG021);
                return false;
            }

            var url = '../rest/emitir-parecer-final?print=true&segundaFase=true';
            $window.location.href = url;
        }

        $scope.fecharAviso = function (index) {
            if (index === undefined) {
                $scope.aviso = null;
            } else {
                $scope.aviso[index] = {
                    show: false
                };

                //$scope.aviso.splice(index, 1);
            }
        };

        $scope.finalizar = function () {
            $capesModal.confirm({
                "text": AppSettings.messages.MSG018,
                "title": 'Confirmação',
                "dismissText": 'Voltar'
            }).then(function () {
                $scope.emitir.enviado = false;

                if ($scope.emitir.artigo1 == undefined) {
                    $scope.aviso = {
                        show: true,
                        type: 'danger',
                        msg: AppSettings.messages.MSG023
                    };
                    //$rootScope.dados = dados;
                    window.scrollTo(0, 0);

                } else {
                    $scope.emitir.enviado = true;
                    EmitirParecerFinalService.salvar($scope.emitir)
                        .then(function (dados) {
                            $scope.aviso = {
                                show: true,
                                type: 'success',
                                msg: AppSettings.messages.MSG002
                            };
                            $rootScope.dados = dados;
                            window.scrollTo(0, 0);
                            //$window.location.href = '#/emitirparecerfinalsegundafase';
                            //$window.location.reload();
                            $location.path($location.path());

                        }).catch(function (err) {
                    });
                }
            });
        }

        $scope.reordenarArtigos = function () {
            var rowAux = $rootScope.rows;
            var rowAux2 = [];
            $rootScope.rows = [];
            var i = 0;
            var j = 0;
            angular.forEach(rowAux, function (row) {
                if (row.indicacao_premio == null) {
                    j = i + 60;
                    rowAux2[j] = row;
                } else {
                    if (row.indicacao_premio == "P" || row.indicacao_premio.id == "P") {
                        rowAux2[i] = row;
                    } else {
                        j = i + 30;
                        rowAux2[j] = row;
                    }
                }
                i++;
            });
            var k = 0;
            angular.forEach(rowAux2, function (row) {
                $rootScope.rows[k] = row;
                k++;
            });
        }

    }];
controllersModule.controller('EmitirParecerFinalSegundaFaseCtrl', EmitirParecerFinalSegundaFaseController);