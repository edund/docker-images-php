'use strict';

var controllersModule = require('../_index');


/**
 * @ngInject
 */
var AvaliacaoArtigosController = ["$scope", "$rootScope", "DistribuicaoService", "toaster", "AppSettings", "$capesModal", "CommonService", "GerenciarInstrucaoAvaliacaoService", "IncluirAvaliacaoService",
    function ($scope, $rootScope, DistribuicaoService, toaster, AppSettings, $capesModal, CommonService, GerenciarInstrucaoAvaliacaoService, IncluirAvaliacaoService) {


        $scope.aviso = {}
        /**
         * Categorias
         */
        $scope.categorias = [];
        $scope.categoria = {
            'selecionado': null
        };

        /**
         * Grupos
         */
        $scope.grupos = [];
        $scope.grupo = {
            'selecionado': null
        };

        $scope.form = {
            anoPremio: null,
            categoriaTematica: null,
            comissaoAdhoc: null
        }
        $scope.avaliacoes = [];

        var fnBuscarAvaliacoes = function (params) {

            IncluirAvaliacaoService.recuperaAvaliacao(params)
                    .then(function (data) {
                        if (typeof(data._embedded) !== 'undefined') {
                            $scope.avaliacoes = data._embedded.incluir_avaliacao;
                        }
                    })
                    .catch(function (err) {
                        toaster.pop('error', 'Sistema', 'Não foi possivel recuperar as avaliações cadastradas');
                    });
        };


        DistribuicaoService.recuperarCategoriasTematicas(1)
                .then(function (dados) {

                    $scope.categorias.itens = dados.categorias;

                }).catch(function (err) {
            if (err.status === 401) {
                CommonService.redirectUserAccess(true);
            }
        });

        GerenciarInstrucaoAvaliacaoService.recuperarDocumentos()
                .then(function (dados) {
                    if (typeof(dados._embedded) !== 'undefined') {
                        $scope.documentos = dados._embedded.instrucao_avaliacao;
                        //$('#areaavaliacao').trigger("chosen:updated");
                    }
                }).catch(function (err) {
            toaster.pop('error', 'Sistema', 'Não foi possivel recuperar a pessoa informada');
        });

        $scope.carregarGrupos = function () {

            if ($scope.categoria.selecionado == null)
                return false;

            //@todo segundo parametro eh a etapa
            DistribuicaoService.recuperarGrupos($scope.categoria.selecionado.idTema)
                    .then(function (dados) {

                        CommonService.isAccessProfile('verificar');
                        if (dados) {
                            $scope.grupos.itens = dados.grupos;
                        }
                        $scope.form.comissaoAdhoc = null;

                    }).catch(function (err) {
                toaster.pop('error', 'Sistema', 'Não foi possivel recuperar a pessoa informada');
            });


        };

        $scope.buscar = function () {
            if ($scope.categoria.selecionado !== undefined && $scope.categoria.selecionado !== null)
                $scope.form.categoriaTematica = $scope.categoria.selecionado.idTema;

            if ($scope.grupo.selecionado !== undefined && $scope.grupo.selecionado !== null)
                $scope.form.comissaoAdhoc = $scope.grupo.selecionado.id;


            fnBuscarAvaliacoes($scope.form);
        };

        //fnBuscarAvaliacoes();

    }];

controllersModule.controller('AvaliacaoArtigosCtrl', AvaliacaoArtigosController);