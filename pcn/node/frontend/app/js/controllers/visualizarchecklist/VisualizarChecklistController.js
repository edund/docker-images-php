'use strict';

var controllersModule = require('../_index');

/**
 * @ngInject
 */
var VisualizarChecklistController = ["toaster", "$scope", "$rootScope", "$capesModal",
    "AppSettings", "CommonService", "$location", "$stateParams", "VisualizarChecklistService",
    function (toaster, $scope, $rootScope, $capesModal, AppSettings,
        CommonService, $location, $stateParams, VisualizarChecklistService) {

        $scope.fecharAviso = function ()
        {
            $scope.aviso = {
                show: false
            };
        }

        $scope.show_result = false;
        $scope.aviso = {
            'show': false,
            'message': null,
        };

        recuperarCombo(VisualizarChecklistService, $scope, toaster, AppSettings, $stateParams, CommonService);

        $scope.search = function () {
            resultSearch($scope, AppSettings, VisualizarChecklistService, toaster);
        };
    }];

controllersModule.controller('VisualizarChecklistCtrl', VisualizarChecklistController);

function resultSearch($scope, AppSettings, VisualizarChecklistService, toaster) {

    jQuery('#ano_chosen').css({border: "1px solid #ccc"});
    if ($scope.ano === undefined || $scope.ano.id === null || $scope.ano.id === undefined) {
        jQuery('#ano_chosen').css({border: "1px solid #a94442"});
        toaster.pop('error', '', AppSettings.messages.MSG025);
        return;
    } else {
        VisualizarChecklistService.pesquisar($scope.pesquisar, 1)
            .then(function (dados) {
                $scope.result_search = dados.result_search;
                $scope.itens_verificacao = dados.itens_verificacao;
                $scope.itens_verificacao_opcoes = dados.itens_verificacao_opcoes;
                $scope.rtAno = dados.ano;
                if ($scope.result_search.length > 0) {
                    $scope.pagination = {
                        total: dados.totalRegistros,
                        current: 1,
                        perPage: 10,
                        maxSize: 5
                    };
                    $scope.pageChanged = function () {
                        VisualizarChecklistService.pesquisar($scope.pesquisar, $scope.pagination.current)
                            .then(function (dados) {
                                $scope.result_search = dados.result_search;
                                $scope.itens_verificacao = dados.itens_verificacao;
                                $scope.itens_verificacao_opcoes = dados.itens_verificacao_opcoes;
                                $scope.rtAno = dados.ano;
                            }).catch(function (err) {
                        });
                    };

                    $scope.show_result = true;
                    $scope.aviso.show = false;
                } else {
                    $scope.show_result = false;
                    $scope.msg = AppSettings.messages.MSG012;
                    $scope.aviso.type = 'danger';
                    $scope.aviso.show = true;
                    $scope.aviso.msg = AppSettings.messages.MSG012;
                }
            }).catch(function (err) {
        });
    }

}

function recuperarCombo(VisualizarChecklistService, $scope, toaster, AppSettings, $stateParams, CommonService)
{
    VisualizarChecklistService.retornoCombo()
        .then(function (retorno) {
            $scope.itens = retorno.itens;
            $scope.AppSettings = AppSettings;
        }).catch(function (err) {
        if (err.status === 401) {
            CommonService.redirectUserAccess(true);
        } else {
            toaster.pop('error', 'Sistema', 'Não foi possivel recuperar a informação');

        }
    });
}