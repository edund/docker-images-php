'use strict';

var controllersModule = require('../_index');

/**
 * @ngInject
 */
var SegundoPassoController = ["$scope","$rootScope","$capesModal","$window",
    "$stateParams", "$anchorScroll","$location","CommonService","PessoasService",
    "ArtigoService","CategoriaService","PeriodicoService", "DadosAutorService",
    "EnderecoService","toaster","AppSettings", "PCNInscricaoService", "$state",
    function ($scope,$rootScope,$capesModal,$window,$stateParams,$anchorScroll,
              $location,CommonService,PessoasService,ArtigoService,CategoriaService,
              PeriodicoService, DadosAutorService, EnderecoService ,toaster, AppSettings,
              PCNInscricaoService, $state)
    {

        $scope.linkLattes = null;
        $scope.aviso = [];
        $scope.pessoa = {
        };

        $scope.inscricao = {

        };

        //backup para quando o usuário clica em trocar endereço
        //com isso os dados são mantidos em caso de "decidi voltar o endereço"
        $scope.tempData = {
          enderecos : {
            NACIONAL : null,
            EXTERIOR : null
          }
        };

        $scope.formPessoa = {
          tipoEndereco: 'NACIONAL',
          cep: '',
          estados: [],
          municipios: []
        };

        $scope.formArtigo = {
          categorias : [],
          titulos_periodicos: []
          //declaracao : false
        }

        CommonService.isEndInscription();

        $scope.fecharAviso = function () {
          $scope.aviso = { show: false };
        };

        $scope.__init = function() {
          //1. dados da pessoa logada
          //1.1 dados pessoais
          //1.2 endereços

          //2. dados da inscrição
          $scope.__carregar_inscricao();

          $scope.__carregarCategoriasTematicas();
        };

        //troca o tipo de form endereço e mantém um bkp do endereço anterior
        $scope.changeFormEndereco = function(tipoEndereco) {
          console.log('cfasfdasdfsd');
          // console.log(tipoEndereco);
          // console.log(angular.isObject(tipoEndereco));
          $scope.formPessoa.estados = [];
          $scope.formPessoa.municipios = [];
          $scope.pessoa.endereco_principal = [];
          console.log(tipoEndereco);
          // $scope.updateTempEnderecos(tipoEndereco);
          console.log($scope.tempData.enderecos);

          if (tipoEndereco == 'NACIONAL') {
            $scope.pessoa.endereco_principal = $scope.tempData.enderecos['NACIONAL'];
            $scope.formPessoa.municipios = [];
          } else {
            $scope.pessoa.endereco_principal = $scope.tempData.enderecos['EXTERIOR'];
            var callbacks = [
              function() {
                $scope.consultaMunicipios($scope.tempData.enderecos['EXTERIOR'].municipio.uf);
              }
            ];
            $scope.consultaEstados($scope.tempData.enderecos['EXTERIOR'].municipio.uf.pais, callbacks);
          }
          if (!angular.isObject(tipoEndereco)) return false;
          var indiceEnderecoPrincipal = Object.keys($scope.pessoa.endereco_principal);
          var id = $scope.pessoa.endereco_principal[indiceEnderecoPrincipal].id;
          var bkpTipoEndereco = (tipoEndereco == 'NACIONAL') ? 'EXTERIOR' : 'NACIONAL';

          if ($scope.tempData.enderecos[tipoEndereco] != null) {
            $scope.pessoa.endereco_principal = $scope.tempData.enderecos[tipoEndereco];
          } else {
            $scope.tempData.enderecos[bkpTipoEndereco] = $scope.pessoa.endereco_principal[indiceEnderecoPrincipal];
            $scope.pessoa.endereco_principal = {};
          }

          $scope.pessoa.endereco_principal[indiceEnderecoPrincipal].id = id;
        };

        $scope.updateTempEnderecos = function(tipoEndereco)
        {
          if (tipoEndereco == 'NACIONAL') {
            $scope.pessoa.endereco_principal = $scope.tempData.enderecos['NACIONAL'];
          } else {
            $scope.pessoa.endereco_principal = $scope.tempData.enderecos['EXTERIOR'];
          }
        };

        $scope.consultarCep = function(cep)
        {
          //serviço com erro no mec
          EnderecoService.consultarCep(cep).then(function(response)
          {
            if(!response._embedded.endereco.length){
              toaster.pop('error', 'Sistema', 'CEP inválido');
              return false;
            }
            //
            $scope.pessoa.endereco_principal = response._embedded.endereco[0];
          }).catch(function(err)
          {
            // console.log(err);
            // if (err.data.status === 500) {
              toaster.pop('error', 'Sistema', 'Não foi possível buscar o endereço, serviço fora do ar, por favor preencha os campos.');
              $scope.endereco_principal_fail = true;
              $scope.pessoa.dados_auxiliares_brasileiro = {
                'paises': [
                  {
                    'id': 31,
                    'nome': 'Brasil'
                  }
                ]
            };

            $scope.pessoa.endereco_principal = {
                'cep': $scope.formPessoa.cep,
                'municipio': {
                  'nome': '',
                  'uf': {
                    'nome': '',
                    'pais': {
                      'id': 31,
                      'nome': 'Brasil'
                    }
                  }
                }
              };
            $scope.consultaEstados($scope.pessoa.endereco_principal.municipio.uf.pais)
            // }
          });
        };

        $scope.consultaEstados = function(pais, callbacks) {
          EnderecoService.consultarEstados(pais.id).then(function(response) {
            $scope.formPessoa.estados = response._embedded.endereco;
            $scope.formPessoa.municipios = [];



            if(typeof callbacks != 'undefined') {
              callbacks.forEach(function(e) {
                console.log(e);
                e();
              })
            }
          }).catch(function(err){
            switch (err.status) {
                case 409:
                    toaster.pop('error', 'Sistema', err.data.mensagem);
                    break;
                case 412:
                    break;
                default:
                    toaster.pop('error', 'Sistema', 'ocorreu um erro ao salvar');
            }
          });
      };

      $scope.consultaMunicipios = function(estado) {
        if (estado === undefined) return false;
        EnderecoService.consultarMunicipios(estado.id).then(function(response){
          $scope.formPessoa.municipios = response._embedded.endereco;
        })
        .catch(function(err){
          switch (err.status) {
            case 409:
              toaster.pop('error', 'Sistema', err.data.mensagem);
              break;
            case 412:
              break;
            default:
              toaster.pop('error', 'Sistema', 'ocorreu um erro ao salvar');
          }
          });
      };

      $scope.__carregarCategoriasTematicas = function() {
        console.log("$scope.__carregarCategoriasTematicas");
        CategoriaService.recuperarCategorias().then(function (retorno) {
          $scope.formArtigo.categorias = retorno.categorias;
        }).catch(function (err) {
          console.log('Error', err);
          toaster.pop('error', 'Sistema', "Erro ao obter categorias temáticas.");
        });
      };

      $scope.__carregarPeriodicos = function(idPessoa) {
        PeriodicoService.recuperarTitulosPeriodicos(idPessoa).then(function (titulos) {

          if (!CommonService.isParametrizacao(titulos, $scope)) {
            return;
          }

          $scope.formArtigo.periodicos = titulos.periodicos;
        }).catch(function (err) {
            toaster.pop('error', 'Sistema', "Erro ao obter periódicos");
            console.log('Error', err);
        });
      };

      $scope.carregarArtigos = function(periodico, pessoa) {
        if (periodico === null) return false;
        PeriodicoService.recuperarAnosArtigos(pessoa.id, periodico.id_veiculo, null).then(function (data) {

          if (!$scope.formArtigo.artigos) {
            $scope.formArtigo.artigos = data.artigos;
          }

          }).catch(function (err) {
            toaster.pop('error', 'Sistema', "Erro ao obter artigos");
          });
      };

      setTimeout(() => {
        $scope.carregarArtigos
      }, 1000);


      $scope.carregaDadosDoArtigo = function ()
      {
        PeriodicoService.recuperarDadosArtigos($scope.inscricao.artigo.id).then(function (data) {
          $scope.inscricao.id_artigo = artigo.id;
          $scope.inscricao.detalhe_inscricao.nr_issn = data.issn;
          $scope.inscricao.detalhe_inscricao.id_idioma = data.id_idioma;
          $scope.inscricao.detalhe_inscricao.ds_titulo_periodico = $scope.inscricao.periodico.titulo;
          $scope.inscricao.detalhe_inscricao.ds_titulo_artigo = $scope.inscricao.artigo.titulo;
          $scope.inscricao.detalhe_inscricao.ds_editora = data.editora;
          $scope.inscricao.detalhe_inscricao.nr_volume = data.volume;
          $scope.inscricao.detalhe_inscricao.nr_fasciculo = data.fasciculo;
          $scope.inscricao.detalhe_inscricao.nr_doi = data.doi;
          $scope.inscricao.detalhe_inscricao.nr_pagina_inicial = data.numero_pg_inicial;
          $scope.inscricao.detalhe_inscricao.nr_pagina_final = data.numero_pg_final;
          $scope.inscricao.detalhe_inscricao.nr_serie = data.serie;
          $scope.inscricao.detalhe_inscricao.ds_url = data.url;
          $scope.inscricao.detalhe_inscricao.tx_observacao = data.observacao;
          $scope.inscricao.detalhe_inscricao.ds_divulgacao = data.divulgacao;
          if(!$scope.inscricao.artigo.ano){
            $scope.inscricao.detalhe_inscricao.nr_ano_publicacao = data.nr_ano_publicacao;
          }else{
            $scope.inscricao.detalhe_inscricao.nr_ano_publicacao = $scope.inscricao.artigo.ano;
          }

          if($scope.inscricao.detalhe_inscricao.issn) {
            $scope.has_issn = true;
          } else {
            $scope.has_issn = false;
          }

          $scope.inscricao.coautores = data.coautores;

        }).catch(function (err) {
          console.log(err);
        });
      };

      $scope.avancarForm = function() {
        var f = function(inscricao) {
          var path ="/passo3/" + inscricao.id;
          $location.path(path);
        }
        $scope.save(f);
      }

      $scope.save = function(f) {
        PCNInscricaoService.salvarInscricao($scope.inscricao).then(function(data) {
          if(typeof f == 'function') {
            f(data);
          }
          toaster.pop('success', 'Sistema', 'Dados da inscrição salvos com sucesso. Prossiga para o próximo passo.');
        }).catch(function(err) {
          console.log(err);
          toaster.pop('error', 'Sistema', 'Não foi possível salvar a inscrição  código ' + $state.params.idInscricao + ", detalhe do erro: " + err.data.detail);
        });
      };

      $scope.declaracao = function() {
        $scope.formArtigo.declaracao = true;
      };

        $scope.salvarDadosPessoa = function(pessoa) {
          $scope.endereco_principal_fail = false;
          DadosAutorService.editar(pessoa.id, $scope.__adaptPessoaToDadosAutor(pessoa)).then(function(response){

            setTimeout(function () {
              console.log('reponse',response);
              $scope.pessoa.endereco_principal.municipio = response.endereco.municipio;
              console.log('pessoa',$scope.pessoa.endereco_principal);
            }, 900);

            $scope.formPessoa.tipoEndereco = ($scope.pessoa.endereco_principal.municipio.uf.pais.id == 31) ? 'NACIONAL' : 'EXTERIOR';

            if($scope.pessoa.endereco_principal.municipio.uf.pais.id != 31) {
              console.log('pessoa2',$scope.pessoa.endereco_principal);
              console.log('pessoa3',$scope.tempData.enderecos['EXTERIOR']);
              $scope.tempData.enderecos['EXTERIOR'] = $scope.pessoa.endereco_principal;
              // $scope.pessoa.endereco_principal = $scope.tempData.enderecos['EXTERIOR'];
              // var callbacks = [
              //   function() {
              //     $scope.consultaMunicipios($scope.tempData.enderecos['EXTERIOR'].municipio.uf);
              //   }
              // ];
              // $scope.consultaEstados($scope.tempData.enderecos['EXTERIOR'].municipio.uf.pais, callbacks);
              console.log('carregar pessoa', $scope.tempData);
              var callbacks = [
                function() {
                  $scope.consultaMunicipios(response.endereco.municipio.uf);
                }
              ];
              $scope.consultaEstados(response.endereco.municipio.uf.pais, callbacks);
            } else {
              $scope.tempData.enderecos['NACIONAL'] = $scope.pessoa.endereco_principal;
            }

            // $scope.__carregarPessoa(pessoa.id);

            toaster.pop('success', 'Sistema', AppSettings.messages.MSG002);
          }).catch(function(err){
              console.log(err.status);
              switch (err.status) {
                case 409:
                  toaster.pop('error', 'Sistema', err.data.mensagem);
                break;
                case 412:
                  console.log(err);
                  toaster.pop('error', 'Sistema', err.data.detail);
                break;
              default:
                console.log(err);
                toaster.pop('error', 'Sistema', 'ocorreu um erro ao salvar dados pessoais');
            }
          });
        };

        $scope.__adaptPessoaToDadosAutor = function(pessoa) {
          var dadosAutor = {
            "telefone": {
              "id" : pessoa.telefone_fixo_principal.id,
              "tipoFisico": "FIXO",
              "codigoPais": pessoa.telefone_fixo_principal.codigoPais,
              "codigoArea": pessoa.telefone_fixo_principal.codigoArea,
              "telefone": pessoa.telefone_fixo_principal.telefone
            },
            "celular": {
              "id" : pessoa.telefone_celular_principal.id,
              "tipoFisico": "CELULAR",
              "codigoPais": pessoa.telefone_celular_principal.codigoPais,
              "codigoArea": pessoa.telefone_celular_principal.codigoArea,
              "telefone": pessoa.telefone_celular_principal.telefone
            },
            "email": {
              "id" : pessoa.correio_eletronico_principal.id,
              "email": pessoa.correio_eletronico_principal.email
            },
            "nomeSocial": {
              "nome": pessoa.nome
            },
            "endereco": {
              "id" : pessoa.endereco_principal.id,
              "cep": pessoa.endereco_principal.cep,
              "pais": pessoa.endereco_principal.municipio.uf.pais,
              "uf": pessoa.endereco_principal.municipio.uf,
              "municipio": pessoa.endereco_principal.municipio,
              "bairro": pessoa.endereco_principal.bairro,
              "numero": pessoa.endereco_principal.numero,
              "complemento": pessoa.endereco_principal.complemento,
              "logradouro": pessoa.endereco_principal.logradouro
            }
          };

          console.log('adaptPessoa ',pessoa.endereco_principal.municipio);

          return dadosAutor;
        };

        $scope.__carregar_inscricao = function() {
          PCNInscricaoService.obterInscricao($state.params.idInscricao).then(function(data) {
            $scope.inscricao = data;
                console.log('$scope.inscricao ' + $scope.inscricao.artigo);
            if($scope.inscricao.artigo != null){
            $scope.inscricao.artigo.titulo_artigo = data.detalhe_inscricao.id;
            $scope.pessoa.id = $scope.usuario_logado.corporativo.id;
            $scope.formArtigo.declaracao = true;
            $scope.carregarArtigos($scope.inscricao.periodico,$scope.pessoa);
            $scope.carregaDadosDoArtigo();
            }
          }).catch(function(data) {
            toaster.pop('error', 'Sistema', 'Não foi possível encontrar a inscrição  código ' + $state.params.idInscricao);
            console.log('error', data);
            $scope.init = false;
            $location.url('/');
          });
        };

        $scope.__carregarPessoa = function(idPessoa) {
          PessoasService.recuperar(idPessoa).then(function(data) {
            $scope.pessoa = data;
            console.log('carregar pessoa', data);
            console.log('carregar pessoa', $scope.tempData);
            var indiceEP = Object.keys($scope.pessoa.endereco_principal);
            console.log('carregar pessoa', indiceEP);
            $scope.pessoa.endereco_principal = $scope.pessoa.endereco_principal[indiceEP];
            $scope.pessoa.telefone_celular_principal = $scope.pessoa.telefone_celular_principal[Object.keys($scope.pessoa.telefone_celular_principal)];
            $scope.pessoa.telefone_cobranca = $scope.pessoa.telefone_cobranca[Object.keys($scope.pessoa.telefone_cobranca)];
            $scope.pessoa.telefone_comercial = $scope.pessoa.telefone_comercial[Object.keys($scope.pessoa.telefone_comercial)];
            $scope.pessoa.telefone_fixo = $scope.pessoa.telefone_fixo[Object.keys($scope.pessoa.telefone_fixo)];
            $scope.pessoa.telefone_fixo_principal = $scope.pessoa.telefone_fixo_principal[Object.keys($scope.pessoa.telefone_fixo_principal)];
            $scope.pessoa.correio_eletronico_principal = $scope.pessoa.correio_eletronico_principal[Object.keys($scope.pessoa.correio_eletronico_principal)];


            if ($scope.pessoa.endereco_principal !== undefined) {
              $scope.formPessoa.cep = $scope.pessoa.endereco_principal.cep;
              $scope.formPessoa.tipoEndereco = ($scope.pessoa.endereco_principal.municipio.uf.pais.id == 31) ? 'NACIONAL' : 'EXTERIOR';

              if($scope.pessoa.endereco_principal.municipio.uf.pais.id != 31) {
                $scope.tempData.enderecos['EXTERIOR'] = $scope.pessoa.endereco_principal;
                // $scope.pessoa.endereco_principal = $scope.tempData.enderecos['EXTERIOR'];
                // var callbacks = [
                //   function() {
                //     $scope.consultaMunicipios($scope.tempData.enderecos['EXTERIOR'].municipio.uf);
                //   }
                // ];
                // $scope.consultaEstados($scope.tempData.enderecos['EXTERIOR'].municipio.uf.pais, callbacks);
                console.log('carregar pessoa', $scope.tempData);
                var callbacks = [
                  function() {
                    $scope.consultaMunicipios($scope.pessoa.endereco_principal.municipio.uf);
                  }
                ];
                $scope.consultaEstados($scope.pessoa.endereco_principal.municipio.uf.pais, callbacks);
              } else {
                $scope.tempData.enderecos['NACIONAL'] = $scope.pessoa.endereco_principal;
              }
            }

            // console.log($scope.pessoa.endereco_principal);

          }).catch(function(error) {
            toaster.pop('error', 'Sistema', 'Não foi possível recuperar dados da pessoa código ' + idPessoa);
            console.log('Error', error);
          });
        };

        $scope.__carregarLattes = function (cpf) {
          PessoasService.recuperarLattes(cpf).then(function(data) {
            $scope.linkLattes = "http://lattes.cnpq.br/" + data.id;
          }).catch(function(error) {
            toaster.pop('error', 'Sistema', 'Não foi possível recuperar o Currículo Lattes do usuário ' + cpf);
            console.log('Error', error);
          });
        };

        //watch de usuarioLogado (atualiza a lista de ies quando o usuário logado for carregado)
        $rootScope.$watch('usuarioLogado', function(newValue, oldValue) {
          if(newValue) {
            $scope.usuario_logado = newValue;
            $scope.__carregarLattes(newValue.id);
            $scope.__carregarPessoa(newValue.corporativo.id);
            $scope.__carregarPeriodicos(newValue.corporativo.id);
          }
        });

        $scope.$watchGroup(['inscricao.periodico', 'usuario_logado'], function(newVal, oldVal) {
          if(typeof newVal[0]  != 'undefined' && typeof newVal[1] != 'undefined' ) {
            console.log('watch inscricao.periodico', newVal[0]);
            $scope.carregarArtigos(newVal[0], newVal[1]);
          }
        });

        //TODO: verificar a carga de dados do artigo
        // $scope.$watch('inscricao.artigo', function(newVal, oldVal) {
        //   if(typeof newVal  != 'undefined') {
        //     console.log('watch carregaDadosDoArtigo', newVal);
        //     $scope.carregaDadosDoArtigo(newVal);
        //   }
        // });




        $scope.__init();
}]

controllersModule.controller('SegundoPassoCtrl', SegundoPassoController);
