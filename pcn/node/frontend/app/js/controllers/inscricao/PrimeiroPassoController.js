'use strict';

var controllersModule = require('../_index');

/**
 * @ngInject
 */
var PrimeiroPassoController = ["$scope", "$rootScope", "$location", "CommonService",
    "PremioService", "EventoService", "IESService", "PessoasService", "toaster", "AppSettings",
    "ValidacaoService", "PCNInscricaoService", "$state", '$capesModal',
    function ($scope, $rootScope, $location, CommonService, PremioService,
              EventoService, IESService, PessoasService, toaster, AppSettings, ValidacaoService,
              PCNInscricaoService, $state, $capesModal) {

        $scope.init = false;

        //ng-model que nos interessa, aqui ficam os dados informados
        //pelo nosso usuario
        $scope.dados_inscricao = {
          id: null,
          id_tp_subtp_producao: null,
          status_artigo : null,
          nm_producao: null,
          evento: null,
          an_conclusao_producao: null,
          cd_programa: null,
          ies: null,
          id_tese: null
        };

        //aqui estão as listas de options, dados de formulário que o usuário não informa
        //eles são carregados logo no __init do controller
        $scope.dados_form = {
          anos_previstos_defesa: null,
          programas_snpg: null,
          ies: null
        };

        $scope.__init = function() {
          $scope.__carregar_inscricao();

          //regras mantidas do código antigo
          ValidacaoService.validaPeriodo(1).then(function (validacao) {
            if (validacao.validade == true) {
                $scope.init = false;
                toaster.pop('error', 'Sistema', 'Acesso não permitido (código: 1)');
                $location.url('/');
            }

            $scope.validaParametro = validacao.validaParam;

            if (validacao.validaParam !== true) {
                toaster.pop('error', 'Sistema', 'Inscrição não parametrizada. Entre em contato o Gestor do sistema.');
                CommonService.isParametrizacao(validacao.validaParam, $scope);
                return;
            }

          }).catch(function (err) {
            //toaster.pop('error', 'Sistema', 'Acesso não permitido (código: 3)');
            $scope.init = false;
            $location.url('/');
          });

          CommonService.isEndInscription();

          $scope.init = true;
        };

        $scope.isArtigoEmAndamento = function() {
          return $scope.dados_inscricao.status_artigo == 'A' && $scope.dados_inscricao.id_tp_subtp_producao != null;
        };

        $scope.isArtigoFinalizado = function() {
          return $scope.dados_inscricao.status_artigo == 'F' && $scope.dados_inscricao.id_tp_subtp_producao != null;
        };

        $scope.__carregarIes = function() {
          IESService.recuperar($rootScope.usuarioLogado.corporativo.id).then(function (ies) {
            console.log("Obtendo lista de ies do usuário", ies.instituicoes_ensino);
            $scope.dados_form.ies = ies.instituicoes_ensino;
          }).catch(function (err) {
            toaster.pop('error', 'Sistema', 'Não foi possivel recuperar as instituições de ensino do autor.');
          });
        }

        $scope.carregarProgramas = function () {
            IESService.recuperarProgramasSNPG($scope.dados_inscricao.ies).then(function (programas) {
              $scope.dados_form.programas_snpg = programas;
            }).catch(function (err) {
              toaster.pop('error', 'Sistema', 'Não foi possivel recuperar o programa informado');
            });
        };

        $scope.fecharAviso = function () {
          $scope.aviso = {show: false};
        };

        $scope.__carregar_inscricao = function() {
          console.log("CARREGANDO INSCRICAO", $state.params.idInscricao);
          PCNInscricaoService.obterInscricao($state.params.idInscricao).then(function(data) {
              console.log(data);
            $scope.dados_inscricao = data;
          }).catch(function(data) {
            toaster.pop('error', 'Sistema', 'Não foi possível encontrar a inscrição  código ' + $state.params.idInscricao);
            console.log('error', data);
            $scope.init = false;
            $location.url('/');
          });
        };

        $scope.__carregar_lista_anos = function(idEvento, tipoGraduacao) {
          EventoService.recuperarAnosPrevistosDefesa(idEvento, tipoGraduacao).then(function (anos_previstos_defesa) {
            $scope.dados_form.anos_previstos_defesa = anos_previstos_defesa;
          }).catch(function (err) {
            console.log(err);

          });
        };

        $scope.save = function(f) {
          console.log($scope.dados_inscricao);
          PCNInscricaoService.salvarInscricao($scope.dados_inscricao).then(function(data) {
            if(typeof f == 'function') {
              f(data);
            }
            toaster.pop('success', 'Sistema', 'Dados da inscrição salvos com sucesso. Prossiga para o próximo passo.');
          }).catch(function(err) {
            console.log(err);
            toaster.pop('error', 'Sistema', 'Não foi possível salvar a inscrição  código ' + $state.params.idInscricao + ", detalhe do erro: " + err.data.detail);
          });
        };

        $scope.isValid = function() {
          if($scope.isArtigoEmAndamento()) {
            return ($scope.dados_inscricao.cd_programa != null &&
              $scope.dados_inscricao.an_conclusao_producao != null &&
              $scope.dados_inscricao.nm_producao != null &&
              $scope.dados_inscricao.id_tp_subtp_producao != null &&
              $scope.dados_inscricao.status_artigo != null);
          }

          if($scope.isArtigoFinalizado()) {
            return ($scope.dados_inscricao.id_tese != null);
          }

          return false;
        };

        $scope.__obterTesesAutor = function() {
          var idPessoa = $rootScope.usuarioLogado.usuario.corporativo.id;
          var tipoGraduacao = $scope.dados_inscricao.id_tp_subtp_producao;

          PessoasService.recuperarTesesAutor(idPessoa, tipoGraduacao).then(function(data) {
            $scope.teses = data.teses;
          }).catch(function(error) {
            console.log(error);
            toaster.pop('error', 'Sistema', 'Não foi possível obter as teses do autor. Detalhe do erro: ' + error.data.detail);
          });
        }

        $scope.saveSituacao = function(f) {
            console.log($scope.dados_inscricao);
            PCNInscricaoService.salvarInscricao($scope.dados_inscricao).then(function(data) {
                if(typeof f == 'function') {
                    f(data);
                }

            }).catch(function(err) {
                toaster.pop('error', 'Sistema', 'Não foi possível salvar a inscrição  código ' + $state.params.idInscricao + ", detalhe do erro: " + err.data.detail);
            });
        };

        $scope.resetDadosInscricao = function(status_artigo) {
            if(status_artigo == 'A'){
                toaster.pop('success','', 'Você alterou sua situação de "Finalizado" para "Em Andamento", os dados preenchidos do Projeto serão descartados.');

            }if(status_artigo == 'F'){
                setTimeout(function(){
                    toaster.pop('success','', 'Você alterou sua situação de "Em Andamento" para "Finalizado", os dados preenchidos do Projeto serão descartados.');
                },2000);
            }

            $scope.dados_inscricao.status_artigo = status_artigo;
            $scope.dados_inscricao.nm_producao = '';
            $scope.dados_inscricao.an_conclusao_producao = null;
            $scope.dados_inscricao.cd_programa = null;
            $scope.dados_inscricao.ies = null;
            $scope.dados_inscricao.id_tese = null;
            $scope.saveSituacao();

            if(status_artigo == 'F') {
                $scope.__obterTesesAutor();
            }

            //var confirmOptions = {
          //  text: 'Alterando sua situação de "Em Andamento" para "Finalizado", os dados preenchidos do Projeto serão descartados. Deseja continuar?',
          //  title: "Confirmação",
          //  confirmText: "Sim",
          //  dismissText: "Não"
          //};

          //$capesModal.confirm(confirmOptions).then(function (e) {
          //  console.log('Yes No modal - YES selecionado', e);
          //  $scope.dados_inscricao.status_artigo = status_artigo;
          //  $scope.dados_inscricao.nm_producao = '';
          //  $scope.dados_inscricao.an_conclusao_producao = null;
          //  $scope.dados_inscricao.cd_programa = null;
          //  $scope.dados_inscricao.ies = null;
          //  $scope.dados_inscricao.id_tese = null;
            //$scope.save();

            //if(status_artigo == 'F') {
            //  $scope.__obterTesesAutor();
            //}
          //}).catch(function (error) {
          //  $scope.dados_inscricao.status_artigo = (status_artigo == 'A') ? 'F' : 'A';
          //  console.log('Yes No modal - NO selecionado');
          //});
        }

        $scope.selecionarTese = function(tese) {
          $scope.dados_inscricao.id_tese = tese.id;
          $scope.dados_inscricao.an_conclusao_producao = tese.defesa.split('-')[0];
          $scope.dados_inscricao.cd_programa = tese.codigoProgramaPpg;
          $scope.save();
        }

        $scope.avancarForm = function() {
          var f = function(inscricao) {
            var path ="/passo2/" + inscricao.id;
            $location.path(path);
          }
          $scope.save(f);
        }

        //inicializa a tela:
        $scope.__init();

        //watch de dados_inscricao (atualiza a lista de anos quando a inscrição for carregada)
        $scope.$watchGroup(['dados_inscricao.id_tp_subtp_producao', 'dados_inscricao.evento'], function(newValues, oldValues, scope) {
          var id_tp_subtp_producao = newValues[0];
          var evento = newValues[1];

          if(id_tp_subtp_producao && evento) {
            $scope.__carregar_lista_anos(evento.id, id_tp_subtp_producao);
          }
        });

        //watch de código programa informado e IES utilizado para edição de ies / programa
        $scope.$watchGroup(['dados_inscricao.cd_programa', 'dados_form.ies'], function(newValues, oldValues, scope) {
          if(newValues[0] != null && newValues[1] != null) {
              // loop para resolver bug da IES não estar sendo preenchida na edição
              // no caso de programas vinculados a mais de uma IES
              // bug relatado no redmine nº 16977
            IESService.recuperarProgramaCodigoSNPG(newValues[0]).then(function (programas) {
                angular.forEach(programas._embedded.programa[0].agrupadoresInstituicaoEnsino, function (array, key) {
                    angular.forEach($scope.dados_form.ies, function (arrayies, key) {
                        if(array.instituicaoEnsino.id == arrayies.id){
                            $scope.dados_inscricao.ies = arrayies.id;
                            $scope.carregarProgramas();
                            return;
                        }
                    });
                });
            }).catch(function (err) {
              toaster.pop('error', 'Sistema', 'Não foi possivel recuperar o programa informado');
            });
          }
        });

        //watch utilizado para realizar a busca de teses (quando forms preenchidos)
        $scope.$watchGroup(['__id_usuario_logado', 'dados_inscricao.id_tp_subtp_producao'], function(newValues, oldValues, scope) {
          if(newValues[0] != null && newValues[1] != null) {
            $scope.__obterTesesAutor();
          }
        });

        //watch de usuarioLogado (atualiza a lista de ies quando o usuário logado for carregado)
        $rootScope.$watch('usuarioLogado', function(newValue, oldValue) {
          if(newValue) {
            $scope.__id_usuario_logado = $rootScope.usuarioLogado.corporativo.id;
            $scope.__carregarIes();
          }
        });
    }];

controllersModule.controller('PrimeiroPassoCtrl', PrimeiroPassoController);
