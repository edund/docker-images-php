'use strict';

var controllersModule = require('../_index');

/**
 * @ngInject
 */
var TerceiroPassoController = ["$scope", "$window", "$rootScope", "$capesModal",
    "CommonService", "PremioService", "ArtigoService", "toaster", "AppSettings", "$state",
    "PCNInscricaoService", "$http",
    function ($scope, $window, $rootScope, $capesModal, CommonService, PremioService,
              ArtigoService, toaster, AppSettings, $state, PCNInscricaoService, $http) {

        CommonService.isEndInscription();

        $scope.arquivos_enviado = {};
        $scope.total_enviados = {};

        $scope.__carregar_inscricao = function() {
          console.log($state.params.idInscricao);
          PCNInscricaoService.obterInscricao($state.params.idInscricao).then(function(data) {
            $scope.inscricao = data;
          }).catch(function(data) {
            toaster.pop('error', 'Sistema', 'Não foi possível encontrar a inscrição  código ' + $state.params.idInscricao);
            console.log('error', data);
            $scope.init = false;
            $location.url('/');
          });
        };

        $scope.__init = function() {
          $scope.__carregar_inscricao();
        };

        $scope.__init();

        $scope.$watch('inscricao', function(newValue, oldValue) {
          if(newValue) {
            console.log(newValue);
            $scope.recuperar_arquivos_enviados(newValue);
            $scope.recuperar_arquivos_evento(newValue.evento);
          }
        });

        $scope.recuperar_arquivos_enviados = function(inscricao) {
          PremioService.recuperarArquivosEnviados(inscricao.id)
            .then(function (retorno) {
                if (retorno.enviados === undefined) {
                    $scope.total_enviados = 1;

                } else {
                    $scope.total_enviados = retorno.enviados.length;
                }
                $scope.arquivos_enviado = retorno;

            }).catch(function (err) {
              console.log(err);
              toaster.pop('error', 'Sistema', 'Não foi possível recuperar os arquivos da inscrição de código ' + inscricao.id);
              //$window.location.href = '#/pcn';
              
            });
        }

        $scope.recuperar_arquivos_evento = function(evento) {
          ArtigoService.recuperarArquivos(evento.id)
            .then(function (retorno) {
                $scope.arquivos = retorno;

            }).catch(function (err) {
          });
        }

        $scope.remover = function (arquivo) {
            $capesModal.confirm({
                "text": AppSettings.messages.MSG006,
                "title": 'Exclusão',
                "dismissText": 'Voltar',
                "confirmText": "Salvar"
            }).then(function () {
                PremioService.removerArquivoArtigo(arquivo).then(function () {
                    $scope.atualizar();

                }).catch(function (error) {
                });

            });
        };

        $scope.atualizar = function () {
            PremioService.recuperarArquivosEnviados($state.params.idInscricao)
                .then(function (retorno) {
                    if (retorno.enviados === undefined) {
                        $scope.total_enviados = 1;

                    } else {
                        $scope.total_enviados = retorno.enviados.length;
                    }
                    $scope.arquivos_enviado = retorno;

                }).catch(function (err) {
            });
        };

        $scope.finalizar = function (artigo, total_enviados) {
            if (total_enviados == true) {
                $capesModal.confirm({
                    "text": 'Ao clicar no botão "Confirmar" você enviará a inscrição do artigo ' + $scope.inscricao.detalhe_inscricao.ds_titulo_artigo + '.<br /><br />Caso não tenha certeza de que este é o artigo a ser enviado ou ainda precise complementar os dados da inscrição, clique na opção"Voltar" e clique em "Confirmar" para salvar seu trabalho e poder retomá-lo quando desejar.<br /><br />Qualquer dúvida, entre em contato com a CAPES através do e-mail pcn@capes.gov.br',
                    "title": 'Confirmação',
                    "dismissText": 'Voltar'
                }).then(function () {
                  $http({
                    url: '/rest/refac/inscricao/' + $scope.inscricao.id + "?finalizar=true",
                    method: 'PUT',
                    data: $scope.inscricao
                  }).success(function (data) {
                      $window.location.href = '#/passo4/'+ $scope.inscricao.id;
                      console.log('estou aqui ' + $scope.inscricao.id);
                      console.log('dados aqui ' + data);
                  }).error(function (err, status) {
                    console.log(err, status);
                  });

                    //$window.location.href = '#/passo4/'+ data;
                    //console.log('estou aqui id da inscricao '+ data);

                }, function () {
                });
            }
        };


        $scope.fileuploadConf = {
            'options': {// passed into the Dropzone constructor
                'url': '/rest/premio/tiposarquivos/1',
                'method': 'post',
                'maxFilesize': 8,
                'dictDefaultMessage': 'Clique aqui para enviar.',
                'dictFileTooBig': AppSettings.messages.MSG048,
                'headers': {"tipo-arquivo": 1, "id-pessoa": 14564},
                'maxFiles': 1,
                'acceptedFiles': "application/pdf,.pdf,.PDF",
                'addRemoveLinks': true,
                'dictResponseError': AppSettings.messages.MSG017,
                'dictInvalidFileType': AppSettings.messages.MSG008,
                init: function () {
                    this.on("addedfile", function (file) {
                        if (this.files[1] != null) {
                            this.removeFile(this.files[1]);
                        }
                        var removeButton = Dropzone.createElement("<a>Substituir Arquivo</a>");
                        var _this = this;
                        removeButton.addEventListener("click", function (e) {
                            e.preventDefault();
                            e.stopPropagation();
                            _this.removeFile(file);
                        });
                        file.previewElement.appendChild(removeButton);
                    });
                    this.on("error", function (file, msg, response) {
                        var dropzone = this;
                        file.previewElement.addEventListener("click", function (e) {
                            e.preventDefault();
                            e.stopPropagation();
                            dropzone.removeFile(file);
                        });

                        if (response !== undefined) {
                            $(file.previewElement).find('.dz-error-message > span').text(AppSettings.messages.MSG017);
                            return false;
                        }
                    });

                    this.on("success", function (file, response) {
                        PremioService.recuperarArquivosEnviados($state.params.idInscricao)
                            .then(function (retorno) {
                                if (retorno.enviados === undefined) {
                                    $scope.total_enviados = 1;

                                } else {
                                    $scope.total_enviados = retorno.enviados.length;
                                }
                                $scope.arquivos_enviado = retorno;

                            }).catch(function (err) {
                        });
                    });
                }
            },
        };
    }];

controllersModule.controller('TerceiroPassoCtrl', TerceiroPassoController);