'use strict';

var controllersModule = require('../_index');

/**
 * @ngInject
 */
var QuartoPassoController = ["$scope","$rootScope","$capesModal","$window",
    "$stateParams", "$anchorScroll","$location","CommonService","PessoasService",
    "ArtigoService","CategoriaService","PeriodicoService", "DadosAutorService",
    "EnderecoService","toaster","AppSettings", "PCNInscricaoService", "$state",
    function ($scope,$rootScope,$capesModal,$window,$stateParams,$anchorScroll,
              $location,CommonService,PessoasService,ArtigoService,CategoriaService,
              PeriodicoService, DadosAutorService, EnderecoService ,toaster, AppSettings,
              PCNInscricaoService, $state)
    {
        $scope.inscricao = {

        };

        $scope.__init = function() {
           

            //2. dados da inscrição
            $scope.__carregar_inscricao();
                        console.log($scope);
        };
        console.log($scope);

        $scope.__carregar_inscricao = function() {
            PCNInscricaoService.obterInscricao($state.params.id).then(function(data) {
                $scope.inscricao = data;
            }).catch(function(data) {
                toaster.pop('error', 'Sistema', 'Não foi possível encontrar a inscrição  código ' + $state.params.idInscricao);
                console.log('error', data);
                $scope.init = false;
                $location.url('/');
            });
        };

        $scope.__init();
    }]

controllersModule.controller('QuartoPassoCtrl', QuartoPassoController);
