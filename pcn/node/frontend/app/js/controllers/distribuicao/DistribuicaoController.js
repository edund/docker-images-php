'use strict';

var controllersModule = require('../_index');


/**
 * @ngInject
 */
var DistribuicaoController = ["$scope", "$rootScope", "DistribuicaoService", "toaster", "AppSettings", "$capesModal", "CommonService", "ManterComissaoService",
    function ($scope, $rootScope, DistribuicaoService, toaster, AppSettings, $capesModal, CommonService, ManterComissaoService) {

        $scope.aviso = [];
        /**
         * Categorias
         */
        $scope.categorias = [];
        $scope.categoria = {
            'selecionado': null
        };
        $scope.grupos = [];
        /**
         * Grupos DE
         */
        $scope.gruposde = [];
        $scope.grupode = {
            'selecionado': null
        };
        /**
         * Grupos PARA
         */
        $scope.grupospara = [];
        $scope.grupopara = {
            'selecionado': null
        };
        /**
         * Artigos
         */
        $scope.artigos = [];
        $scope.artigo = {
            'selecionados': null
        }
        /**
         * Etapas
         */
        $scope.etapas = [];
        $scope.etapa = {
            'selecionado': null
        };

        /**
         * Areas de Avaliacao
         */
        $scope.areasavaliacaocollection = [];
        $scope.areaavaliacao = {
            'selecionado': null
        };


        if ($rootScope.aviso !== undefined && $rootScope.aviso[0] !== undefined) {
            $scope.showbox = 'true';
            $scope.mensagem = $rootScope.aviso;
            $rootScope.aviso = '';
        }

        DistribuicaoService.recuperarCategoriasTematicas(1)
                .then(function (dados) {

                    $scope.categorias.itens = dados.categorias;

                }).catch(function (err) {
            if (err.status === 401) {
                CommonService.redirectUserAccess(true);
            }
        });

        ManterComissaoService.recuperarEtapas()
                .then(function (dados) {
                    $scope.etapas.itens = dados.etapas;
                }).catch(function (err) {
            toaster.pop('error', 'Sistema', 'NÃ£o foi possivel recuperar a pessoa informada');
        });

        $scope.carregarGrupos = function () {

            //console.log('Categoria', $scope.categoria.selecionado);
            //console.log('Etapa', $scope.etapa.selecionado);

            if ($scope.categoria.selecionado == null) {
                return false;
            }

            //$scope.etapa.selecionado.id
            //@todo segundo parametro eh a etapa
            DistribuicaoService.recuperarGrupos($scope.categoria.selecionado.idTema)
                    .then(function (dados) {

                        CommonService.isAccessProfile('verificar');
                        if (dados) {
                            $scope.gruposde.itens = dados.grupos;
                            $scope.grupospara.itens = dados.grupos;
                        }

                    }).catch(function (err) {
                toaster.pop('error', 'Sistema', 'NÃ£o foi possivel recuperar a pessoa informada');
            });

            $scope.artigos = [];


        };
        $scope.verificaArtigoSelecionado = function (idArtigo) {

            var verifica = false;
            angular.forEach($scope.artigo.selecionados, function (value, key) {
                if (value) {
                    return verifica = true;
                }

            })
            if (verifica === false) {
                $scope.artigo.selecionados = null;
            }

        };

        $scope.recuperarArtigos = function () {

            if ($scope.categoria.selecionado.idTema == null || $scope.grupode.selecionado.id == null) {
                return false;
            }

            if ($scope.areaavaliacao.selecionado == null) {

                $scope.areaavaliacao = {
                    'selecionado': {
                        'id': null
                    }
                }
            } else {
                $scope.areaavaliacao.selecionado.id = $scope.areaavaliacao.selecionado.id_area_avaliacao;
            }

            DistribuicaoService.recuperarArtigos(
                    $scope.categoria.selecionado.idTema,
                    $scope.grupode.selecionado.id,
                    $scope.areaavaliacao.selecionado.id)
                    .then(function (dados) {

                        CommonService.isAccessProfile('verificar');
                        $scope.artigos = dados.artigos;

                        if ($scope.areaavaliacao.selecionado.id == null) {
                            $scope.areasavaliacaocollection.itens = dados.artigos;

                            setTimeout(function () {
                                $('#areaavaliacao').trigger("chosen:updated");
                            }, 2000);

                        } else {
                            $scope.recuperaAreasAvaliacao();
                        }

                    }).catch(function (err) {
                //toaster.pop('error', 'Sistema', 'NÃ£o foi possivel recuperar a pessoa informada');
            });
        };

        $scope.recuperaAreasAvaliacao = function () {
            //Recarrega o combo com todas as opcoes de areas de avaliacao mesmo depois de uma selecionada
            $scope.areaavaliacao.selecionado.id = null;

            DistribuicaoService.recuperarArtigos(
                    $scope.categoria.selecionado.idTema,
                    $scope.grupode.selecionado.id,
                    null)
                    .then(function (dados) {

                        CommonService.isAccessProfile('verificar');
                        $scope.areasavaliacaocollection.itens = dados.artigos;
                        setTimeout(function () {
                            $('#areaavaliacao').trigger("chosen:updated");
                        }, 2000);

                    }).catch(function (err) {
                toaster.pop('error', 'Sistema', 'NÃ£o foi possivel recuperar a pessoa informada');
            });
        };

        $scope.update = function () {

            $capesModal.confirm({
                "title": "Confirmação",
                "text": "Deseja efetuar a alteração ?",
                "confirmText": "Sim",
                "dismissText": "Não"
            }).then(function (success) {
                DistribuicaoService.salvar($scope.grupode.selecionado.id, $scope.grupopara.selecionado.id, $scope.artigo.selecionados)
                        .then(function (dados) {

                            CommonService.isAccessProfile('verificar');
                            if (dados.status === 200) {
                                $scope.aviso.message = AppSettings.messages.MSG002;
                                $('.alert').removeClass('alert-danger');
                                $('.alert').addClass('alert-success');
                            } else {
                                //$scope.aviso.message = dados.detail;
                                $('.alert').removeClass('alert-success');
                                $('.alert').addClass('alert-danger');
                            }
                            window.scrollTo(0, 0);
                            $scope.aviso.show = true;

                            $scope.artigo.selecionados = null;
                            $scope.selectedAll = false;
                            $scope.recuperarArtigos();

                        }).catch(function (err) {
                });
            }, function (dismiss) {
                //console.log(dismiss);
            });


        };

        $scope.selectedAll = false;
        $scope.checkall = function () {
            $scope.selectedAll = !$scope.selectedAll;

            if ($scope.selectedAll) {
                $scope.artigo.selecionados = {};
                angular.forEach($scope.artigos, function (item) {
                    $scope.artigo.selecionados[item.id_inscricao] = $scope.selectedAll;
                });
            } else {
                $scope.artigo.selecionados = null;
            }
        }



    }];

controllersModule.controller('DistribuicaoCtrl', DistribuicaoController);