'use strict';

var controllersModule = require('../_index');

/**
 * @ngInject
 */
var AnalisarController = ["$stateParams", "$rootScope", "$scope", "$window",
    "$location", "CommonService", "AppSettings", "AnalisarArtigoService",
    "toaster", "$capesModal", "GerenciarInstrucaoAvaliacaoService", "DistribuicaoService", "ValidacaoService",
    function ($stateParams, $rootScope, $scope, $window, $location, CommonService,
        AppSettings, AnalisarArtigoService, toaster, $capesModal,
        GerenciarInstrucaoAvaliacaoService, DistribuicaoService, ValidacaoService) {
        $scope.sugestaoOb = false;
        $scope.comissao = '';
        $scope.categoria = '';

        $rootScope.parecer = {};
        $rootScope.indicacoes = {
            'itens': [
                {
                    'id': 'S',
                    'nome': 'Indicado'
                },
                {
                    'id': 'N',
                    'nome': 'Não indicado'
                }
            ]
        };
        $scope.aviso = [];

        $("#indicacao").prepend("<option value='' >&nbsp;</option>");

        $scope.carregarComissoes = function () {
            DistribuicaoService.recuperarGrupos($rootScope.dados.categoria.idTema, $rootScope.dados.etapa, 'membro_comissao')
                .then(function (dados) {
                    if (dados) {
                        $rootScope.comissoes = {
                            'itens': dados.grupos
                        };
                    }
                    $rootScope.dados.itens = {};
                    $scope.pagination = {
                        total: 0,
                        current: 1,
                        perPage: AppSettings.resultsPerPage,
                        limit: 0,
                        maxSize: 5
                    };

                }).catch(function (err) {
                toaster.pop('error', 'Sistema', 'Não foi possivel recuperar a pessoa informada');
            });

        };


        $scope.carregarAnaliseArtigos = function (comissao) {
            AnalisarArtigoService.recuperarDadosParaAnalise(false, $rootScope.dados.categoria.idTema, $rootScope.dados.comissao.id)
                .then(function (dados) {
                    CommonService.isAccessProfile('analisar');
                    $rootScope.dados = dados;
                    $rootScope.comissoes = {
                        'itens': dados.grupos
                    };

                    if (dados.permissao === false) {
                        $rootScope.aviso = AppSettings.messages.MSG019;
                        $location.path('/');
                    }


                    $scope.pagination = {
                        total: dados.itens.length,
                        current: 1,
                        perPage: AppSettings.resultsPerPage,
                        limit: 0,
                        maxSize: 5
                    };

                    $scope.pageChanged = function () {
                        if ($scope.pagination.current === 1) {
                            $scope.pagination.limit = 0;
                        } else {
                            $scope.pagination.limit = (($scope.pagination.current * $scope.pagination.perPage) - $scope.pagination.perPage);
                        }
                    };


                }).catch(function (err) {
                if (err.status === 401 || err.status === 500) {
                    CommonService.redirectUserAccess(true);
                }
            });

        };
        if ($rootScope.parametroInativo == 'ok') {
            CommonService.isParametrizacao($rootScope.param, $scope);
            $rootScope.parametroInativo = '';
        }
        if ($stateParams.idInscricao) {
            ValidacaoService.validaPeriodo(4)
                .then(function (validacao) {
                    if (validacao.validade == true) {
                        $location.url('/');
                    }
                    if (validacao.validaParam !== true) {
                        $rootScope.parametroInativo = 'ok';
                        $rootScope.param = validacao.validaParam;
                        $window.location.href = '#/analisar';
                    } else {
                        $rootScope.parametroInativo = 'error';
                        $rootScope.param = {};
                    }
                }).catch(function (err) {
                $location.url('/');
            });
            AnalisarArtigoService.recuperarArtigoParaAnalise(
                $stateParams.idInscricao, false, ($rootScope.dados != undefined ? $rootScope.dados.comissao.id : ''))
                .then(function (dados) {
                    CommonService.isAccessProfile('analisar');
                    if (dados.parametrizacao) {
                        var parametrizacao = dados.parametrizacao;
                        //parametrizacao.cod_mensagem = 42;
                        CommonService.isParametrizacao(parametrizacao, $scope);
                    }
                    $rootScope.dados = dados;

                    setTimeout(function () {
                        $('#indicacao').val((angular.isDefined(dados.parecer) ? dados.parecer[0].indicado : ''));
                        $('#indicacao').trigger("chosen:updated");
                    }, 200);


                }).catch(function (err) {
                if (err.status === 401 || err.status === 500) {
                    CommonService.redirectUserAccess(true);
                }
            });
        } else {
            ValidacaoService.validaPeriodo(4)
                .then(function (validacao) {
                    if (validacao.validade == true) {
                        $location.url('/');
                    }
                }).catch(function (err) {
                $location.url('/');
            });
            AnalisarArtigoService.recuperarDadosParaAnalise(false)
                .then(function (dados) {
                    CommonService.isAccessProfile('analisar');
                    $rootScope.dados = dados;
                    $rootScope.comissoes = {
                        'itens': dados.grupos
                    };

                    if (dados.permissao === false) {
                        $rootScope.aviso = AppSettings.messages.MSG019;
                        $location.path('/');
                    }


                    $scope.pagination = {
                        total: dados.itens.length,
                        current: 1,
                        perPage: AppSettings.resultsPerPage,
                        limit: 0,
                        maxSize: 5
                    };

                    $scope.pageChanged = function () {
                        if ($scope.pagination.current === 1) {
                            $scope.pagination.limit = 0;
                        } else {
                            $scope.pagination.limit = (($scope.pagination.current * $scope.pagination.perPage) - $scope.pagination.perPage);
                        }
                    };

                }).catch(function (err) {
                if (err.status === 401 || err.status === 500) {
                    CommonService.redirectUserAccess(true);
                }
            });
        }

        $scope.emitirParecer = function () {

            jQuery('#indicacao_chosen').css({border: "1px solid #ccc"});
            if (jQuery('#indicacao').val() === "" && $rootScope.dados.sugestao === true) {
                jQuery('#indicacao_chosen').css({border: "1px solid #a94442"});
                toaster.pop('error', '', AppSettings.messages.MSG025);
                return;
            } else {
                $scope.avaliar.segundaFase = false;
                AnalisarArtigoService.salvar($scope.avaliar)
                    .then(function (dados) {
                        AnalisarArtigoService.recuperarArtigoParaAnalise($stateParams.idInscricao)
                            .then(function (dados) {
                                $scope.aviso = {
                                    show: true,
                                    type: 'success',
                                    msg: AppSettings.messages.MSG002
                                };
                                $rootScope.dados = dados;
                                window.scrollTo(0, 0);

                            }).catch(function (err) {
                            if (err.status === 500) {
                                $window.location.href = '#/';
                            }
                        });


                    }).catch(function (err) {
                });
            }
        }

        $scope.downloadAllFiles = function (idInscricao) {
            CommonService.isServiceDocument(idInscricao)
                .then(function (reply) {
                    var url = '../rest/arquivo/download/insc-' + idInscricao;
                    $window.location.href = url;
                    return false;

                }).catch(function (err) {
                if (err.status == 500 || err.status === 404) {
                    toaster.pop('error', 'Serviço', AppSettings.messages.MSG017);
                }
            });
        };

        $scope.fecharAviso = function (index) {
            $(".alert-danger").hide();
            if (index) {
                $scope.aviso[index] = {
                    show: false
                };
            } else {
                $scope.aviso = {
                    show: false
                };
            }
        };

        $scope.baixarTodosPareceres = function (idInscricao) {
            CommonService.isAccessProfile('analisar');
            var url = '../rest/analisar-artigo?idArtigo=' + idInscricao;
            $window.location.href = url;
        };

        $scope.baixarPareceres = function (tipo) {
            CommonService.isAccessProfile('analisar');
            var countParecer = 0;
            $scope.categoria = $rootScope.dados.categoria.idTema;
            $scope.comissao = $rootScope.dados.comissao.id;
            angular.forEach($rootScope.dados.itens, function (array, key) {
                if (array.indicacoes.length > 0)
                    countParecer++;
            });

            if ($rootScope.dados.parametrizacao != undefined) {
                if ($rootScope.dados.parametrizacao.status === 'inativo') {
                    CommonService.isParametrizacao($rootScope.dados.parametrizacao, $scope, 'popup');
                    return false;
                }
            }

            if (countParecer == 0) {
                toaster.pop('info', 'Relatório', AppSettings.messages.MSG021);
                return false;
            }
            var url = '../rest/analisar-artigo?' + tipo + '=' + tipo + '&comissao=' + $scope.comissao + '&categoria=' + $scope.categoria;
            $window.location.href = url;
        };

        $scope.modalArquivosInstrucaoAvaliacao = function () {

            $capesModal.open({
                modalOptions: {
                    size: 'lg',
                    keyboard: false,
                    backdrop: 'static',
                    controller: ["$scope", "$modalInstance", "GerenciarInstrucaoAvaliacaoService",
                        function ($scope, $modalInstance, GerenciarInstrucaoAvaliacaoService) {
                            "ngInject";

                            $scope.fechar = function (modal) {
                                $modalInstance.close();
                            };
                            var args = {"idComissaoAdhoc": $rootScope.dados.idComissao};
                            GerenciarInstrucaoAvaliacaoService.recuperarDocumentos(args)
                                .then(function (dados) {
                                    if (typeof(dados._embedded) !== 'undefined') {
                                        $scope.documentos = dados._embedded.instrucao_avaliacao;
                                        //$('#areaavaliacao').trigger("chosen:updated");
                                    }
                                }).catch(function (err) {
                                toaster.pop('error', 'Sistema', 'Não foi possivel recuperar a pessoa informada');
                            });

                        }],
                    templateUrl: 'analisar/modalArquivosInstrucaoAvaliacao.html',
                }
            }).then(function (success) {
            }, function (error) {
            });

        };

        $scope.modalIncluirAvaliacao = function () {

            $capesModal.open({
                modalOptions: {
                    size: 'lg',
                    keyboard: false,
                    backdrop: 'static',
                    controller: ["$scope", "$modalInstance", "IncluirAvaliacaoService",
                        function ($scope, $modalInstance, IncluirAvaliacaoService) {
                            "ngInject";

                            $scope.arquivo = [];

                            IncluirAvaliacaoService.recuperaAvaliacao('analisar').then(function (dados) {
                                if (typeof(dados._embedded) !== 'undefined') {
                                    var arquivo = dados._embedded.incluir_avaliacao;
                                    if (arquivo.length) {
                                        $scope.arquivo = arquivo;
                                    }
                                }
                            }).catch(function (err) {
                                toaster.pop('error', 'Sistema', 'Não foi possivel recuperar a avaliação');
                            });


                            $scope.fechar = function (modal) {
                                $modalInstance.close();
                            };

                            $scope.fileuploadConf = {
                                'options': {// passed into the Dropzone constructor
                                    'url': '/rest/incluir-avaliacao',
                                    'method': 'post',
                                    'maxFilesize': 2, //MB
                                    'dictDefaultMessage': 'Clique aqui para enviar.',
                                    'headers': {"tipo-arquivo": 1, "id-pessoa": 14564},
                                    'maxFiles': 1,
                                    'acceptedFiles': ".doc, .docx",
                                    'addRemoveLinks': true,
                                    'dictInvalidFileType': "Arquivo inválido. Somente aceito arquivo do tipo DOC ou DOCX",
                                    'dictFileTooBig': AppSettings.messages.MSG048,
                                    'dictResponseError': AppSettings.messages.MSG017,
                                    init: function () {
                                        this.on("addedfile", function (file) {
                                            if (this.files[1] != null) {
                                                this.removeFile(this.files[1]);
                                            }
                                            var removeButton = Dropzone.createElement("<a>Remover</a>");
                                            var _this = this;
                                            removeButton.addEventListener("click", function (e) {
                                                e.preventDefault();
                                                e.stopPropagation();
                                                _this.removeFile(file);
                                            });
                                            file.previewElement.appendChild(removeButton);
                                        });
                                        this.on("error", function (file, msg, response) {
                                            var dropzone = this;
                                            file.previewElement.addEventListener("click", function (e) {
                                                e.preventDefault();
                                                e.stopPropagation();
                                                dropzone.removeFile(file);
                                            });

                                            if (response !== undefined) {
                                                $(file.previewElement).find('.dz-error-message > span').text(AppSettings.messages.MSG017);
                                                return false;
                                            }
                                        });

                                        this.on("success", function (file, response) {
                                            $modalInstance.close();
                                            toaster.pop('success', 'Sistema', 'Avaliação incluída com sucesso');
                                        });
                                    }
                                },
                            };

                        }],
                    templateUrl: 'analisar/modalIncluirAvaliacao.html',
                }
            }).then(function (success) {
            }, function (error) {
            });

        };
        $scope.sugestaoObrigatoria = function () {
            return verifSugestao($scope, $rootScope);
        }
        jQuery("#indicacao").change(function () {
            $scope.sugestaoObrigatoria = function () {
                return validSugestao($scope, $rootScope);
            }
        });
    }];

controllersModule.controller('AnalisarCtrl', AnalisarController);
function verifSugestao($scope, $rootScope, toaster) {
    if ($rootScope.dados !== undefined) {
        if ($rootScope.dados.sugestao !== undefined && $rootScope.dados.sugestao !== false) {
            var indicacao = $rootScope.dados.parecer[0].indicado;
            $scope.sugestao_obrigatoria = $rootScope.dados.sugestao;
            if ($scope.sugestao_obrigatoria === true) {
                if (indicacao === null) {
                    return true;
                }
            }
        }
    }
    return false;
}

function validSugestao($scope, $rootScope) {
    if ($rootScope.dados !== undefined) {
        if ($rootScope.dados.sugestao !== undefined && $rootScope.dados.sugestao !== false) {
            var selecionado = jQuery('#indicacao').val();
            $scope.sugestao_obrigatoria = $rootScope.dados.sugestao;
            if ($scope.sugestao_obrigatoria === true) {
                if (selecionado === "") {
                    return true;
                }
            }
        }
    }
    return false
}