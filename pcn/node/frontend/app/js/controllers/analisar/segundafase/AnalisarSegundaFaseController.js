'use strict';

var controllersModule = require('../../_index');

/**
 * @ngInject
 */
var AnalisarSegundaFaseController = [
    "$capesModal", "$stateParams", "$rootScope", "$scope", "$window",
    "$location", "CommonService", "AppSettings", "AnalisarArtigoSegundaEtapaService",
    "toaster", "DistribuicaoService", "CategoriaService", "ComissaoService", "ValidacaoService",
    function ($capesModal, $stateParams, $rootScope, $scope, $window, $location,
        CommonService, AppSettings, AnalisarArtigoSegundaEtapaService, toaster,
        DistribuicaoService, CategoriaService, ComissaoService, ValidacaoService) {
        $scope.sugestaoOb = false;
        $rootScope.parecer = {};
        $rootScope.indicacoes = {
            'itens': [
                {
                    'id': 'S',
                    'nome': 'Indicado'
                },
                {
                    'id': 'N',
                    'nome': 'Não indicado'
                }
            ]
        };

        $scope.aviso = [];

        $("#indicacao").prepend("<option value='' >&nbsp;</option>");

        if ($rootScope.categoria == undefined) {
            $rootScope.categoria = {
                selecionada: ''
            };
        }

        if ($rootScope.comissao == undefined) {
            $rootScope.comissao = {
                selecionada: ''
            };
        } else {
            ComissaoService.obterComissoesPorMembro(2, $rootScope.categoria.selecionada.idTema)
                .then(function (dados) {
                    $rootScope.filtro = dados;
                    $rootScope.categoria.selecionada = $rootScope.categoria.selecionada;
                    $rootScope.comissao.selecionada = $rootScope.comissao.selecionada;
                    if (typeof $stateParams.idInscricao == 'undefined') {
                        $scope.obterDados();
                    }

                }).catch(function (err) {
                if (err.status === 401 || err.status === 500) {
                    CommonService.redirectUserAccess(true);
                }
            });
        }

        $rootScope.isReadonly = {
            'titulo_artigo': false,
            'autor': false,
        }

        CategoriaService.obterCategoriasPorMembro(2)
            .then(function (dados) {
                $rootScope.filtro = dados;

            }).catch(function (err) {
            if (err.status === 401 || err.status === 500) {
                CommonService.redirectUserAccess(true);
            }
        });

        $scope.carregarComissoes = function ()
        {
            ComissaoService.obterComissoesPorMembro(2, $rootScope.categoria.selecionada.idTema)
                .then(function (dados) {
                    $rootScope.filtro = dados;
                    $rootScope.categoria.selecionada = $rootScope.categoria.selecionada;


                }).catch(function (err) {
                if (err.status === 401 || err.status === 500) {
                    CommonService.redirectUserAccess(true);
                }
            });
        }

        $scope.obterDados = function ()
        {
            $scope.carregarComissoes();
            carregarArtigosIndicados(AnalisarArtigoSegundaEtapaService, $rootScope, CommonService, $scope, AppSettings, $location);
        }

        $scope.init = function (coluna)
        {
            $scope.sort(coluna);
            $scope.sort(coluna);
        };

        $scope.sort = function (keyname)
        {
            $scope.sortKey = keyname;   //set the sortKey to the param passed
            $scope.reverse = !$scope.reverse; //if true make it false and vice versa
        }

        $scope.carregarTodosArtigos = function ()
        {
            carregarTodosArtigos(AnalisarArtigoSegundaEtapaService, $rootScope, $scope, CommonService, AppSettings, $location);
        }

        $scope.habilitarDesabilitarCampos = function (object)
        {
            $rootScope.isReadonly = object;
        }

        $scope.resgatarArtigo = function (idInscricao, resgatar, indicacoes)
        {
            $scope.parans = {
                'inscricao': idInscricao,
                'resgatar': resgatar,
                'segundaFase': true
            }

            if (indicacoes > 0) {
                $capesModal.confirm({
                    "text": AppSettings.messages.MSG022,
                    "title": 'Confirmação',
                    "dismissText": 'Voltar'
                }).then(function () {
                    resgatarArtigos(AnalisarArtigoSegundaEtapaService, $rootScope, CommonService, $scope, AppSettings, resgatar, $location)
                });

            } else {
                resgatarArtigos(AnalisarArtigoSegundaEtapaService, $rootScope, CommonService, $scope, AppSettings, resgatar, $location);
            }
        }


        $scope.emitirParecer = function () {

            jQuery('#indicacao_chosen').css({border: "1px solid #ccc"});
            if (jQuery('#indicacao').val() === "" && $rootScope.dados.sugestao === true) {
                jQuery('#indicacao_chosen').css({border: "1px solid #a94442"});
                toaster.pop('error', '', AppSettings.messages.MSG025);
                return;
            } else {
                $scope.avaliar.segundaFase = true;
                AnalisarArtigoSegundaEtapaService.salvar($scope.avaliar)
                    .then(function (dados) {
                        AnalisarArtigoSegundaEtapaService.recuperarArtigoParaAnalise($stateParams.idInscricao)
                            .then(function (dados) {
                                $scope.aviso = {
                                    show: true,
                                    type: 'success',
                                    msg: AppSettings.messages.MSG002
                                };
                                $rootScope.dados = dados;
                                window.scrollTo(0, 0);

                            }).catch(function (err) {
                            if (err.status === 500) {
                                $window.location.href = '#/';
                            }
                        });


                    }).catch(function (err) {
                });
            }
        }
        if ($rootScope.parametroInativo == 'ok') {
            CommonService.isParametrizacao($rootScope.param, $scope);
            $rootScope.parametroInativo = '';
        }
        if ($stateParams.idInscricao) {
            ValidacaoService.validaPeriodo(6)
                .then(function (validacao) {
                    if (validacao.validade == true) {
                        $location.url('/');
                    }
                    if (validacao.validaParam !== true) {
                        $rootScope.parametroInativo = 'ok';
                        $rootScope.param = validacao.validaParam;
                        $window.location.href = '#/analisarsegundafase';
                    } else {
                        $rootScope.parametroInativo = 'error';
                        $rootScope.param = {};
                    }
                }).catch(function (err) {
                $location.url('/');
            });
            AnalisarArtigoSegundaEtapaService.recuperarArtigoParaAnalise($stateParams.idInscricao)
                .then(function (dados) {
                    CommonService.isAccessProfile('analisarsegundafase');
                    if (dados.parametrizacao) {
                        var parametrizacao = dados.parametrizacao;
                        CommonService.isParametrizacao(parametrizacao, $scope);
                    }

                    $rootScope.dados = dados;

                    setTimeout(function () {
                        $('#indicacao').val((angular.isDefined(dados.parecer) ? dados.parecer[0].indicado : ''));
                        $('#indicacao').trigger("chosen:updated");
                    }, 200);


                }).catch(function (err) {
                if (err.status === 401 || err.status === 500) {
                    CommonService.redirectUserAccess(true);
                }
            });
        }

        ValidacaoService.validaPeriodo(6)
            .then(function (validacao) {
                if (validacao.validade == true) {
                    $location.url('/');
                }
                if (validacao.validaParam !== true) {
                    $rootScope.parametroInativo = 'ok';
                    $rootScope.param = validacao.validaParam;
                    $location.url('/');
                } else {
                    $rootScope.parametroInativo = 'error';
                    $rootScope.param = {};
                }
            }).catch(function (err) {
            $location.url('/');
        });

        $scope.baixarTodosPareceres = function (idInscricao) {
            CommonService.isAccessProfile('analisar');
            var countParecer = 0;

            angular.forEach($rootScope.dados.itens, function (array, key) {
                if (array.indicacoes.length > 0)
                    countParecer++;

            });

            if (countParecer == 0) {
                toaster.pop('info', 'Relatório', AppSettings.messages.MSG021);
                return false;
            }

            var url = '../rest/analisar-artigo-segunda-etapa?idArtigo=' + idInscricao;
            $window.location.href = url;
        };

        $scope.baixarPareceres = function (tipo) {
            CommonService.isAccessProfile('analisar');
            var countParecer = 0;
            $scope.categoria = $rootScope.dados.categoria.idTema;
            $scope.comissao = $rootScope.dados.comissao.id;
            angular.forEach($rootScope.dados.itens, function (array, key) {
                if (array.indicacoes.length > 0)
                    countParecer++;
            });

            if ($rootScope.dados.parametrizacao != undefined) {
                if ($rootScope.dados.parametrizacao.status === 'inativo') {
                    CommonService.isParametrizacao($rootScope.dados.parametrizacao, $scope, 'popup');
                    return false;
                }
            }

            if (countParecer == 0) {
                toaster.pop('info', 'Relatório', AppSettings.messages.MSG021);
                return false;
            }

            var url = '../rest/analisar-artigo-segunda-etapa?' + tipo + '=' + tipo + '&comissao=' + $scope.comissao + '&categoria=' + $scope.categoria;
            $window.location.href = url;
        };

        $scope.downloadAllFiles = function (idInscricao) {
            CommonService.isServiceDocument(idInscricao)
                .then(function (reply) {
                    var url = '../rest/arquivo/download/insc-' + idInscricao;
                    $window.location.href = url;
                    return false;

                }).catch(function (err) {
                if (err.status == 500 || err.status === 404) {
                    toaster.pop('error', 'Serviço', AppSettings.messages.MSG017);
                }
            });
        };

        $scope.voltar = function (categoria, comissao)
        {
            $rootScope.categoria.selecionada = categoria;
            $rootScope.comissao.selecionada = comissao;
            $window.location.href = '#/analisarsegundafase';
            console.log($rootScope.categoria.selecionada, $rootScope.comissao.selecionada);
        }

        $scope.baixarParecerEtapa = function (tipo, idInscricao) {
            CommonService.isAccessProfile('analisar');

            AnalisarArtigoSegundaEtapaService.baixarParecer(tipo, idInscricao)
                .then(function (dados) {
                    if (dados.parecer == false) {
                        toaster.pop('info', 'Relatório', AppSettings.messages.MSG021);
                        return false;
                    } else {
                        var url = '../rest/analisar-artigo-segunda-etapa?' + tipo + '=' + tipo + "&idArtigo=" + idInscricao;
                        $window.location.href = url;
                    }
                }).catch(function (err) {
                if (err.status === 401 || err.status === 500) {
                    CommonService.redirectUserAccess(true);
                }
            });

        };

        $scope.fecharAviso = function (index) {
            $(".alert-danger").hide();
            if (index) {
                $scope.aviso[index] = {
                    show: false
                };
            } else {
                $scope.aviso = {
                    show: false
                };
            }
        };


        $scope.sugestaoObrigatoria = function () {
            return verifSugestao($scope, $rootScope);
        }


        jQuery("#indicacao").change(function () {
            $scope.sugestaoObrigatoria = function () {
                return validSugestao($scope, $rootScope);
            }
        });


    }];


controllersModule.controller('AnalisarSegundaFaseCtrl', AnalisarSegundaFaseController);

function resgatarArtigos(AnalisarArtigoSegundaEtapaService, $rootScope, CommonService, $scope, AppSettings, resgatar, $location) {
    AnalisarArtigoSegundaEtapaService.resgatarArtigo($scope.parans)
        .then(function (dados) {
            if (resgatar === false) {
                carregarArtigosIndicados(AnalisarArtigoSegundaEtapaService, $rootScope, CommonService, $scope, AppSettings, $location);
            } else {
                carregarTodosArtigos(AnalisarArtigoSegundaEtapaService, $rootScope, $scope, CommonService, AppSettings, $location);
            }
        }).catch(function (err) {
        if (err.status === 401 || err.status === 500) {
            CommonService.redirectUserAccess(true);
        }
    });

}

function carregarTodosArtigos(AnalisarArtigoSegundaEtapaService, $rootScope, $scope, CommonService, AppSettings, $location) {

    AnalisarArtigoSegundaEtapaService.recuperarDadosParaAnalise($rootScope.categoria.selecionada.idTema, $rootScope.comissao.selecionada.id, 'all')
        .then(function (dados) {
            CommonService.isAccessProfile('analisar');
            $rootScope.dados = dados;
            if (dados.permissao === false) {
                $rootScope.aviso = AppSettings.messages.MSG019;
                $location.path('/');
            }
            paginator(dados, $scope, AppSettings);

        }).catch(function (err) {
        if (err.status === 401 || err.status === 500) {
            CommonService.redirectUserAccess(true);
        }
    });
}

function carregarArtigosIndicados(AnalisarArtigoSegundaEtapaService, $rootScope, CommonService, $scope, AppSettings, $location) {
    var categoria = null,
        comissao = null;

    if ($rootScope.comissao.selecionada != undefined) {
        categoria = $rootScope.categoria.selecionada.idTema;
        comissao = $rootScope.comissao.selecionada.id;
        AnalisarArtigoSegundaEtapaService.recuperarDadosParaAnalise(categoria, comissao)
            .then(function (dados) {

                CommonService.isAccessProfile('analisar');
                $rootScope.dados = dados;

                if (dados.permissao === false) {
                    $rootScope.aviso = AppSettings.messages.MSG019;
                    $location.path('/');
                }
                paginator(dados, $scope, AppSettings);

            }).catch(function (err) {
            if (err.status === 401 || err.status === 500) {
                CommonService.redirectUserAccess(true);
            }
        });
    }
}

function paginator(dados, $scope, AppSettings) {
    $scope.pagination = {
        total: dados.itens.length,
        current: 1,
        perPage: AppSettings.resultsPerPage,
        limit: 0,
        maxSize: 5
    };

    $scope.pageChanged = function () {
        if ($scope.pagination.current === 1) {
            $scope.pagination.limit = 0;
        } else {
            $scope.pagination.limit = (($scope.pagination.current * $scope.pagination.perPage) - $scope.pagination.perPage);
        }
    };
}

function verifSugestao($scope, $rootScope) {
    if ($rootScope.dados !== undefined) {
        if ($rootScope.dados.sugestao !== undefined && $rootScope.dados.sugestao !== false) {
            var indicacao = $rootScope.dados.parecer[0].indicado;
            $scope.sugestao_obrigatoria = $rootScope.dados.sugestao;
            if ($scope.sugestao_obrigatoria === true) {
                if (indicacao === null) {
                    return true;
                }
            }
        }
    }
    return false
}

function validSugestao($scope, $rootScope) {
    if ($rootScope.dados !== undefined) {
        if ($rootScope.dados.sugestao !== undefined && $rootScope.dados.sugestao !== false) {
            var selecionado = jQuery('#indicacao').val();
            $scope.sugestao_obrigatoria = $rootScope.dados.sugestao;
            if ($scope.sugestao_obrigatoria === true) {
                if (selecionado === "") {
                    return true;
                }
            }
        }
    }
    return false
}