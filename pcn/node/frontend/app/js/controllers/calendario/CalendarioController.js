'use strict';

var controllersModule = require('../_index');
var moment = require('moment');
/**duardo
 * @ngInject
 */
var CalendarioController = ["toaster", "$scope", "$rootScope", "$capesModal",
    "AppSettings", "CommonService", "PremioService", "$stateParams",
    function (toaster, $scope, $rootScope, $capesModal, AppSettings,
            CommonService, PremioService, $stateParams) {

        $scope.aviso = [];
        $scope.rows = [];
        $scope.excluir = [];
        $scope.situacoes = [
            {
                id: 'A',
                nome: 'Aberta'
            },
            {
                id: 'F',
                nome: 'Fechada'
            },
        ];

        if (!$stateParams.id)
        {
            PremioService.recuperarPremios($stateParams.id)
                    .then(function (premios) {
                        if ($rootScope.mostrarAviso == 1) {
                            $scope.aviso = {
                                show: true,
                                type: 'success',
                                msg: AppSettings.messages.MSG002
                            };
                            $rootScope.mostrarAviso = 0;
                        }
                        console.log(premios);
                        $scope.premios = premios;
                    }).catch(function (err) {
                if (err.status === 401) {
                    CommonService.redirectUserAccess(true);
                }
            });

            $scope.alterarEventoPadrao = function (id_evento) {
                PremioService.alterarEventoPadrao(id_evento)
                        .then(function (premios) {
                            $scope.premios = premios;
                        }).catch(function (err) {
                });
            }

        } else {
            PremioService.recuperarFaseDoPremio($stateParams.id)
                    .then(function (fases) {
                        $scope.itens = fases;
                        $scope.todos = fases.fases;
                        $scope.AppSettings = AppSettings;
                        $scope.evento = {};

                        angular.forEach($scope.itens.itens_evento, function (fase) {
                            if (fase.default == "S") {
                                $scope.evento.selecionado = {"id": fase.id};
                            }
                        });
                    }).catch(function (err) {
                toaster.pop('error', 'Sistema', 'Não foi possivel recuperar a pessoa informada');
            });

            $scope.salvar = function ()
            {
                var titulos_fases_excluídas = [];
                angular.forEach($scope.enviar.fases.$modelValue, function (fase) {
                    angular.forEach($scope.enviar.excluir.$modelValue, function (value, indice) {
                        if (indice == fase['idFase'] && value === true) {
                            titulos_fases_excluídas.push((' ' + fase['nome']));
                        }
                    });
                });

                if (titulos_fases_excluídas.length > 0) {
                    $capesModal.confirm({
                        "text": AppSettings.messages.MSG027.slice(0, 34) + titulos_fases_excluídas + AppSettings.messages.MSG027.slice(40 + Math.abs(0)),
                        "title": 'Confirmação',
                        "dismissText": 'Voltar'
                    }).then(function () {
                        salvarCalendario(PremioService, $scope, toaster, AppSettings, $stateParams);
                    });
                } else {
                    salvarCalendario(PremioService, $scope, toaster, AppSettings, $stateParams);
                }
            }

            $scope.importar = function ()
            {
                PremioService.importar($scope.enviar_evento)
                        .then(function (fases) {
                            $scope.itens = fases;
                            $scope.AppSettings = AppSettings;
                            window.scrollTo(0, 0);
                            $scope.sortableOptions =
                                    {
                                        stop: function (e, ui) {
                                            for (var index in $scope.itens.fases) {
                                                $scope.itens.fases[index].i = index;
                                            }
                                        },
                                        axis: 'y'
                                    };
                            $scope.aviso = {
                                show: true,
                                type: 'success',
                                msg: AppSettings.messages.MSG002
                            };
                        }).catch(function (err) {
                    toaster.pop('error', 'Sistema', 'Não foi possivel recuperar a pessoa informada');
                });
            }

        }

        $scope.calcularDuracao = function (data_inicio, data_fim, fase)
        {
            if (data_inicio != "" && data_fim != "") {
                var date1 = moment(data_inicio, 'DD/MM/YYYY');
                var date2 = moment(data_fim, 'DD/MM/YYYY');
                var diff = date2.diff(date1, 'days');
                fase.duracao = diff+1;
            }
        }

        $scope.fecharAviso = function ()
        {
            $scope.aviso = {
                show: false
            };
        }
    }];

controllersModule.controller('CalendarioCtrl', CalendarioController);

function recuperarFaseDoPremio($stateParams, $scope, toaster, PremioService, AppSettings)
{

    PremioService.recuperarFaseDoPremio($stateParams.id)
            .then(function (fases) {
                $scope.itens = fases;
                $scope.AppSettings = AppSettings;
                window.scrollTo(0, 0);
                $scope.sortableOptions =
                        {
                            stop: function (e, ui) {
                                for (var index in $scope.itens.fases) {
                                    $scope.itens.fases[index].i = index;
                                }
                            },
                            axis: 'y'
                        };
            }).catch(function (err) {
        toaster.pop('error', 'Sistema', 'Não foi possivel recuperar a pessoa informada');
    });
}

function salvarCalendario(PremioService, $scope, toaster, AppSettings, $stateParams)
{
    var campoObrigatorio = 0;
    jQuery('.obr').removeClass('has-error');
    var i = 0;
    angular.forEach($scope.enviar.fases.$modelValue, function (value) {
        if (value.inicio == "") {
            jQuery('#dtInicio-' + i).addClass('has-error');
            campoObrigatorio = 1;
        }
        if (value.previsaoEncerramento == "") {
            jQuery('#dtFinal-' + i).addClass('has-error');
            campoObrigatorio = 1;
        }
        jQuery('#situacao-' + i).find('div').eq(0).css({border: "1px solid #cccccc"});
        if (value.situacao.id == "") {
            jQuery('#situacao-' + i).find('div').eq(0).css({border: "1px solid #a94442"});
            campoObrigatorio = 1;
        }
        i++;
    });
    if (campoObrigatorio == 0) {
        PremioService.salvarCalendario($scope.enviar)
                .then(function (dados) {
                    if (dados.status == 2) {
                        $scope.aviso = {
                            show: true,
                            type: 'success',
                            msg: AppSettings.messages.MSG002
                        };
                        recuperarFaseDoPremio($stateParams, $scope, toaster, PremioService, AppSettings);
                    } else if (dados.status == 3) {
                        toaster.pop('error', '', AppSettings.messages.MSG028);
                        jQuery('#' + dados.label).addClass('has-error');
                    } else {
                        $scope.aviso = {
                            show: true,
                            type: 'danger',
                            msg: 'Falha ao tentar gravar!'
                        };
                        window.scrollTo(0, 0);
                    }
                }).catch(function (err) {
            toaster.pop('error', 'Sistema', 'Não foi possivel recuperar a pessoa informada');
        });
    } else {
        toaster.pop('error', '', AppSettings.messages.MSG025);

    }
}