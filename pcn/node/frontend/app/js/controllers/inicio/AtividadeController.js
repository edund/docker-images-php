'use strict';

var controllersModule = require('../_index');


/**
 * @ngInject
 */
var AtividadeController = ["$capesModal", "$stateParams", "$rootScope", "$scope", "$window",
    "$location", "CommonService", "AppSettings", "toaster", "PremioService", "$http",
    function ($capesModal, $stateParams, $rootScope, $scope, $window, $location,
      CommonService, AppSettings, toaster, PremioService, $http) {
        $scope.fases = {};
        $scope.aviso = [];
        $scope.template = 'passo1';
        $scope.alerta = {
            'show': false,
            'message': null,
        };

        if ($rootScope.parametroInativo == 'ok') {
            CommonService.isParametrizacao($rootScope.param, $scope);
            $rootScope.parametroInativo = '';
        }

        if ($rootScope.aviso !== undefined && $rootScope.aviso[0] !== undefined) {
            $scope.showbox = 'true';
            $scope.mensagem = $rootScope.aviso;
            $rootScope.aviso = '';
        }

        $scope.fecharAviso = function (index) {
            $(".alert-danger").hide();
            if (index) {
                $scope.aviso[index] = {
                    show: false
                };
            } else {
                $scope.aviso = {
                    show: false
                };
            }
        };

        setTimeout(function () {
            PremioService.recuperarFasesDoPremio(1, $rootScope.user)
                .then(function (fases) {
                    $scope.fases = fases;
                    $rootScope.id_evento = fases.id_evento;
                }).catch(function (err) {
                toaster.pop('error', 'Sistema', 'Não foi possivel recuperar a pessoa informada');
            });
        },600);

        $scope.inicializarInscricao = function() {
          //antes de realizar o post, verificar se o usuário já possui inscrição
          var url = '/rest/refac/inscricao';
          var data = {
            usuario : $rootScope.usuarioLogado.id
          };
          var config = { headers: {'Content-Type': 'application/json'} };
          $http.post(url, data, config).then(function (response) {
              console.log('ddddd', response);
              var path ="/passo1/" + response.data.id;
            $location.path(path);
          }, function (response) {
            console.log(response);
            toaster.pop('error', 'Sistema', 'Erro ao iniciar inscrição para usuário ' + $rootScope.usuarioLogado.id);
          });
        };


    }];

controllersModule.controller('AtividadeCtrl', AtividadeController);
