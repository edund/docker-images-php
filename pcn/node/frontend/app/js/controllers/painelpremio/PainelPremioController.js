'use strict';

var controllersModule = require('../_index');

/**
 * @ngInject
 */
var PainelPremioController = ["toaster", "$scope", "$rootScope", "$capesModal",
    "AppSettings", "CommonService", "PainelPremioService", "$location", "$stateParams",
    function (toaster, $scope, $rootScope, $capesModal, AppSettings,
            CommonService, PainelPremioService, $location, $stateParams) {

        $scope.aviso = [];

        $scope.fecharAviso = function ()
        {
            $scope.aviso = {
                show: false
            };
        }
        if ($stateParams.id == undefined) {
            recuperarCombo(PainelPremioService, $scope, toaster, AppSettings, $stateParams, CommonService);
        }

        $scope.orderByFieldS = 'dscSituacao';
        $scope.reverseSortS = false;

        $scope.orderByFieldP = 'dscCategoriaParecer';
        $scope.reverseSortP = false;

        $scope.orderByFieldF = 'dscCategoriaParecerFinal';
        $scope.reverseSortF = false;

        $scope.redirect = function (url)
        {
            $location.path(url);
        }

        $scope.carregaEtapa = function ()
        {
            recuperarDados(PainelPremioService, $scope, toaster, AppSettings, $scope.etapa.id, CommonService);
        }

        $scope.carregaRelatorio = function (tpRelatorio)
        {
            $stateParams.tpRelatorio = tpRelatorio;
            recuperarDadosRelatorio(PainelPremioService, $scope, toaster, AppSettings, $stateParams, CommonService);
        }

    }];

controllersModule.controller('PainelPremioCtrl', PainelPremioController);

function recuperarCombo(PainelPremioService, $scope, toaster, AppSettings, $stateParams, CommonService)
{
    PainelPremioService.retornoCombo()
            .then(function (retorno) {
                $scope.itens = retorno.combo;
                $scope.AppSettings = AppSettings;
                $scope.retorno = retorno;
                $scope.etapa = {"id": "C", "dsc": "Todos"};
            }).catch(function (err) {
        if (err.status === 401) {
            CommonService.redirectUserAccess(true);
        } else {
            toaster.pop('error', 'Sistema', 'Não foi possivel recuperar a informação');

        }
    });
}


function recuperarDados(PainelPremioService, $scope, toaster, AppSettings, idEtapa, CommonService)
{
    PainelPremioService.retornoDados(idEtapa)
            .then(function (retorno) {
                $scope.retorno = retorno;
                $scope.AppSettings = AppSettings;
            }).catch(function (err) {
        if (err.status === 401) {
            CommonService.redirectUserAccess(true);
        } else {
            toaster.pop('error', 'Sistema', 'Não foi possivel recuperar a informação');

        }
    });
}

function recuperarDadosRelatorio(PainelPremioService, $scope, toaster, AppSettings, $stateParams, etapaSel, CommonService)
{
    PainelPremioService.recuperarDadosRelatorio($stateParams.tpRelatorio, $stateParams.id, $stateParams.etapa)
            .then(function (retorno) {
                $scope.codElemento = $stateParams.id;
                $scope.etapa = $stateParams.etapa;
                $scope.itens = retorno;
                $scope.AppSettings = AppSettings;
            }).catch(function (err) {
        if (err.status === 401) {
            CommonService.redirectUserAccess(true);
        } else {
            toaster.pop('error', 'Sistema', 'Não foi possivel recuperar a informação');

        }
    });
}