'use strict';

var AppSettings = {
    titleApp: "Prêmio CAPES/Natura Campus de Excelência em Pesquisa",
    version: "1.4.84",
    resultsPerPage: "50",
    browsers: {
        "ie": {
            "homologado": 'ligado',
            "version": '9'
        },
        "chrome": {
            "homologado": 'ligado',
            "version": '40'
        },
        "firefox": {
            "homologado": 'ligado',
            "version": '38'
        },
        "safari": {
            "homologado": 'ligado',
            "version": 'xxx'
        },
    },
    perfis: {
        "P01": {
            'Name': "VISITANTE",
            'Routers': {}
        },
        "P02": {
            'Name': "DISCENTE",
            'Routers': {}
        },
        "P03": {
            'Name': "COMISSÃO EXECUTIVA",
            'Routers': [
                '/verificar',
                '/comissao',
                '/distribuicao'
            ]
        },
        "P04": {
            'Name': "MEMBRO DE COMISSÃO",
            'Routers': [
                '/analisar',
                '/analisarsegundafase',
                '/gerenciarinstrucaoavaliacao',
                '/avaliacaoartigos'
            ]
        },
        "P05": {
            'Name': "PRESIDENTE DE COMISSÃO",
            'Routers': [
                '/analisar',
                '/emitirparecerfinal',
                '/analisarsegundafase',
                '/emitirparecerfinalsegundafase',
                '/gerenciarinstrucaoavaliacao',
                '/avaliacaoartigos'
            ]
        },
    },
    messages: {
        "MSG001": "Login ou senha inválidos.",
        "MSG002": "Ação realizada com sucesso.",
        "MSG003": "",
        "MSG004": "",
        "MSG005": "Número máximo de arquivos atingido",
        "MSG006": "Deseja realizar a exclusão do arquivo?",
        "MSG007": "",
        "MSG008": "Arquivo inválido. Somente aceito arquivo do tipo PDF.",
        "MSG009": "",
        "MSG010": "Inscrição confirmada - e-mail",
        "MSG011": "Deseja realizar a exclusão?",
        "MSG012": "Nenhum registro localizado.",
        "MSG014": "Já existe parecer emitido. Não é possível realizar a exclusão do consultor.",
        "MSG015": "A comissão está fechada. Não é possível adicionar o consultor.",
        "MSG016": "Faltam <NUMERO> consultores. A comissão não pode ser fechada.",
        "MSG017": "Ocorreu um erro no serviço e não foi possível processar sua solicitação.",
        "MSG018": "Após o envio do parecer final não será possível realizar nenhuma alteração. Deseja confirmar a ação?",
        "MSG019": "Não foi possível concluir a ação, por favor, verifique suas credenciais de acesso.",
        "MSG020": "As inscrições para o Prêmio CAPES/Natura Campus de Excelência em Pesquisa estão encerradas",
        "MSG021": "Não foi possível gerar o relatório, não existem dados a serem apresentados.",
        "MSG022": "Este artigo já possui um parecer, deseja realizar a exclusão?",
        "MSG023": "Informe ao menos um artigo para salvar ou finalizar o parecer.",
        "MSG024": "Não foi possível concluir a ação. Fase já cadastrada.",
        "MSG025": "Campo(s) de preenchimento obrigatório.",
        "MSG026": "Não foi possível concluir a ação solicitada. A fase não existe na base de dados.",
        "MSG027": "Confirma a exclusão da(s) fase(s) <ITEM> do calendário?",
        "MSG028": "Data de início da fase não pode ser maior do que a data de previsão de fim.",
        "MSG029": "Evento já cadastrado para o prêmio.",
        "MSG030": "Parâmetro já cadastrado para o evento do prêmio.",
        "MSG031": "Não é possível realizar ação solicitada: Fase relacionada com Evento.",
        "MSG033": "Ano do evento informado não é permitido. O Prêmio CAPES/Natura Campus de Excelência em Pesquisa foi criado a partir de 2015.",
        "MSG034": "Exclusão não pode ser realizada. Realização de inscrição já iniciada.",
        "MSG035": "Ano da premiação informado não é permitido. O Prêmio CAPES/Natura Campus de Excelência em Pesquisa foi criado a partir de 2015.",
        "MSG036": "Inscrição não pode ser realizada. Necessário configurar o(s) parâmetro(s) <ITENS>. Por favor, contatar pcn@capes.gov.br.",
        "MSG037": "A comissão já possui o número máximo de membros parametrizado para o evento - <ITEM> membros.",
        "MSG038": "O consultor selecionado já está como presidente em <ITEM> comissão(ões), que é a quantidade permitida para o evento do prêmio.",
        "MSG042": "Parâmetro não foi configurado corretamente. Por favor, contatar pcn@capes.gov.br.",
        "MSG043": "Período da fase encerrado. Não será possível acessar a funcionalidade do sistema.",
        "MSG044": "Parâmetro(s) <ITEM> inativo(s). Necessário reativar o(s) parâmetro(s). Por favor, contatar pcn@capes.gov.br.",
        "MSG045": "Faltam \"<ITEM>\" artigos. A comissão não pode ser fechada.",
        "MSG046": "Evento já possui edital cadastrado.",
        "MSG047": "Nenhum Edital encontrado.",
        "MSG048": "Arquivo muito grande ({{filesize}}Mb). Máximo permitido: {{maxFilesize}}Mb.",
        "MSG049": "",
        "MSG050": "A ação não pode ser realizada, pois existe(m) parâmetro(s) necessário(s) que não está(ão) cadastrado(s). Por favor, contatar pcn@capes.gov.br."
    },
};

module.exports = AppSettings;