'use strict';

var servicesModule = require('./_index.js');

/**
 * @ngInject
 */
var CommonService = ["$q", "$http", "$filter", "$rootScope", "$location",
    "$window", "PremioService", "AppSettings", "PessoasService", "toaster",
    function ($q, $http, $filter, $rootScope, $location,
            $window, PremioService, AppSettings, PessoasService, toaster)
    {

        var service = {};

        service.isExistInscription = function (cpf)
        {
            PessoasService.recuperarInscricaoFinalizada(cpf)
                    .then(function (inscricao) {
                        if (inscricao.id_inscricao !== undefined) {
                            console.log(inscricao);
                            $rootScope.artigo = inscricao;
                            $rootScope.id_inscricao = inscricao.id_inscricao;
                            $rootScope.ano_premio = inscricao.ano_premio;
                            $rootScope.data_confirmacao = inscricao.data_confirmacao;
                            $window.location.href = '#/passo4/'+inscricao.id_inscricao;
                        }
                    }).catch(function (err) {
            });
        };

        service.isEndInscription = function () {
            setTimeout(function () {
                PremioService.recuperarFasesDoPremio(1, $rootScope.user)
                    .then(function (fases) {

                        service.isExistInscription($rootScope.user.id);
                        var data = fases['fases'][0].previsaoEncerramento.split('-');
                        var datas = [];

                        datas.fim = new Date(data[0], data[1] - 1, data[2].substring(0, 2), data[2].substring(3, 5), data[2].substring(6, 8));
                        datas.atual = new Date();

                        if (datas.atual > datas.fim) {
                            $rootScope.aviso = AppSettings.messages.MSG020;
                            $window.location.href = '#/pcn';
                        }
                    }).catch(function (err) {
                });
            },800)
        };

        service.isAccessProfile = function (module)
        {
            var status = false;
            var route_now = $location.path();
            setTimeout(function () {
                var perfis = $.map($rootScope.usuarioLogado.roles, function (item, index) {
                    return item.toUpperCase();
                });
                if ($rootScope.usuarioLogado !== undefined) {
                    switch (module) {
                        case 'verificar':
                            status = angular.isString($filter('filter')(perfis, AppSettings.perfis.P03.Name, true)[0]);
                            break;
                        case 'comissao':
                            status = angular.isString($filter('filter')(perfis, AppSettings.perfis.P03.Name, true)[0]);
                            break;
                        case 'analisar':
                        case 'analisarsegundafase':
                            if (angular.isString($filter('filter')(perfis, AppSettings.perfis.P04.Name, true)[0]) ||
                                    angular.isString($filter('filter')(perfis, AppSettings.perfis.P05.Name, true)[0])) {
                                status = true;
                            }
                            break;
                        case 'emitirparecerfinal':
                        case 'emitirparecerfinalsegundafase':
                            status = angular.isString($filter('filter')(perfis, AppSettings.perfis.P05.Name, true)[0]);
                            break;
                    }

                    if (status === false) {
                        service.redirectUserAccess(true);
                    }
                }
            }, 300);
        };

        service.isAccessModule = function (auth)
        {
            if (auth === false) {
                service.redirectUserAccess(true);
            }
        }

        service.redirectUserAccess = function (showMessage)
        {
            if (showMessage === true)
                $rootScope.aviso = AppSettings.messages.MSG019;
            $window.location.href = '#/pcn';
        }

        service.isServiceDocument = function (id)
        {
            var deferred = $q.defer();
            $http.get('/rest/arquivo/download/insc-' + id).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.isParametrizacao = function (parametro, $scope, output)
        {
            var message = '';
            switch (parametro.cod_mensagem) {
                case 36:
                    message = AppSettings.messages.MSG036.slice(0, 74) + parametro.detail + AppSettings.messages.MSG036.slice(81 + Math.abs(0));
                    break;
                case 37:
                    message = AppSettings.messages.MSG037.slice(0, 78) + parametro.detail + AppSettings.messages.MSG037.slice(84 + Math.abs(0));
                    break;
                case 38:
                    message = AppSettings.messages.MSG038.slice(0, 51) + parametro.detail + AppSettings.messages.MSG038.slice(57 + Math.abs(0));
                    break;
                case 42:
                    message = AppSettings.messages.MSG042;
                    break;
                case 44:
                    message = AppSettings.messages.MSG044.slice(0, 13) + parametro.detail + AppSettings.messages.MSG044.slice(19 + Math.abs(0));
                    break;
                case 45:
                    message = AppSettings.messages.MSG045.slice(0, 8) + parametro.detail + AppSettings.messages.MSG045.slice(14 + Math.abs(0));
                    break;
            }

            if (output == 'popup') {
                toaster.pop('error', '', message);

            } else {
                $scope.aviso['parametrizacao'] = {
                    show: true,
                    type: 'danger',
                    msg: message,
                };

                if (parametro.cod_mensagem == undefined) {
                    $scope.aviso['parametrizacao'] = {
                        show: false,
                    };
                    return true;
                } else {
                    return false;
                }
            }
        };

        return service;
    }];

servicesModule.service('CommonService', CommonService);

function isAccess(route_now, rotas_perfil) {
    var status = false;
    angular.forEach(rotas_perfil, function (rota, key) {
        if (route_now == rota) {
            status = true;
        }
    });

    return status;
}