'use strict';

var servicesModule = require('./_index.js');

/**
 * @ngInject
 */
var CategoriaService = ["$q", "$http",
    function ($q, $http) {
        var service = {};

        service.recuperarCategorias = function (id_producao) {
            var deferred = $q.defer();
            $http.get('/rest/radio-categorias', {
                params: {id_producao: id_producao}
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.obterCategoriasPorMembro = function (etapa) {
            var deferred = $q.defer();
            $http.get('/rest/obter-categoria', {
                params: {etapa: etapa}
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.deleteCategoriaTematicaEvento = function (id, id_evento) {
            var deferred = $q.defer();
            $http({
                url: '/rest/obter-categoria/' + id + '?evento=' + id_evento,
                method:  'DELETE',

            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        return service;
    }];

servicesModule.service('CategoriaService', CategoriaService);