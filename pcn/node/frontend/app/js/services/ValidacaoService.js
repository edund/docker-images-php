'use strict';

var servicesModule = require('./_index.js');

/**
 * @ngInject
 */
var ValidacaoService = ["$q", "$http",
    function ($q, $http) {
        var service = {};
        service.validaPeriodo = function (idFase) {
            var deferred = $q.defer();
            $http.get('/rest/validacao', {
                params: {idFase: idFase}
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };


        return service;
    }];

servicesModule.service('ValidacaoService', ValidacaoService);