'use strict';

var servicesModule = require('./_index.js');

/**
 * @ngInject
 */
var PremioService = ["$q", "$http",
    function ($q, $http) {
        var service = {};

        service.recuperarPremio = function (id) {
            var deferred = $q.defer();
            $http.get('/rest/manter-premio' + (id != undefined ? '/' + id : '')).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.importar = function (dados) {
            var deferred = $q.defer();
            $http({
                url: '/rest/calendario',
                method: 'POST',
                data: dados
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.salvar = function (dados) {
            var deferred = $q.defer();
            $http({
                url: '/rest/manter-premio' + (dados.id.$modelValue != undefined ? '/'+dados.id.$modelValue : ''),
                method: dados.id.$modelValue != undefined ? 'PUT' : 'POST',
                data: dados
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.salvarCalendario = function (dados) {
            var deferred = $q.defer();
            $http({
                url: '/rest/calendario' + (dados.id_evento.$modelValue != undefined ? '/'+dados.id_evento.$modelValue : ''),
                method: 'PUT',
                data: dados
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.alterarEventoPadrao = function (id_evento) {
            var deferred = $q.defer();
            var dados = { padrao: true };
            $http({
                url: '/rest/calendario/' + id_evento,
                method: 'PUT',
                data: dados
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.recuperarPremios = function (id_evento) {
            var deferred = $q.defer();
            $http.get('/rest/calendario', {
                params: {id_evento: id_evento}
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.recuperarFaseDoPremio = function (id) {
            var deferred = $q.defer();
            $http.get('/rest/calendario/'+id).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.recuperarFasesDoPremio = function (id_evento, user) {
            var deferred = $q.defer();

            // if (user !== undefined ) {
                $http.get('/rest/fase-premio', {
                    params: {id_evento: id_evento}
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (err, status) {
                    deferred.reject({data: err, status: status});
                });
                return deferred.promise;
            // }
        };

        service.recuperarArquivosEnviados = function (id_inscricao) {
            var deferred = $q.defer();
            $http.get('/rest/arquivos/upload/' + id_inscricao).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.removerArquivoArtigo = function (arquivo) {
            var deferred = $q.defer();
            $http.delete('/rest/arquivos/upload/' + arquivo.id)
                .success(function (data) {
                    deferred.resolve(data);
                }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.download = function (id_arquivo) {
            var deferred = $q.defer();
            $http.get('/rest/arquivos/upload', {
                params: {id_arquivo: id_arquivo}
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.deleteTipoDocumentoEvento = function (id, id_evento) {
            var deferred = $q.defer();
            $http({
                url: '/rest/premio/tiposarquivos/' + id + '?evento=' + id_evento,
                method:  'DELETE',

            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.deleteItemChecklistEvento = function (id, id_evento) {
            var deferred = $q.defer();
            $http({
                url: '/rest/itens-checklist/' + id + '?evento=' + id_evento,
                method:  'DELETE',

            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        return service;
    }];

servicesModule.service('PremioService', PremioService);