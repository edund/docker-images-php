'use strict';

var servicesModule = require('./_index.js');

/**
 * @ngInject
 */
var EnderecoService = ["$q", "$http",
    function ($q, $http){
        var service = {};

        service.consultarCep = function(cep) {

            var url = ['/rest/endereco?resource=cep&cep=', cep ].join("");
            var deferred = $q.defer();
            $http.get(url)
                .success(function (data) {
                    deferred.resolve(data);
                }).error(function (err, status) {
                    deferred.reject({data: err, status: status});
                });
            return deferred.promise;
        };
        service.consultarEstados = function(pais) {

            var url = ['/rest/endereco?resource=uf&pais=', pais ].join("");
            var deferred = $q.defer();
            $http.get(url)
                .success(function (data) {
                    deferred.resolve(data);
                }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.consultarMunicipios = function(uf) {

            var url = ['/rest/endereco?resource=municipio&uf=', uf ].join("");
            var deferred = $q.defer();
            $http.get(url)
                .success(function (data) {
                    deferred.resolve(data);
                }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        return service;
    }];

servicesModule.service('EnderecoService', EnderecoService);