'use strict';

var servicesModule = require('./_index.js');

/**
 * @ngInject
 */
var PainelInscricaoService = ["$q", "$http",
    function ($q, $http) {
        var service = {};

        service.retornoDados = function () {
            var deferred = $q.defer();
            $http.get('/rest/painel-inscricao').success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.recuperarDadosRelatorio = function (tpRelatorio, codElemento) {
            var deferred = $q.defer();
            $http.get('/rest/painel-inscricao', {
                params: {tpRelatorio: tpRelatorio, codElemento: codElemento}
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        return service;
    }];

servicesModule.service('PainelInscricaoService', PainelInscricaoService);