'use strict';

var servicesModule = require('./_index.js');

/**
 * @ngInject
 */
var ArtigoService = ["$q", "$http",
    function ($q, $http){
        var service = {};

        service.recuperarDivulgacoes = function (id_lista) {
            var deferred = $q.defer();
            $http.get('/rest/combo-divulgacoes/' + id_lista).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.recuperarArquivos = function (id_evento) {
            var deferred = $q.defer();
            $http.get('/rest/premio/tiposarquivos/' + id_evento).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.salvar = function (inscricao) {
            var deferred = $q.defer();
            $http({
                url: '/rest/inscricao' + (inscricao.id_artigo.selecionado.id ? '/' +inscricao.id_artigo.selecionado.id : ''),
                method: 'POST',
                data: inscricao
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.finalizar = function (inscricao) {
            var deferred = $q.defer();
            $http({
                url: '/rest/refac/inscricao/' + inscricao + "?finalizar=true", 
                method: 'POST',
                data: inscricao
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        return service;
    }];

servicesModule.service('ArtigoService', ArtigoService);