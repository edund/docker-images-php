'use strict';

var servicesModule = require('./_index.js');

/**
 * @ngInject
 */
var ManterComissaoService = ["$q", "$http",
    function ($q, $http) {
        var service = {};


        service.recuperarEtapas = function () {

            var deferred = $q.defer();
            $http.get('/rest/manter-comissao', {
                params: {
                    tpConsulta: 'etapas'
                }
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;

        };

        service.recuperarComissoes = function (categoria, etapa) {
            var deferred = $q.defer();
            $http.get('/rest/manter-comissao', {
                params: {
                    tpConsulta: 'comissoes',
                    categoria: categoria,
                    etapa: etapa
                }
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.recuperarAreasEspecificas = function (id) {
            var deferred = $q.defer();
            $http.get('/rest/manter-comissao/' + id).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.pesquisar = function (scope) {
            var deferred = $q.defer();
            $http.get('/rest/manter-comissao', {
                params: {
                    idComissao: (scope.idComissao.$modelValue == undefined ? '' : scope.idComissao.$modelValue),
                    cpf: (scope.cpf.$$rawModelValue == undefined ? '' : scope.cpf.$$rawModelValue),
                    nome: (scope.nome.$modelValue == undefined ? '' : scope.nome.$modelValue),
                    area: (scope.area.$modelValue.id == undefined ? '' : scope.area.$modelValue.id),
                    areaEspecifica: (scope.areaEspecifica.$modelValue == undefined ? '' : scope.areaEspecifica.$modelValue.id)
                }
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.parametrizacaoInclusaoParticipante = function (id, etapa, tpConsulta) {
            var deferred = $q.defer();
            $http.get('/rest/manter-comissao/' + id, {
                params: {
                    parametro: true,
                    etapa: etapa,
                    tpConsulta: tpConsulta
                }
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.removerParticipante = function (idParticipante) {
            var deferred = $q.defer();
            $http.delete('/rest/manter-comissao/' + idParticipante)
                    .success(function (data) {
                        deferred.resolve(data);
                    }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.incluirParticipante = function (participante) {
            var deferred = $q.defer();
            $http({
                url: '/rest/manter-comissao/' + participante.idComissao,
                method: 'POST',
                data: participante
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.alterarComissao = function (comissao) {
            var deferred = $q.defer();
            $http({
                url: '/rest/manter-comissao/' + comissao.id,
                method: 'PUT',
                data: comissao
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.adicionarComissao = function(comissao){
            var deferred = $q.defer();
            $http({
                url: '/rest/manter-comissao/' + comissao,
                method: 'POST',
                data: comissao
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        return service;
    }];

servicesModule.service('ManterComissaoService', ManterComissaoService);