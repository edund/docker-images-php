'use strict';

var servicesModule = require('./_index.js');

/**
 * @ngInject
 */
var AnalisarArtigoService = ["$q", "$http",
    function ($q, $http) {
        var service = {};

        service.recuperarDadosParaAnalise = function (segundaFase, categoria, comissao) {
            var deferred = $q.defer(),
                url = '';


            if (segundaFase == 'false')
                url = '?categoria='+categoria+'&comissao='+comissao;
            else
                url = '?segundaFase='+segundaFase+'&categoria='+categoria+'&comissao='+comissao;
            $http.get('/rest/analisar-artigo' +url ).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.recuperarArtigoParaAnalise = function (id_inscricao, segundaFase, comissao) {
            var deferred = $q.defer();
            $http.get('/rest/analisar-artigo/' + id_inscricao
                + (comissao != undefined  ? '|' + comissao : '')
                + (segundaFase != false  ? '|' + segundaFase : '')
            ).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.salvar = function (dados) {
            var deferred = $q.defer();
            $http({
                url: '/rest/analisar-artigo' + (dados.id_parecer.$modelValue != undefined  ? '/' + dados.id_parecer.$modelValue : ''),
                method:  'POST' ,
                data: dados
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.resgatarArtigo = function (dados) {
            var deferred = $q.defer();
            $http({
                url: '/rest/analisar-artigo/'+dados.inscricao,
                method:  'PUT' ,
                data: dados
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        return service;
    }];

servicesModule.service('AnalisarArtigoService', AnalisarArtigoService);