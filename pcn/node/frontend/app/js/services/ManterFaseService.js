'use strict';

var servicesModule = require('./_index.js');

/**
 * @ngInject
 */
var ManterFaseService = ["$q", "$http",
    function ($q, $http) {
        var service = {};

        service.recuperarFase = function (id) {
            var deferred = $q.defer();
            $http.get('/rest/fase-premio'+(id != undefined ? '/'+id : '')).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.salvar = function (dados) {
            var deferred = $q.defer();
            $http({
                url: '/rest/fase-premio' + (dados.id.$modelValue != undefined ? '/'+dados.id.$modelValue : ''),
                method: dados.id.$modelValue != undefined ? 'PUT' : 'POST',
                data: dados
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.delete = function (id) {
            var deferred = $q.defer();
            $http({
                url: '/rest/fase-premio/' + id,
                method:  'DELETE',

            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        return service;
    }];

servicesModule.service('ManterFaseService', ManterFaseService);