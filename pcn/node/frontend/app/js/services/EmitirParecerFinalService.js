'use strict';

var servicesModule = require('./_index.js');

/**
 * @ngInject
 */
var EmitirParecerFinalService = ["$q", "$http",
    function ($q, $http) {
        var service = {};

        service.recuperarDadosParaAnalise = function (segundaFase, categoria, comissao) {
            var deferred = $q.defer(),
                url = '';

            if (segundaFase == 'false')
                url = '?categoria='+categoria+'&comissao='+comissao;
            else
                url = '?segundaFase='+segundaFase+'&categoria='+categoria+'&comissao='+comissao;

            $http.get('/rest/emitir-parecer-final'+ url).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.salvar = function (dados) {
            var deferred = $q.defer();
            $http({
                url: '/rest/emitir-parecer-final/' + dados.id_participante.$modelValue,
                method:  'POST',
                data: dados
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.removerIndicacao = function (id_inscricao, id_parecer) {
            var deferred = $q.defer();
            $http({
                url: '/rest/emitir-parecer-final/' + id_parecer,
                method:  'PUT',
                data: {'inscricao': id_inscricao}
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.deleteFile = function (id) {
            var deferred = $q.defer();
            $http({
                url: '/rest/emitir-parecer-final/' + id,
                method:  'DELETE',

            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        return service;
    }];

servicesModule.service('EmitirParecerFinalService', EmitirParecerFinalService);