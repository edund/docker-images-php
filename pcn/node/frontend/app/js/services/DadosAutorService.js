'use strict';

var servicesModule = require('./_index.js');

/**
 * @ngInject
 */
var DadosAutorService = ["$q", "$http",
    function ($q, $http){
        var service = {};

        service.editar = function(idPessoa, data) {

            var deferred = $q.defer();
            var url = ['/rest/dados-autor/', idPessoa ].join("");
            $http({
                url: url,
                method: 'POST',
                data: data
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };



        return service;
    }];

servicesModule.service('DadosAutorService', DadosAutorService);