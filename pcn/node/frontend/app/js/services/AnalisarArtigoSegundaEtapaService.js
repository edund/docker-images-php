'use strict';

var servicesModule = require('./_index.js');

/**
 * @ngInject
 */
var AnalisarArtigoSegundaEtapaService = ["$q", "$http",
    function ($q, $http) {
        var service = {};

        service.recuperarDadosParaAnalise = function (categoria, comissao, acao) {
            var deferred = $q.defer();

            $http.get('/rest/analisar-artigo-segunda-etapa' +
                    '?categoria=' + categoria + '&comissao=' + comissao + '&acao=' + acao
                    ).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.recuperarArtigoParaAnalise = function (id_inscricao) {
            var deferred = $q.defer();
            $http.get('/rest/analisar-artigo-segunda-etapa/' + id_inscricao).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.salvar = function (dados) {
            var deferred = $q.defer();
            $http({
                url: '/rest/analisar-artigo-segunda-etapa' + (dados.id_parecer.$modelValue != undefined ? '/' + dados.id_parecer.$modelValue : ''),
                method: 'POST',
                data: dados
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.resgatarArtigo = function (dados) {
            var deferred = $q.defer();
            $http({
                url: '/rest/analisar-artigo-segunda-etapa/' + dados.inscricao,
                method: 'PUT',
                data: dados
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.baixarParecer = function (tipo, idInscricao) {
            var deferred = $q.defer();
            $http.get('/rest/analisar-artigo-segunda-etapa?' + tipo + '=' + tipo + "&idArtigo=" + idInscricao).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };


        return service;
    }];

servicesModule.service('AnalisarArtigoSegundaEtapaService', AnalisarArtigoSegundaEtapaService);