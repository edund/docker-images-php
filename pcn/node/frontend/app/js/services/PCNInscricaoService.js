'use strict';

var servicesModule = require('./_index.js');

/**
 * @ngInject
 */
var PCNInscricaoService = ["$q", "$http", "$rootScope", function ($q, $http, $rootScope) {
  var service = {};

  service.obterInscricao = function (idInscricao) {
      var deferred = $q.defer();


      if (idInscricao !== 'undefined') {
          $http.get('/rest/refac/inscricao/' + idInscricao).success(function (data) {
              deferred.resolve(data);
          }).error(function (err, status) {
              deferred.reject({data: err, status: status});
          });
      }

      return deferred.promise;
  };

  service.salvarInscricao = function(inscricao) {
    var deferred = $q.defer();
    var url = '/rest/refac/inscricao/' + inscricao.id;
    var config = { headers: {'Content-Type': 'application/json'} };

    $http.put(url, inscricao, config).success(function (data) {
      deferred.resolve(data);
    }).error(function (data, status, header, config) {
      deferred.reject({data:data, status:status, header:header, config:config});
    });

    return deferred.promise;
  };

  return service;
}];

servicesModule.service('PCNInscricaoService', PCNInscricaoService);
