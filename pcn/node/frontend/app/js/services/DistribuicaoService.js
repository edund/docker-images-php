'use strict';

var servicesModule = require('./_index.js');

/**
 * @ngInject
 */
var DistribuicaoService = ["$q", "$http",
    function ($q, $http) {
        var service = {};

        service.recuperarCategoriasTematicas = function (id) {
            var deferred = $q.defer();
            $http.get('/rest/distribuicao', {
                params: {id: id, tpConsulta: 'ct'}
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.recuperarGrupos = function (idTema, idEtapa, tpConsulta) {
            var deferred = $q.defer();
            console.log(tpConsulta);
            $http.get('/rest/distribuicao', {
                params: {
                    idCategoria: idTema,
                    idEtapa: idEtapa,
                    tpConsulta: (tpConsulta == undefined ? 'grupos' : tpConsulta)
                }
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.recuperarArtigos = function (idCategoria, idGrupo, idAreaAvaliacao) {

            var deferred = $q.defer();
            $http.get('/rest/distribuicao', {
                params: {
                    idCategoria: idCategoria,
                    idGrupo: idGrupo,
                    idAreaAvaliacao: idAreaAvaliacao,
                    tpConsulta: 'art'
                }
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.salvar = function (idgrupode, idgrupopara, artigos) {
            var deferred = $q.defer();
            artigos.id_comissao_avaliacao_para = idgrupopara;
            $http({
                url: '/rest/distribuicao' + (idgrupopara ? '/' + idgrupopara : ''),
                method: 'POST',
                data: artigos
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        return service;
    }];

servicesModule.service('DistribuicaoService', DistribuicaoService);