'use strict';

var servicesModule = require('./_index.js');

/**
 * @ngInject
 */
var PessoasService = ["$q", "$http",
    function ($q, $http) {
        var service = {};

        service.recuperarLattes = function (cpf) {
            var deferred = $q.defer();
            $http.get('/rest/getIdCvLattes/' + cpf).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.recuperarTesesAutor = function (id, tipo_graducao) {
            var deferred = $q.defer();
            $http.get('/rest/tese', {
                params: {id_pessoa: id, tipo_graducao: tipo_graducao}
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.recuperarTesesAutor = function (id, tipo_graducao) {
            var deferred = $q.defer();
            $http.get('/rest/tese', {
                params: {id_pessoa: id, tipo_graducao: tipo_graducao}
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.recuperar = function (id,retornarIes = false) {
            var deferred = $q.defer();
            var parametros = {};
            if(retornarIes) {
                parametros = {
                    retornar_ies : true
                }
            }
            $http.get('/rest/autor/dadospessoais/' + id, {params:parametros} ).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.recuperarMunicipios = function (id_uf) {
            var deferred = $q.defer();
            $http.get('/rest/combo-municipios/' + id_uf).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.recuperarUFs = function () {
            var deferred = $q.defer();
            $http.get('/rest/localidades/ufs').success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.recuperarTodosIdiomas = function () {
            var deferred = $q.defer();
            $http.get('/rest/idioma').success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.recuperarInscricaoFinalizada = function (cpf) {
            var deferred = $q.defer();
            $http.get('/rest/inscricao', {
                params: {cpf: cpf}
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        return service;
    }];

servicesModule.service('PessoasService', PessoasService);