'use strict';

var servicesModule = require('./_index.js');

/**
 * @ngInject
 */
var VerificarArtigoService = ["$q", "$http",
    function ($q, $http) {
        var service = {};

        service.recuperarIesAndProgramas = function () {
            var deferred = $q.defer();
            $http.get('/rest/verificar-artigo').success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.obterArtigo = function (idArtigo) {
            var deferred = $q.defer();
            $http.get('/rest/verificar-artigo/'+idArtigo).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.pesquisar = function (scope) {
            var deferred = $q.defer();
            $http.get('/rest/verificar-artigo', {
                params: {
                    titulo: (scope.titulo.$modelValue == undefined ? '' : scope.titulo.$modelValue),
                    autor: (scope.autor.$modelValue == undefined ? '' : scope.autor.$modelValue),
                    programa: (scope.programas.$modelValue == undefined ? '' : scope.programas.$modelValue.snpg),
                    ies: (scope.ies.$modelValue == undefined ? '' : scope.ies.$modelValue.id),
                    situacao: (scope.situacao.$modelValue == undefined ? '' : scope.situacao.$modelValue.id),
                    categoria: (scope.categoria.$modelValue == undefined ? '' : scope.categoria.$modelValue.idTema),
                    comissaoAdhoc: (scope.comissaoadhoc.$modelValue == undefined ? '' : scope.comissaoadhoc.$modelValue.id),
                }
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.salvar = function (dados) {
            var deferred = $q.defer();
            $http({
                url: '/rest/verificar-artigo' + (dados.id_artigo.$modelValue ? '/' + dados.id_artigo.$modelValue : ''),
                method: dados.id_artigo.$modelValue ? 'POST' : 'PUT',
                data: dados
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        return service;
    }];

servicesModule.service('VerificarArtigoService', VerificarArtigoService);