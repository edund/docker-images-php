'use strict';

var servicesModule = require('./_index.js');

/**
 * @ngInject
 */
var PeriodicoService = ["$q", "$http",
    function ($q, $http) {
        var service = {};

        service.recuperarDadosArtigos = function (id) {
            var deferred = $q.defer();
            $http.get('/rest/inputs-dados-artigos/' + id).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.recuperarAnosArtigos = function (id_pessoa, id_veiculo, id_producao) {
            var deferred = $q.defer();
            $http.get('/rest/periodicos/anos', {
                params: {id_pessoa: id_pessoa, id_veiculo: id_veiculo, id_producao: id_producao}
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.recuperarTitulosPeriodicos = function (id) {
            var deferred = $q.defer();
            $http.get('/rest/periodicos/titulos/' + id).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        return service;
    }];

servicesModule.service('PeriodicoService', PeriodicoService);