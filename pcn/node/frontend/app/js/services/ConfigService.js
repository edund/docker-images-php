'use strict';

var servicesModule = require('./_index.js');

/**
 * @ngInject
 */
var ConfigService = ["$q", "$http",
    function ($q, $http){
        var service = {};

        service.getVersion = function() {

            var url = '/rest/config';
            var deferred = $q.defer();
            $http.get(url)
                .success(function (data) {
                    deferred.resolve(data);
                }).error(function (err, status) {
                    deferred.reject({data: err, status: status});
                });
            return deferred.promise;
        };

        return service;
    }];

servicesModule.service('ConfigService', ConfigService);