'use strict';

var servicesModule = require('./_index.js');

/**
 * @ngInject
 */
var VisualizarChecklistService = ["$q", "$http",
    function ($q, $http) {
        var service = {};
        service.pesquisar = function (scope, pag) {
            var deferred = $q.defer();
            $http.get('/rest/visualizar-checklist', {
                params: {
                    ano: (scope.ano.$modelValue == undefined ? '' : scope.ano.$modelValue.id),
                    pag: pag
                }
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };
        service.retornoCombo = function () {
            var deferred = $q.defer();
            $http.get('/rest/visualizar-checklist', {
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };
        return service;
    }];

servicesModule.service('VisualizarChecklistService', VisualizarChecklistService);