'use strict';

var servicesModule = require('./_index.js');

/**
 * @ngInject
 */
var EventoService = ["$q", "$http",
    function ($q, $http) {
        var service = {};

        service.recuperarAnosPrevistosDefesa = function (id, tipo) {
            var deferred = $q.defer();

            $http.get('/rest/anosprevistosdefesa/' + id ,{
                params: { tipo : tipo }
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        return service;
    }];

servicesModule.service('EventoService', EventoService);