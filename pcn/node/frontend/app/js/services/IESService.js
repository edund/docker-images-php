'use strict';

var servicesModule = require('./_index.js');

/**
 * @ngInject
 */
var IESService = ["$q", "$http",
    function ($q, $http) {
        var service = {};

        service.recuperar = function(id){
            var deferred = $q.defer();
            $http.get( '/rest/instituicoes/ies/' + id).success(function(data) {
                deferred.resolve(data);
            }).error(function(err, status) {
                deferred.reject({data:err, status:status});
            });
            return deferred.promise;
        };

        service.recuperarProgramasSNPG = function(id){
            var deferred = $q.defer();
            $http.get( '/rest/programasnpg/ies/' + id).success(function(data) {
                deferred.resolve(data);
            }).error(function(err, status) {
                deferred.reject({data:err, status:status});
            });
            return deferred.promise;
        };

        service.recuperarProgramaCodigoSNPG = function(codigoSNPG){
          var deferred = $q.defer();
          var params = { params: { codigoPrograma:  codigoSNPG}};
          $http.get('/rest/programasnpg/ies', params).success(function(data) {
              deferred.resolve(data);
          }).error(function(err, status) {
              deferred.reject({data:err, status:status});
          });
          return deferred.promise;
      };

        return service;
    }];

servicesModule.service('IESService', IESService);