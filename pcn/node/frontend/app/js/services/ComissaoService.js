'use strict';

var servicesModule = require('./_index.js');

/**
 * @ngInject
 */
var ComissaoService = ["$q", "$http",
    function ($q, $http) {
        var service = {};

        service.obterComissoesPorMembro = function (etapa, idCategoria) {
            var deferred = $q.defer();
            $http.get('/rest/obter-comissao', {
                params: {etapa: etapa, idCategoria: idCategoria}
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        return service;
    }];

servicesModule.service('ComissaoService', ComissaoService);