'use strict';

var servicesModule = require('./_index.js');

/**
 * @ngInject
 */
var GerenciarInstrucaoAvaliacaoService = ["$q", "$http",
    function ($q, $http) {
        var service = this;

        service.obterInfo = function() {
            var deferred = $q.defer();
            return deferred.promise;
        };
        service.recuperarDocumentos = function(args) {
            var deferred = $q.defer();
            var p = args || {};

            $http.get('/rest/instrucao-avaliacao', {
                params: p
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.recuperarDocumento = function (id) {
            var deferred = $q.defer();
            $http.get('/rest/instrucao-avaliacao/' + id).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };

        service.excluirDocumento = function(id) {
            var deferred = $q.defer();
            $http.delete('/rest/instrucao-avaliacao/' + id)
                .success(function (data) {
                    deferred.resolve(data);
                }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };
        return service;
    }];

servicesModule.service('GerenciarInstrucaoAvaliacaoService', GerenciarInstrucaoAvaliacaoService);