'use strict';

var servicesModule = require('./_index.js');

/**
 * @ngInject
 */
var IncluirAvaliacaoService = ["$q", "$http", "$rootScope",
    function ($q, $http, $rootScope) {
        var service = this;

        service.recuperaAvaliacao = function( operacao ) {
            var p = operacao || {};
            var deferred = $q.defer();
            $http.get('/rest/incluir-avaliacao'+ (operacao != '' ? '?operacao=' + operacao : ''), {
                params : p
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject({data: err, status: status});
            });
            return deferred.promise;
        };


        return service;
    }];

servicesModule.service('IncluirAvaliacaoService', IncluirAvaliacaoService);