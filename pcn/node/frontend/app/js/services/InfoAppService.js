'use strict';

var servicesModule = require('./_index.js');

/**
 * @ngInject
 */
var InfoAppService = ["$q", "$http",
	function ($q, $http) {
		var service = this;

		service.obterInfo = function() {
			var deferred = $q.defer();
			return deferred.promise;
		};

		service.recuperarUsuarioLogado = function () {
			var deferred = $q.defer();

			$http.get('/auth/status').success(function (data) {
				deferred.resolve(data);
			}).error(function (err, status) {
				deferred.reject({data: err, status: status});
			});
			return deferred.promise;
		};

		return service;
	}];

servicesModule.service('InfoAppService', InfoAppService);