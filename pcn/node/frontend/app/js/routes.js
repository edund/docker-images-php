'use strict';

/**
 * @ngInject
 */
function configureRoutes($stateProvider, $locationProvider, $urlRouterProvider) {

    $locationProvider.html5Mode(false);

    $stateProvider
            .state('Dashboard', {
                url: '/',
                controller: 'AtividadeCtrl as atividade',
                templateUrl: 'atividade.html',
                title: 'Atividade',
                authenticated: true
            });

    $stateProvider
            .state('Emitir Parecer Final Segunda Fase', {
                url: '/emitirparecerfinalsegundafase',
                controller: 'EmitirParecerFinalSegundaFaseCtrl as emitirparecerfinal',
                templateUrl: 'emitirparecerfinal/segundafase/index.html',
                title: 'Emitir Parecer Final do artigo',
                authenticated: true
            });

    $stateProvider
            .state('manter_premio1', {
                url: '/premio',
                controller: 'PremioCtrl as premio',
                templateUrl: 'premio/index.html',
                title: 'Manter Prêmio',
                authenticated: true
            });

    $stateProvider
            .state('manter_premio2', {
                url: '/premio/:id',
                controller: 'PremioCtrl as premio',
                templateUrl: 'premio/index.html',
                title: 'Manter Prêmio',
                authenticated: true
            });

    $stateProvider
            .state('calendario', {
                url: '/calendario',
                controller: 'CalendarioCtrl as calendario',
                templateUrl: 'calendario/index.html',
                title: 'Calendário',
                authenticated: true
            });

    $stateProvider
            .state('calendario_fases', {
                url: '/calendario/:id',
                controller: 'CalendarioCtrl as calendario',
                templateUrl: 'calendario/edit.html',
                title: 'Fases do Calendário',
                authenticated: true
            });

    $stateProvider
            .state('manter_fase', {
                url: '/manterfase',
                controller: 'ManterFaseCtrl as manterfase',
                templateUrl: 'manterfase/index.html',
                title: 'Manter Fase',
                authenticated: true
            });

    $stateProvider
            .state('Analisar e emitir parecer Segunda Fase', {
                url: '/analisarsegundafase',
                controller: 'AnalisarSegundaFaseCtrl as analisar',
                templateUrl: 'analisar/segundafase/index.html',
                title: 'Analisar e emitir Parecer dos artigos da segunda fase',
                authenticated: true
            });

    $stateProvider
            .state('Avaliar artigo da segunda fase', {
                url: '/avaliarsegundafase/:idInscricao',
                controller: 'AnalisarSegundaFaseCtrl as analisar',
                templateUrl: 'analisar/segundafase/avaliar.html',
                title: 'Avaliar artigo da segunda fase',
                authenticated: true
            });

    $stateProvider
            .state('Emitir Parecer Final', {
                url: '/emitirparecerfinal',
                controller: 'EmitirParecerFinalCtrl as emitirparecerfinal',
                templateUrl: 'emitirparecerfinal/index.html',
                title: 'Emitir Parecer Final do artigo',
                authenticated: true
            });

    $stateProvider
            .state('Analisar e emitir parecer', {
                url: '/analisar',
                controller: 'AnalisarCtrl as analisar',
                templateUrl: 'analisar/index.html',
                title: 'Analisar e emitir Parecer dos artigos',
                authenticated: true
            });

    $stateProvider
            .state('Avaliar artigo', {
                url: '/avaliar/:idInscricao',
                controller: 'AnalisarCtrl as analisar',
                templateUrl: 'analisar/avaliar.html',
                title: 'Avaliar artigo',
                authenticated: true
            });

    $stateProvider
            .state('Montar comissão', {
                url: '/comissao',
                controller: 'ComissaoCtrl as comissao',
                templateUrl: 'comissao/index.html',
                title: 'Montar as comissões do prêmio',
                authenticated: true
            });

    $stateProvider
            .state('Verificar', {
                url: '/verificar',
                controller: 'VerificarCtrl as verificar',
                templateUrl: 'verificar/index.html',
                title: 'Verificar artigos inscritos',
                authenticated: true
            });

    $stateProvider
            .state('Checklist', {
                url: '/checklist/:idArtigo',
                controller: 'VerificarCtrl as verificar',
                templateUrl: 'verificar/checklist.html',
                title: 'Checklist dos artigos inscritos',
                authenticated: true
            });

    $stateProvider
            .state('Passo 1', {
                url: '/passo1/:idInscricao',
                controller: 'PrimeiroPassoCtrl as primeiroPasso',
                templateUrl: 'inscricao/passo1.html',
                title: 'Inicializar inscrição',
                authenticated: true
            });

    $stateProvider
            .state('Passo 2', {
                url: '/passo2/:idInscricao',
                controller: 'SegundoPassoCtrl as segundoPasso',
                templateUrl: 'inscricao/passo2.html',
                title: 'Preencher dados do artigo',
                authenticated: true
            });
    $stateProvider
            .state('Passo 3', {
                url: '/passo3/:idInscricao',
                controller: 'TerceiroPassoCtrl as terceiroPasso',
                templateUrl: 'inscricao/passo3.html',
                title: 'Enviar documentos',
                authenticated: true
            });

    $stateProvider
            .state('Passo 4', {
                url: '/passo4/:id',
                controller: 'QuartoPassoCtrl as quartoPasso',
                templateUrl: 'inscricao/passo4.html',
                title: 'Finalizar inscrição',
                authenticated: true
            });

    $stateProvider
            .state('Efetuar Login', {
                url: '/login',
                controller: 'EfetuarLoginCtrl as efetuarLogin',
                templateUrl: 'login.html',
                title: 'Efetuar Login',
                authenticated: true
            });

    $stateProvider
            .state('Distribuicao', {
                url: '/distribuicao',
                controller: 'DistribuicaoCtrl as distribuicao',
                templateUrl: 'distribuicao/index.html',
                title: 'Distribuição dos artigos inscritos',
                authenticated: true
            });

    $stateProvider
            .state('Gerenciar Instrução de Avaliação', {
                url: '/gerenciarinstrucaoavaliacao',
                controller: 'GerenciarInstrucaoAvaliacaoCtrl as gerenciarInstrucaoAvaliacao',
                templateUrl: 'gerenciarinstrucaoavaliacao/index.html',
                title: 'Gerenciar Instrução Avaliação',
                authenticated: true
            });

    $stateProvider
            .state('Avaliação de Artigos', {
                url: '/avaliacaoartigos',
                controller: 'AvaliacaoArtigosCtrl as avaliacaoArtigos',
                templateUrl: 'avaliacaoartigos/index.html',
                title: 'Avaliação de Artigos',
                authenticated: true
            });
    $stateProvider
            .state('Painel de Inscrição', {
                url: '/painelinscricao',
                controller: 'PainelInscricaoCtrl as painelInscricao',
                templateUrl: 'painelinscricao/index.html',
                title: 'Painel de Resumo das Inscrições',
                authenticated: true
            });
    $stateProvider
            .state('Painel de Inscrição - Visualizar Detalhe por Situação', {
                url: '/painelinscricao/porsituacao/:id',
                controller: 'PainelInscricaoCtrl as painelInscricao',
                templateUrl: 'painelinscricao/porSituacao.html',
                title: 'Painel de Resumo das Inscrições - Visualizar Detalhe por Situação',
                authenticated: true
            });
    $stateProvider
            .state('Painel de Inscrição - Visualizar Detalhe por Ano', {
                url: '/painelinscricao/porano/:id',
                controller: 'PainelInscricaoCtrl as painelInscricao',
                templateUrl: 'painelinscricao/porAno.html',
                title: 'Painel de Resumo das Inscrições - Visualizar Detalhe por Ano',
                authenticated: true
            });
    $stateProvider
            .state('Painel de Inscrição - Visualizar Detalhe por Ies', {
                url: '/painelinscricao/pories/:id',
                controller: 'PainelInscricaoCtrl as painelInscricao',
                templateUrl: 'painelinscricao/porIes.html',
                title: 'Painel de Resumo das Inscrições - Visualizar Detalhe por Ies',
                authenticated: true
            });
    $stateProvider
            .state('Painel de Inscrição - Visualizar Detalhe por Qualis', {
                url: '/painelinscricao/porqualis/:id',
                controller: 'PainelInscricaoCtrl as painelInscricao',
                templateUrl: 'painelinscricao/porQualis.html',
                title: 'Painel de Resumo das Inscrições - Visualizar Detalhe por Qualis',
                authenticated: true
            });
    $stateProvider
            .state('Painel de Inscrição - Visualizar Detalhe por Categoria', {
                url: '/painelinscricao/porcategoria/:id',
                controller: 'PainelInscricaoCtrl as painelInscricao',
                templateUrl: 'painelinscricao/porCategoria.html',
                title: 'Painel de Resumo das Inscrições - Visualizar Detalhe por Categoria',
                authenticated: true
            });
    $stateProvider
            .state('Painel de Inscrição - Visualizar Detalhe por Nível', {
                url: '/painelinscricao/pornivel/:id',
                controller: 'PainelInscricaoCtrl as painelInscricao',
                templateUrl: 'painelinscricao/porNivel.html',
                title: 'Painel de Resumo das Inscrições - Visualizar Detalhe por Nível',
                authenticated: true
            });
    $stateProvider
            .state('Painel do Prêmio', {
                url: '/painelpremio',
                controller: 'PainelPremioCtrl as painelPremio',
                templateUrl: 'painelpremio/index.html',
                title: 'Painel do Prêmio com Detalhe',
                authenticated: true
            });
    $stateProvider
            .state('Painel do Prêmio - Comissões', {
                url: '/painelpremio/comissao/:etapa/:id',
                controller: 'PainelPremioCtrl as painelPremio',
                templateUrl: 'painelpremio/comissao.html',
                title: 'Painel do Prêmio com Detalhe - Comissões',
                authenticated: true
            });
    $stateProvider
            .state('Painel do Prêmio - Comissões com parecer', {
                url: '/painelpremio/parecer/:etapa/:id',
                controller: 'PainelPremioCtrl as painelPremio',
                templateUrl: 'painelpremio/parecer.html',
                title: 'Painel do Prêmio com Detalhe - Comissões com parecer',
                authenticated: true
            });
    $stateProvider
            .state('Painel do Prêmio - Comissões com parecer final', {
                url: '/painelpremio/parecerfinal/:etapa/:id',
                controller: 'PainelPremioCtrl as painelPremio',
                templateUrl: 'painelpremio/parecerFinal.html',
                title: 'Painel do Prêmio com Detalhe - Comissões com parecer final',
                authenticated: true
            });
    $stateProvider
            .state('Gerar relatório prêmio', {
                url: '/relatoriopremio',
                controller: 'RelatorioPremioCtrl as relatorioPremio',
                templateUrl: 'relatoriopremio/index.html',
                title: 'Gerar Relatório Prêmio',
                authenticated: true
            });
    $stateProvider
            .state('Gerar relatório geral', {
                url: '/relatoriogeral',
                controller: 'RelatorioGeralCtrl as relatorioGeral',
                templateUrl: 'relatoriogeral/index.html',
                title: 'Gerar Relatório Geral',
                authenticated: true
            });
    $stateProvider
            .state('Visualizar Checklist dos Artigos', {
                url: '/visualizarchecklist',
                controller: 'VisualizarChecklistCtrl as visualizarChecklist',
                templateUrl: 'visualizarchecklist/index.html',
                title: 'Visualizar Checklist dos Artigos',
                authenticated: true
            });
	$stateProvider
            .state('Publicação de Editais', {
                url: '/edital',
                controller: 'EditalCtrl as edital',
                templateUrl: 'edital/index.html',
                title: 'Edital',
                authenticated: true
            });

    $stateProvider
        .state('Publicação de Editais - Consulta Editais', {
            url: '/edital/consultaredital',
            controller: 'EditalCtrl as edital',
            templateUrl: 'edital/consultarEditais.html',
            title: 'Consulta Editais',
            authenticated: true
        });
    $urlRouterProvider.otherwise('/');
}

module.exports = {configureRoutes: configureRoutes};