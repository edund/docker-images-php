'use strict';

/** 
 * @ngInject 
 */
function configureTranslate($translateProvider){

// configura idioma
  var defaultLanguage = 'pt-br';
  var languageSupport = ['pt-br','en'];

  $translateProvider.useSanitizeValueStrategy('escaped');

  $translateProvider.useStaticFilesLoader({
    prefix: 'languages/',
    suffix: '.json'
  });

  $translateProvider.determinePreferredLanguage(function () {
     var userLang = navigator.language || navigator.userLanguage;
     return (languageSupport.indexOf(userLang) == -1) ? defaultLanguage : userLang;
  }); 
}

module.exports = { 
  configureTranslate : configureTranslate
};