'use strict';

var routes = require('./routes');
var translates = require('./translates');

/**
 * @ngInject
 */
var OnConfig = ["$stateProvider","$locationProvider","$urlRouterProvider","$translateProvider","blockUIConfig", "AuthServiceConfig",
    function ($stateProvider, $locationProvider, $urlRouterProvider,$translateProvider, blockUIConfig, AuthServiceConfig){
        // Configura o block UI
        blockUIConfig.message = 'Carregando...';

        //configura as rotas do sistema
        routes.configureRoutes($stateProvider, $locationProvider, $urlRouterProvider);

        // configura suporte a internacionalização
        translates.configureTranslate($translateProvider);
        AuthServiceConfig.url = "/auth";
        AuthServiceConfig.task = "/status";

    }];

module.exports = OnConfig;