'use strict';

var controllersModule = require('../_index');

/**aalvessa
 * @ngInject
 */
var EditalController = ["$state", "toaster", "$scope", "$rootScope", "$capesModal",
    "AppSettings", "CommonService", "EditalService", "$stateParams", "$window",
    function ($state, toaster, $scope, $rootScope, $capesModal, AppSettings,
              CommonService, EditalService, $stateParams, $window) {
        $scope.aviso = [];
        $scope.isValid = false;
        $scope.flagSucess = false;
        $scope.AppSettings = AppSettings;
        $scope.edicao = '';
        $scope.file_upload = '';
        $scope.disableButton = true;

        $scope.fileuploadConf = {
            'options': {
                'url': '/rest/edital',
                'method': 'post',
                'paramName': "files",
                'maxFilesize': '2',
                'autoProcessQueue': false,
                'parallelUploads': 1,
                'maxFiles': 1,
                'clickable': true,
                'headers': {"tipo-arquivo": $scope.tipo_documento, "id-pessoa": $scope.id_pessoa},
                'dictDefaultMessage': 'Clique aqui ou arraste e solte um arquivo para enviar.',
                'dictFileTooBig': AppSettings.messages.MSG048,
                'dictInvalidFileType': AppSettings.messages.MSG008,
                'addRemoveLinks': true,
                'dictRemoveFile':"Remover",
                'acceptedFiles': '.pdf',
                'dictResponseError': AppSettings.messages.MSG017,
            },
        };
        Dropzone.autoDiscover = false;
        Dropzone.options.fileUpload = {
            init: function () {
                var myDropzone = this;
                jQuery('#arquivo_edital').removeClass('has-error');
                jQuery('#file-upload').css({backgroundColor: "#fff", border: "1px dashed #005883"});

                $("#btn-salvar").click(function (e) {
                    $scope.file_upload = myDropzone.files.length;
                    $scope.isValid = validarDados($scope, toaster, AppSettings, $stateParams, CommonService);

                    if ($scope.isValid === true) {
                        if (myDropzone.files.length !== 0) {
                            e.preventDefault();
                            e.stopPropagation();
                            myDropzone.processQueue();
                        }
                            if ($scope.edicao !== "") {
                                var dados = {
                                    'id': $scope.edicao,
                                    'evento': $scope.evento.id,
                                    'dsArquivoEvento': $scope.dsArquivoEvento,
                                    'anPublicacao': $scope.anPublicacao,
                                    'inPublicacao': $scope.inPublicacao
                                }
                                EditalService.salvar(dados)
                                    .then(function (dados) {
                                        if (dados.exist === true) {
                                            $scope.aviso = {
                                                show: true,
                                                type: 'danger',
                                                msg: AppSettings.messages.MSG029
                                            };
                                        } else {
                                            if (dados.status === 'error') {
                                                toaster.pop('error', '', AppSettings.messages.MSG034);
                                            } else {
                                                $scope.aviso = {
                                                    show: true,
                                                    type: 'success',
                                                    msg: AppSettings.messages.MSG002
                                                };
                                                carregarDados(EditalService, $scope, toaster, AppSettings, $stateParams, CommonService);
                                            }
                                        }
                                    }).catch(function (err) {
                                });
                            }
                    }else{
                        toaster.pop('error', '', AppSettings.messages.MSG025,10000);
                    }
                });

                this.on("error", function (files, errorMessage, xhr) {
                    if (xhr.status === 500 || xhr.status === 404) {
                        $(files.previewElement).find('.dz-error-message > span').text(AppSettings.messages.MSG017);
                    }
                    if (xhr.status === 405) {
                        $scope.aviso = {
                            show: true,
                            type: 'danger',
                            msg: AppSettings.messages.MSG046
                        };
                        carregarDados(EditalService, $scope, toaster, AppSettings, $stateParams, CommonService);
                        $(files.previewElement).find('.dz-error-message > span').text(AppSettings.messages.MSG046);
                    }
                });

                this.on("success", function (files, response,xhr) {
                    var dropzone = this;
                    dropzone.removeAllFiles();
                    $scope.aviso = {
                        show: true,
                        type: 'success',
                        msg: AppSettings.messages.MSG002
                    };
                    carregarDados(EditalService, $scope, toaster, AppSettings, $stateParams, CommonService);
                });

            }
        };
        carregarDados(EditalService, $scope, toaster, AppSettings, $stateParams, CommonService);

        $scope.editArquivo = function (arquivo) {
            $scope.id = arquivo.id;
            $scope.dsArquivoEvento = arquivo.dsArquivoEvento;
            $scope.evento = arquivo.evento;
            $scope.eventoEdicao = arquivo.evento.id;
            $scope.anPublicacao = arquivo.anPublicacao;
            $scope.inPublicacao = arquivo.inPublicacao === 'S' ? true : false;
            $scope.edicao = arquivo.id;
        }

        $scope.validarAno = function (model) {
            if (model < 2015) {
                $scope.aviso = {
                    show: true,
                    type: 'danger',
                    msg: 'Ano da publicação informado não é permitido. O Prêmio CAPES Natura Campus de Excelência em Pesquisa foi criado a partir de 2015.'
                };
                $scope.anPublicacao = '';
                $scope.aviso.msg = 'Ano da publicação informado não é permitido. O Prêmio CAPES Natura Campus de Excelência em Pesquisa foi criado a partir de 2015.';
            } else {
                $scope.fecharAviso();
            }
        }

        $scope.removeArquivo = function (param) {
            $capesModal.confirm({
                "text": AppSettings.messages.MSG011,
                "title": 'Confirmação',
                "dismissText": 'Voltar'
            }).then(function () {
                if (!param) {
                    $scope.aviso = {
                        show: true,
                        type: 'error',
                        msg: 'Não foi possivel excluir o documento.',
                    };
                    window.scrollTo(0, 0);
                } else {
                    EditalService.removerArquivoEvento(param)
                        .then(function (dados) {
                            $scope.aviso = {
                                show: true,
                                type: 'success',
                                msg: AppSettings.messages.MSG002
                            };
                            window.scrollTo(0, 0);
                            carregarDados(EditalService, $scope, toaster, AppSettings, CommonService);
                        }).catch(function (err) {
                        if (err.status == 500 || err.status === 404) {
                            toaster.pop('error', 'Sistema', AppSettings.messages.MSG017);
                        }else{
                            toaster.pop('error', 'Sistema', 'Não foi possivel excluir o documento.');
                        }

                    });
                }
            });
        };

        $scope.fecharAviso = function () {
            $scope.aviso = {
                show: false
            };
        };

        $scope.validarEvento = function () {
            var obj_edital = $scope;
            var exist_obj = false;
            var pos_old_obj = 0;
            jQuery('#evento_edital').removeClass('has-error');
            jQuery('.chosen-container-single .chosen-single').css({border: "1px solid #ccc"});
            angular.forEach($scope.itens_arquivos, function (arquivo, indice) {
                if (arquivo.evento.id === obj_edital.evento.id && $scope.eventoEdicao !== obj_edital.evento.id) {
                    exist_obj = true;
                    pos_old_obj = indice;
                }
            });
            if (exist_obj) {
                $scope.evento = '?';
                jQuery('#evento_edital').addClass('has-error');
                jQuery('.chosen-container-single .chosen-single').css({border: "1px solid #f10909"});
                toaster.pop('error', '', AppSettings.messages.MSG046);
                return;
            } else {
                return true;
            }
        };

        $scope.downloadFile = function (idEdital) {
            EditalService.downloadEdital(idEdital)
                .then(function (reply) {
                    var url = '../rest/edital/' + idEdital;
                    $window.location.href = url;
                    return false;

                }).catch(function (err) {
                if (err.status == 500 || err.status === 404) {
                    toaster.pop('error', 'Serviço', AppSettings.messages.MSG017);
                }
            });
        };

    }];
controllersModule.controller('EditalCtrl', EditalController);

function carregarDados(EditalService, $scope, toaster, AppSettings, CommonService) {
    EditalService.listaEventoArquivos()
        .then(function (dados) {
            $scope.itens_arquivos = dados.itens_arquivos;
            $scope.itens_combo = dados.itens_combo;
            $scope.itens_editais = dados.itens_editais;
            $scope.AppSettings = AppSettings;
            $scope.permissao  = dados.permissao;
            $scope.id = '';
            $scope.dsArquivoEvento = '';
            $scope.evento = '';
            $scope.eventoEdicao = '';
            $scope.anPublicacao = '';
            $scope.inPublicacao = '';
            $scope.edicao = '';
        }).catch(function (err) {
        if (err.status === 401) {
            CommonService.redirectUserAccess(true);
        } else {
            toaster.pop('error', 'Sistema', 'Não foi possivel recuperar a informação');
        }
    });
}

function validarDados($scope, toaster, AppSettings) {
    var obj_edital = $scope;
    var exist_obj = false;
    var campo_vazio = 0;
    jQuery('#evento_edital').removeClass('has-error');
    jQuery('.chosen-container-single .chosen-single').css({border: "1px solid #ccc"});
    jQuery('#ds_arquivo_evento').removeClass('has-error');
    jQuery('#an_publicacao').removeClass('has-error');
    jQuery('#arquivo_edital').removeClass('has-error');
    jQuery('#file-upload').css({ backgroundColor: "rgb(255, 255, 255)", border: "1px dashed #005883"});

    if (obj_edital.evento === undefined || obj_edital.evento === '?' || obj_edital.evento === "") {
        campo_vazio = 1;
        jQuery('#evento_edital').addClass('has-error');
        jQuery('.chosen-container-single .chosen-single').css({border: "1px solid #f10909"});
    }
    if (obj_edital.dsArquivoEvento === undefined || obj_edital.dsArquivoEvento === null || obj_edital.dsArquivoEvento === "") {
        campo_vazio = 1;
        jQuery('#ds_arquivo_evento').addClass('has-error');
    }
    if (obj_edital.anPublicacao === undefined || obj_edital.anPublicacao === "") {
        campo_vazio = 1;
        jQuery('#an_publicacao').addClass('has-error');
    }
    if ($scope.edicao === "" && $scope.file_upload === 0) {
        jQuery('#arquivo_edital').addClass('has-error');
        jQuery('#file-upload').css({ backgroundColor: "#dcdcdc", border: "1px dashed #f10909"});
        campo_vazio = 1;
    }
    if (campo_vazio === 1) {
        toaster.pop('error', '', AppSettings.messages.MSG025,1000);
        // return false;
    }
    if (campo_vazio === 0) {
        return true;
    }
}
