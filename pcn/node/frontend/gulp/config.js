'use strict';

module.exports = {

  'serverport': 3000,

  'backend': {
	  'context': 'pcn',
	  'http': 'http://pcn.local.capes.gov.br:8000'
  },


  'styles': {
    'src' : ['node_modules/capes-tmpl-externo-angular/dist/css/main.css','node_modules/angularjs-toaster/toaster.css','app/styles/**/*.less'],
    'dest': 'build/css',
	'login': {
		'src': 'node_modules/capes-tmpl-externo-angular/dist/css/login.css'
	}
  },

  'scripts': {
    'src' : ['app/js/**/*.js', '!app/js/**/*.js'],
    'dest': 'build/js'
  },

  'images': {
    'src' : ['app/images/**/*','node_modules/capes-tmpl-externo-angular/dist/images/**/*'],
    'dest': 'build/images'
  },

  'fonts': {
    'src' : ['app/fonts/**/*','node_modules/capes-tmpl-externo-angular/dist/fonts/**/*'],
    'dest': 'build/fonts'
  },

  'translate': {
    'src' : ['app/languages/**/*'],
    'dest': 'build/languages'
  },

  'views': {
    'watch': [
      'app/index.html',
      'app/views/**/*.html'
    ],
    'src': 'app/views/**/*.html',
    'dest': 'app/js'
  },

  'gzip': {
    'src': 'build/**/*.{html,xml,json,css,js,js.map}',
    'dest': 'build/',
    'options': {}
  },

  'dist': {
    'root'  : 'build'
  },

  'browserify': {
    'entries'   : ['./app/js/main.js'],
    'bundleName': 'main.js',
    'sourcemap' : true
  },

  'test': {
    'karma': 'test/karma.conf.js',
    'protractor': 'test/protractor.conf.js'
  }

};
