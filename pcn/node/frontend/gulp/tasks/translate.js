'use strict';

var gulp   = require('gulp');
var config = require('../config');
var minify = require('gulp-jsonminify');

gulp.task('translate', function() {

  return gulp.src(config.translate.src)
    .pipe(gulp.dest(config.translate.dest));

});
