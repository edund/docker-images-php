Geração do ambiente dos prêmios
==================================

# Adicione ao seu projeto #

Simplesmente, descompacte o arquivo no seu projeto, isso criará `docker-compose.yml` na raiz do seu projeto e nas pastas `pct e pcn` contendo nginx e php-fpm config para cada um deles.

**Nota**: você pode colocar os arquivos em outro local do seu projeto. Certifique-se de modificar os locais para o dockerfile php-fpm, as substituições do php.ini e a configuração do nginx no `docker-compose.yml`, se você o fizer. 


# Como executar #

Dependências:

  * Docker engine v1.13 ou superior. O pacote fornecido pelo sistema operacional pode ser um pouco antigo; se você encontrar problemas, atualize. Veja em [https://docs.docker.com/engine/installation](https://docs.docker.com/engine/installation)
  * Docker compose v1.12 or higher. Veja em [docs.docker.com/compose/install](https://docs.docker.com/compose/install/)

Quando terminar, simplesmente `cd` no seu projeto e execute` docker-compose up -d`. Isso inicializará e iniciará todos os contêineres, deixando-os em execução em segundo plano.

## Serviços expostos fora do seu ambiente ##

Você pode acessar seu aplicativo via **`localhost`**, se estiver executando os contêineres diretamente, ou através de **``** quando executado em uma vm. nginx e mailhog respondem a qualquer nome de host, caso você queira adicionar seu próprio nome de host no seu `/etc/hosts`

Servidor|Endereço|Saída containers
------|---------|-----------
edund-Webserver-pct|[localhost:80](http://localhost:80)
edund-Webserver-pcn|[localhost:8000](http://localhost:8000)
Mailhog web interface|[localhost:8001](http://localhost:8001)
MariaDB|**host:** `localhost`; **porta:** `8003`


## Hosts em seu ambiente ##

Você precisará configurar seu aplicativo para usar qualquer serviço que você ativou:

Servidor|Nome do host|Número da porta
------|---------|-----------
edund-php-fpm-pct|edund-edund-php-pct|80
edund-php-fpm-pcn|edund-php-fpm|8000
edund-MariaDB|edund-mariadb|3306 (default)
edund-Memcached|edund-memcached|11211 (default)
edund-Redis|edund-redis|6379 (default)
edund-Elasticsearch|edund-elasticsearch|9200 (HTTP default) / 9300 (ES transport default)
edund-ClickHouse|edund-clickhouse|9000 (HTTP default)
edund-SMTP (Mailhog)|edund-mailhog|1025 (default)

# Dicas para usar Docker compose #

**Nota:** você precisa primeiro estar dentro do diretório aonde o arquivo docker-compose.yml estar.

  * Inciar os containers em background: `docker-compose up -d`
  * Inicar os containers em primeiro plano: `docker-compose up`. Você verá um fluxo de logs para cada contêiner em execução.
  * Parar os containers: `docker-compose stop`
  * Excluir os containers: `docker-compose kill`
  * Ver os logs do container: `docker-compose logs`
  * Executar comandos dentro do container: `docker-compose exec SERVICE_NAME COMANDO` Onde `COMMANDO` é o que você deseja executar. Exemplos:
        * Acessar Shell do PHP container, `docker-compose exec edund-php-fpm-pct ou pcn bash`
        * Executar comandos via console, `docker-compose exec edund-php-fpm-pct ou pcn COMANDO`
        * Open a mysql shell, `docker-compose exec edund-mysql mysql -uroot -pSENHA_ROOT_USUARIO`

# Recomendações #

É difícil evitar problemas de permissão de arquivo ao mexer nos contêineres, porque, do ponto de vista do sistema operacional, todos os arquivos criados no contêiner pertencem ao processo que executa o mecanismo da janela de encaixe (geralmente é raiz). Sistemas operacionais diferentes também terão problemas diferentes. Por exemplo, você pode executar coisas em contêineres usando `docker exec -it -u $ (id -u): $ (id -g) CONTAINER_NAME COMMAND` para forçar seu ID de usuário atual ao processo, mas isso só funcionará se o seu sistema operacional host for Linux, não mac. Siga algumas regras simples e salve um mundo de mágoas.
  
  * Execute o compositor fora do contêiner php, pois isso instalaria todas as suas dependências pertencentes ao `root` dentro da pasta do fornecedor.
  * Execute comandos (por exemplo, o console do Symfony ou o artesão do Laravel) diretamente dentro do seu contêiner. Você pode facilmente abrir um shell como descrito acima e fazer o que quiser a partir daí.


# Problemas de memória #

Caso tenha problemas em subir algum container, isso pode ser falta de memória alocada para rodar a vm, para solucionar este problema basta executar o comando abaixo:

**Linux:**  
`sudo sysctl -w vm.max_map_count=262144`

**Windows:**
`Em breve`  
